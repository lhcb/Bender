# 2018-04-21 Bender v32r1
=========================

This version is released on '2018-patches' branch of DaVinci and 'master' branch of Bender 

It is based on DaVinci [v44r1](http://lhcbdoc.web.cern.ch/lhcbdoc/davinci/releases/v44r1)

