{% set used = [] -%}
{% macro section(labels) -%}
{% for mr in select_mrs(merge_requests, labels, used) %}
- {{mr.title}}, !{{mr.iid}} (@{{mr.author.username}}) {{find_tasks(mr)}}  
  {{mr.description|mdindent(2)}}
{% endfor %}
{%- endmacro %}

{{date}} {{project}} {{version}}
===

This version uses Lbcom v30rX, LHCb v50rX, Gaudi v31r0 and LCG_95 with ROOT 6.16.00.

This version is a xxx release for xxx  

This version is released on `master` branch. The previous release on `master` branch  was Boole `v40rY`.  

### New features
{{ section(['new feature']) }}

### Enhancements
{{ section(['enhancement']) }}

### Bug fixes
{{ section(['bug fix']) }}

### Code modernisations and cleanups
{{ section(['cleanup', 'modernisation']) }}

### Monitoring changes
{{ section(['monitoring']) }}

### Changes to tests
{{ section(['testing']) }}

### Other
{{ section([[]]) }}
