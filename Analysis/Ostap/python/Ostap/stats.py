#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file Ostap/stats.py
#  Few very simple statistical utilities
#
#     .oooooo.                .
#    d8P'  `Y8b             .o8
#   888      888  .oooo.o .o888oo  .oooo.   oo.ooooo.
#   888      888 d88(  "8   888   `P  )88b   888' `88b
#   888      888 `"Y88b.    888    .oP"888   888   888
#   `88b    d88' o.  )88b   888 . d8(  888   888   888
#    `Y8bood8P'  8""888P'   "888" `Y888""8o  888bod8P'
#                                            888
#                                           o888o
#
#  This file is a part of
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV.
#  And it is based on the
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  <b>``C++ ToolKit for Smart and Friendly Physics Analysis''</b>
#
#  By usage of this code one clearly states the disagreement
#  with the smear campaign of Dr.O.Callot et al.:
#  ``No Vanya's lines are allowed in LHCb/Gaudi software''
#
#  @date   2017-08-15
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#
# =============================================================================
""" Few very simple statistcial utilities for ostap (wrappers for scipy.stats)

This file is a part of BENDER project:

``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV.

And it is based on the LoKi project: ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement with the smear campaign
of Dr.O.Callot et al.:

``No Vanya's lines are allowed in LHCb/Gaudi software''
"""
# =============================================================================
__author__ = 'Vanya BELYAEV Ivan.Belyaev@itep.ru'
__date__ = "2017-08-15"
__version__ = '$Revision$'
__all__ = (
    'cmoment',  ## central moment            (with uncertainty)
    'variance',  ## moment (relative to zero) (with uncertainty)
    'dispersion',  ## dispersion                (with uncertainty)
    'mean',  ## mean                      (with uncertainty)
    'rms',  ## rms                       (with uncertainty)
)
# =============================================================================
from Ostap.Core import VE

# =============================================================================


# =============================================================================
## central moment with errors
#  Evaluate the central moment and its  uncertainty
#  - evaluation is  fast: numpy+scipy.stats
#  - uncertainty is approximate, order of 1/n^2
def cmoment(lst, order, errors=True):
    """Evaluate the central moment with uncertainties
    - evaluation is fast: numpy+scipy.stats
    - but uncertainty is approximate, order of 1/n^2
    >>> a = [ 1,2,3 ... ]
    >>> print cmoment ( a , 3    )
    """
    if not lst: raise AttributeError("Invalid data")
    elif not isinstance(order, int) or order < 0:
        raise AttributeError("Invalid order of the central moment")
    elif 0 == order:
        return 1.0 if not errors else VE(1, 0)
    elif 1 == order:
        return 0.0 if not errors else VE(0, 0)

    ## resolve  generator objects
    import types
    if isinstance(lst, types.GeneratorType):
        lst = [i for i in lst]

    ##use numbpy & scipy
    import numpy, scipy.stats

    ## convert to numpy if needed
    if not isinstance(lst, numpy.array):
        lst = numpy.array(lst)

    ## use scipy
    v = scipy.stats.moment(lst, order)

    if not errors: return v  ## RETURN

    ## evaluate the uncertainty in the central moment with precision (1/n)^2
    mu2 = scipy.stats.moment(lst, 2)
    muo = float(v)
    mu2o = scipy.stats.moment(lst, 2 * order)
    muop = scipy.stats.moment(lst, order + 1)
    muom = scipy.stats.moment(lst, order - 1)

    d2 = mu2o
    d2 -= 2 * order * muom * muop
    d2 -= muo * muo
    d2 += order * order * mu2 * muom * muom
    d2 /= len(lst)

    return VE(v, d2)


# =============================================================================
## calculate the ``regular'' moment (relative to zero) with uncertainty
#  - evaluation is relatively slow: bare python ``sum''
#  - uncertaintues are correct
def moment(lst, order, errors=True):
    """Calculate the ``regular'' moment (relative to zero) with uncertainties
    - evaluation is relatively low: bare python ``sum''
    - uncertaintues are correct
    >>> a = [ 1,2,3 ... ]
    >>> print moment ( a , 4 )
    >>> print moment ( a , 4 , errors = False )
    """
    if not lst: raise AttributeError("Invalid data")
    elif not isinstance(order, int) or order < 0:
        raise AttributeError("Invalid order of the moment")
    elif 0 == order:
        return 1.0 if not errors else VE(1, 0)
    elif 1 == order:
        return mean(lst, errors)

    # resolve generator objects
    import types
    if isinstance(lst, types.GeneratorType):
        lst = [i for i in lst]

    ## need to find better way with numpy
    n = len(lst)
    v = sum(i**order for i in lst) / n
    #
    if not errors: return v
    #
    ## calculate the uncertainty
    mu2o = moment(lst, 2 * order, False)
    muo = float(v)
    #
    return VE(v, (mu2o - muo) / n)


# =============================================================================
## evaluate the variance of the sample
def variance(lst, errors=True):
    """ Evaluate the variance of the sample
    >>> a = [ 1,2,3 ... ]
    >>> print variance   ( a )
    >>> print dispersion ( a ) ## ditto
    """
    return moment(lst, 2, errors)


# =============================================================================
## ditto
dispersion = variance


# =============================================================================
## get RMS
def rms(lst, errors=True):
    """Get RMS for the sample
    >>> a = [ 1,2,3 ... ]
    >>> print rms ( a )
    """
    return variance(lst, errors)**0.5


# =============================================================================
## get mean value (with uncertainty)
def mean(lst, errors=True):
    """Get mean value (with uncertainty) for the sample
    >>> a = [ 1,2,3 ... ]
    >>> print mean ( a )
    """
    if not lst: raise AttributeError("Invalid data")

    ## resolve  generator objects
    import types
    if isinstance(lst, types.GeneratorType):
        lst = [i for i in lst]

    ## need to use numpy if arrays are very long
    n = len(lst)
    v = sum(lst) / n
    #
    if not errors: return v
    #
    return VE(v, variance(lst, False) / n)


# =============================================================================
if '__main__' == __name__:

    import Ostap.Line
    logger.info(__file__ + '\n' + Ostap.Line.line)
    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(80 * '*')
    logger.info(' Author  : %s' % __author__)
    logger.info(' Version : %s' % __version__)
    logger.info(' Date    : %s' % __date__)
    logger.info(' Symbols : %s' % list(__all__))
    logger.info(80 * '*')

# =============================================================================
# The END
# =============================================================================
