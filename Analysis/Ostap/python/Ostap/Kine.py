#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  Few very simple math-functions related to kinematics
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2011-06-07
#
# =============================================================================
"""Decoration of some RooFit objects for efficient use in python"""
# =============================================================================
__version__ = "$Revision$"
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2011-06-07"
__all__ = (
    'EtaVsP',  ## eta=eta(p)  for the fixed PT
    'EtaVsPt',  ## eta=eta(pt) for the fixed P
    'YvsP',  ## y=yy(p)     for the fixed PT and mass
    'YvsPt',  ## y=y(pt)     for the fixed P  and mass
    'EtaVsPPT',  ## eta=eta(p,pt)
    'PtVsPEta',  ## pt=pt(p,eta)
    'PvsPtEta',  ## p= p(pt,eta)
    ##
    'kallen',  ##  kallen ``lambda''/``triangle'' function
    'phasespace2',  ## two-body phase space
    'phasespace3',  ## two-body phase space
    'phasespace4',  ## two-body phase space
    'phasespace',  ## two-body phase space
    ##
    'G',  ## the basic universal 4-body function G
)
# =============================================================================
import math
# =============================================================================
# logging
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' == __name__: logger = getLogger('Ostap.Kine')
else: logger = getLogger(__name__)
# =============================================================================
logger.debug('Few very simple math-functions related to kinematics')
# =============================================================================
_acosh = math.acosh
_atanh = math.atanh
_sqrt = math.sqrt
_cosh = math.cosh
if not hasattr(math, 'coth'):
    math.coth = lambda x: 1.0 / math.tanh(x)
    math.coth.__doc__ = """coth(x)
    Return the   hyperbolic cotangent of x
    """
    logger.debug("Insert coth function into math")

if not hasattr(math, 'acoth'):
    math.acoth = lambda x: math.tanh(1.0 / x)
    math.acoth.__doc__ = """acoth(x)
    Return the hyperbolic area cotangent of x (|x|>1)
    """
    logger.debug("Insert acoth function into math")


# =============================================================================
## @class EtaVsP
#  very simple function \f$ \eta = \eta(p) \$  for the fixed transverse momentum
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2014-07-19
class EtaVsP(object):
    """
    Very simple function \f$ \eta = \eta(p) \$  for the fixed transverse momentum
    """

    def __init__(self, pt):
        assert isinstance(pt, (int, long, float)) and 0 <= pt, "PT is invalid!"
        self.pt = float(pt)

    def __call__(self, p):
        return _acosh(max(p, self.pt) / self.pt)


# =============================================================================
## @class EtaVsPt
#  very simple function \f$ \eta = \eta(p_T) \$  for the fixed momentum
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2014-07-19
class EtaVsPt(object):
    """
    Very simple function \f$ \eta = \eta(p_T) \$  for the fixed momentum
    """

    def __init__(self, p):
        assert isinstance(p, (int, long, float)) and 0 <= p, "P is invalid!"
        self.p = float(p)

    def __call__(self, pt):
        return _acosh(self.p / min(pt, self.p))


## rapidity as function of P for fixed pt and mass
class YvsP(object):
    def __init__(self, pt, mass):
        assert isinstance(pt, (int, long, float)) and 0 <= pt, "PT is invalid!"
        assert isinstance(mass,
                          (int, long, float)) and 0 <= mass, "M  is invalid!"
        self.pt2 = float(pt) * float(pt)
        self.m2 = float(mass) * float(mass)

    def __call__(self, p):

        p2 = p * p
        e2 = p2 + self.m2
        pz2 = p2 - self.pt2

        return _atanh(_sqrt(max(pz2, 0.0) / e2))


## rapidity as function of Pt for fixed p and mass
class YvsPt(object):
    def __init__(self, p, mass):
        assert isinstance(p, (int, long, float)) and 0 <= p, "P  is invalid!"
        assert isinstance(mass,
                          (int, long, float)) and 0 <= mass, "M  is invalid!"
        self.p2 = float(p) * float(p)
        self.m2 = float(mass) * float(mass)
        self.e2 = self.p2 + self.m2

    def __call__(self, pt):
        pt2 = pt * pt
        pz2 = self.p2 - pt2
        return _atanh(_sqrt(max(pz2, 0.0) / self.e2))


## helper wrapper
class _WF1(object):
    def __init__(self, obj):
        self.obj = obj

    def __call__(self, x, pars=[]):
        return self.obj(x[0])


# =============================================================================
## convert the objects to the functions
def _as_TF1_(obj, xmin, xmax):

    import ROOT
    from Ostap.Core import funID

    fobj = _WF1(obj)
    fun = ROOT.TF1(funID(), fobj, xmin, xmax)
    fun._obj = fobj

    fun.SetNpx(500)

    return fun


EtaVsP.asTF1 = _as_TF1_
EtaVsPt.asTF1 = _as_TF1_
YvsP.asTF1 = _as_TF1_
YvsPt.asTF1 = _as_TF1_


class EtaVsPPT(object):
    "eta = eta(p ,pt )"

    def __call__(self, p, pt):
        return _acosh(p / pt)


class PtVsPEta(object):
    "pt  = pt (p ,eta)"

    def __call__(self, p, eta):
        return p / _cosh(eta)


class PvsPtEta(object):
    "p   = p  (pt,eta)"

    def __call__(self, pt, eta):
        return pt * _cosh(eta)


## helper wrapper
class _WF2(object):
    def __init__(self, obj):
        self.obj = obj

    def __call__(self, x, pars=[]):
        return self.obj(x[0], x[1])


## convert the objects to the function
def _as_TF2_(obj, xmin, xmax, ymin, ymax):

    import ROOT
    from Ostap.Core import funID

    fobj = _WF2(obj)
    fun = ROOT.TF2(funID(), fobj, xmin, xmax, ymin, ymax)
    fun._obj = fobj

    fun.SetNpx(250)
    fun.SetNpy(250)

    return fun


EtaVsPPT.asTF2 = _as_TF2_
PtVsPEta.asTF2 = _as_TF2_
PvsPtEta.asTF2 = _as_TF2_


# =============================================================================
## Kallen function, aka ``lambda''/``triangle'' function
#  @see https://en.wikipedia.org/wiki/K%C3%A4ll%C3%A9n_function
def kallen(x, y, z):
    """ Kallen function, aka ``triangle'' function
    - see https://en.wikipedia.org/wiki/K%C3%A4ll%C3%A9n_function
    """
    return x * x + y * y + z * z - 2.0 * x * y - 2.0 * y * z - 2.0 * z * x


# =============================================================================
## Calculate the two-body phase space
#  \f$ R_2 = \frac{ \pi \lambda^{1/2}( M^2 , m_1^2 , m_2^2) }{2*M^2} \f$
#  @code
#  M, m1  , m2 = ...
#  ps = phasespace2 ( M , m1 , m2 )
#  @endcode
#  @attention this is "kinematic" phase space, note missing (2pi)^{-2} factor!
#
def phasespace2(M, m1, m2):
    r"""Calculate the full two-body phase space
    \f$ R_2 = \frac{ \pi \lambda^{1/2}( M^2 , m_1^2 , m_2^2) }{2*M^2} \f$
    >>> M, m1  , m2 = ...
    >>> ps = phasespace2 ( M , m1 , m2 )
    - attention:  this is ``kinematic'' phase space, note missing (2pi)^{-2} factor!
    """
    assert 0 < M and 0 <= m1 and 0 <= m2, 'Invalid setting of masses!'

    ##
    if m1 + m2 >= M: return 0  ## RETURN!

    s = M * M
    import math
    return math.pi * math.sqrt(kallen(s, m1 * m1, m2 * m2)) / (2.0 * s)


# =============================================================================
## Calculate the three body phase space
#  @code
#  M, m1  , m2 , m3 = ...
#  ps3 = phasespace3 ( M , m1  , m2 , m3 )
#  @endcode
def phasespace3(M, m1, m2, m3):
    """Calculate the full three body phase space:
    >>> M, m1  , m2 , m3 = ...
    >>> ps3 = phasespace3 ( M , m1  , m2 , m3 )
    """
    assert 0 < M and 0 <= m1 and 0 <= m2 and 0 <= m3, 'Invalid setting of masses!'

    ##
    if m1 + m2 + m3 >= M: return 0  ## RETURN!

    s = M * M
    m1_2 = m1 * m1
    m2_2 = m2 * m2
    m3_2 = m3 * m3

    high = (M - m1)**2
    low = (m2 + m3)**2

    import math
    func = lambda x: math.sqrt(kallen(x, s, m1_2) * kallen(x, m2_2, m3_2)) / x

    from LHCbMath.deriv import integral
    r = integral(func, low, high, err=False)

    return (math.pi**2) * r / (4.0 * s)


# =============================================================================
## Calculate the four body phase space
#  @code
#  M, m1  , m2 , m3 , m4 = ...
#  ps4 = phasespace4 ( M , m1  , m2 , m3 , m4 )
#  @endcode
#  The algorithm includes two embedded numerical integration -> could be relatively slow
def phasespace4(M, m1, m2, m3, m4):
    """Calculate the full four body phase space
    >>> M, m1  , m2 , m3 , m4 = ...
    >>> ps4 = phasespace4 ( M , m1  , m2 , m3 , m4 )
    - The algorithm includes two embedded numerical integration -> could be relatively slow
    """
    assert 0 < M and 0 <= m1 and 0 <= m2 and 0 <= m3 and 0 <= m4, 'Invalid setting of masses!'

    ##
    if m1 + m2 + m3 + m4 >= M: return 0  ## RETURN!

    low = m1 + m2
    high = M - m3 - m4

    func = lambda x : 2.0 * x * phasespace3 ( M , x , m3 , m4 ) * phasespace2 ( x , m1 , m2  )

    from LHCbMath.deriv import integral
    return integral(func, low, high, err=False)


# ==============================================================================
## calculate full N-body phase space
#  @code
#  M, m1 , m2 , ... , mn = ...
#  ps = phasespace ( M , m1 , m2 , ... , mn )
#  @endcode
#  The algorithm includes embedded numerical integrations -> could be relatively slow
def phasespace(M, *args):
    """Calculate full  N-body phase space
    >>> M, m1 , m2 , ... , mn = ...
    >>> ps = phasespace ( M , m1 , m2 , ... , mn )
    - The algorithm includes embedded numerical integrations -> could be relatively slow
    """

    assert 0 < M and 2 <= len(args), 'Invalid setting of masses!'

    summ = 0.0
    for m in args:
        assert 0 <= m, 'Invalid setting of masses'
        summ += m

    if summ >= M: return 0

    N = len(args)
    if 2 == N: return phasespace2(M, *args)
    elif 3 == N: return phasespace3(M, *args)
    elif 4 == N: return phasespace4(M, *args)

    ## split particles into two groups & (recursively) apply the splitting formula

    k = N / 2

    args1 = args[k:]
    args2 = args[:k]

    low = sum(args1)
    high = M - sum(args2)

    func = lambda x: 2.0 * x * phasespace(M, x, *args2) * phasespace(x, *args1)

    from LHCbMath.deriv import integral
    return integral(func, low, high, err=False)


# =============================================================================
## the basic universal 4-body function G
#  @see E.Byckling, K.Kajantie, "Particle kinematics", John Wiley & Sons,
#       London, New York, Sydney, Toronto, 1973, p.89, eq. (5.23)
#  @see https://userweb.jlab.org/~rafopar/Book/byckling_kajantie.pdf
#  E.g. physical range for 2->2 scattering process is defined as
#  \f$ G(s,t,m_2^2, m_a^2, m_b^2, m_1^2) \le 0 \f$
# or the phsyical range  for Dalitz plot is
#   \f$ G(s_2, s_1,  m_3^2, m_1^2, s , m_2^2) \le 0 \f$
def G(x, y, z, u, v, w):
    r"""The basic universal 4-body function G
    - see E.Byckling, K.Kajantie, ``Particle kinematics'', John Wiley & Sons,
    London, New York, Sydney, Toronto, 1973 p.89, eq. (5.23)
    - see https://userweb.jlab.org/~rafopar/Book/byckling_kajantie.pdf

    E.g. physical range for 2->2 scattering process is defined as
    \f$ G(s,t,m_2^2, m_a^2, m_b^2 , m_1^2)     \le 0 \f$
    or the physical range  for Dalitz plot is
    \f$ G(s_2, s_1,  m_3^2, m_1^2 , s , m_2^2) \le 0 \f$
    """
    r1 = x * x * y + x * y * y + z * z * x + z * u * u + v * v * w + v * w * w
    r2 = x * y * w + x * u * v + y * z * w + y * u * w
    r3 = -x * y * (z + u + v + w)
    r4 = -z * y * (x + y + v + w)
    r5 = -v * w * (x + y + z + u)

    return 0.0 + r1 + r2 + r3 + r4 + r5


# =============================================================================
if '__main__' == __name__:

    import Ostap.Line
    logger.info(__file__ + '\n' + Ostap.Line.line)
    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(80 * '*')
    logger.info(' Author  : %s' % __author__)
    logger.info(' Version : %s' % __version__)
    logger.info(' Date    : %s' % __date__)
    logger.info(' Symbols : %s' % list(__all__))
    logger.info(80 * '*')

# =============================================================================
# The END
# =============================================================================
