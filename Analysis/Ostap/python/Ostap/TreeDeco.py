#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  Module with decoration of Tree/Chain objects for efficient use in python
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2011-06-07
#
# =============================================================================
"""Decoration of Tree/Chain objects for efficient use in python"""
# =============================================================================
__version__ = "$Revision$"
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2011-06-07"
__all__ = ()
# =============================================================================
import ROOT, cppyy  ## attention here!!
# =============================================================================
# logging
# =============================================================================
from Ostap.Logger import getLogger, multicolumn
if '__main__' == __name__: logger = getLogger('Ostap.TreeDeco')
else: logger = getLogger(__name__)
# =============================================================================
cpp = cppyy.makeNamespace('')
VE = cpp.Gaudi.Math.ValueWithError
# =============================================================================
logger.debug('Some useful decorations for Tree/Chain objects')
# =============================================================================
import Ostap.CutsDeco
from Ostap.Core import valid_pointer
# =============================================================================
_large = 2**63 - 1


# =============================================================================
## Iterator over ``good events'' in TTree/TChain:
#  @code
#    >>> tree = ... # get the tree
#    >>> for i in tree.withCuts ( 'pt>5' ) : print i.y
#  @endcode
#  @attention: TTree::GetEntry is already invoked for accepted events,
#              no need in second call
#  @see Analysis::PyIterator
#  @see Analysis::Formula
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-05-06
def _iter_cuts_(self, cuts, first=0, last=_large, progress=False):
    """Iterator over ``good events'' in TTree/TChain:

    >>> tree = ... # get the tree
    >>> for i in tree.withCuts ( 'pt>5' ) : print i.y

    Attention: TTree::GetEntry is already invoked for accepted events,
    no need in second call
    """
    #
    last = min(last, len(self))

    pit = cpp.Analysis.PyIterator(self, cuts, first, last)
    if not pit.ok(): raise TypeError("Invalid Formula: %s" % cuts)
    #
    from Ostap.progress_bar import ProgressBar
    with ProgressBar(
            min_value=first, max_value=last, silent=not progress) as bar:

        step = 13.0 * max(bar.width, 101) / (last - first)

        _t = pit.tree()
        _o = _t
        while _t:

            yield _t
            _t = pit.next()  ## advance to the next entry

            if progress:
                current = pit.current() - 1  ## get the current entry index
                if not _t                          \
                       or  _t != _o                \
                       or current - first   < 120  \
                       or last    - current < 120  \
                       or 0 == current % 100000    \
                       or 0 == int ( step * ( current - first ) ) % 5  :

                    ## show progress bar
                    bar.update_amount(current)
                    _o = _t

        if progress: bar.update_amount(last)

    del pit
    self.GetEntry(0)


ROOT.TTree.withCuts = _iter_cuts_
ROOT.TChain.withCuts = _iter_cuts_

ROOT.TTree.__len__ = lambda s: s.GetEntries()

## redefine
if not hasattr(ROOT.TTree, '_old_nonzero_'):
    ROOT.TTree._old_nonzero_ = ROOT.TTree.__nonzero__

    ## special version of ``nonzero'' for trees/chains
    def _rt_nonzero_(tree):
        """ Special version of ``nonzero'' for trees/chains
        - it tests validity of C++ pointer *and* size of the tree/chain
        >>> chain =
        >>> if not chain : print 'Problem here!'
        """
        return tree._old_nonzero_() and 0 < len(tree)

    ROOT.TTree.__nonzero__ = _rt_nonzero_

ROOT.TTree.__bool__ = ROOT.TTree.__nonzero__


# =============================================================================
## Iterator over ``good events'' in TTree/TChain:
#  @code
#    >>> tree = ... # get the tree
#    >>> for i in tree( 0, 100, 'pt>5' ) : print i.y
#  @endcode
#  @see Analysis::PyIterator
#  @see Analysis::Formula
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-05-06
def _tc_call_(self, first=0, last=_large, cuts=None, progress=False):
    """Iterator over ``good events'' in TTree/TChain:

    >>> tree = ... # get the tree
    >>> for i in tree(0, 100 , 'pt>5' ) : print i.y

    """
    #
    last = min(last, len(self))

    from Ostap.progress_bar import ProgressBar
    with ProgressBar(
            min_value=first, max_value=last, silent=not progress) as bar:

        step = 13.0 * max(bar.width, 101) / (last - first)

        pit = 1
        if cuts:

            pit = cpp.Analysis.PyIterator(self, cuts, first, last)
            if not pit.ok(): raise TypeError("Invalid Formula: %s" % cuts)
            #

            _t = pit.tree()
            _o = _t
            while _t:

                yield _t  ## YIELD
                _t = pit.next()  ## advance to the next entry

                if progress:
                    current = pit.current() - 1  ## get the current entry index
                    if not _t                          \
                           or  _t != _o                \
                           or current - first   < 120  \
                           or last    - current < 120  \
                           or 0 == current % 100000    \
                           or 0 == int ( step * ( current - first ) ) % 5  :

                        ## show progress bar
                        bar.update_amount(current)
                        _o = _t
        else:

            ## just explicit loop
            for current in range(first, last + 1):

                if progress:
                    if     current - first   < 120  \
                           or last - current < 120  \
                           or 0 == current % 100000 \
                           or 0 == int ( step * ( current - first ) ) % 5  :

                        bar.update_amount(current)

                if 0 >= self.GetEntry(current): break
                yield self  ## YIELD!

        if progress: bar.update_amount(last)

    del pit
    self.GetEntry(0)


ROOT.TTree.__call__ = _tc_call_
ROOT.TChain.__call__ = _tc_call_


# =============================================================================
## help project method for ROOT-trees and chains
#
#  @code
#    >>> h1   = ROOT.TH1D(... )
#    >>> tree.Project ( h1.GetName() , 'm', 'chi2<10' ) ## standart ROOT
#
#    >>> h1   = ROOT.TH1D(... )
#    >>> tree.project ( h1.GetName() , 'm', 'chi2<10' ) ## ditto
#
#    >>> h1   = ROOT.TH1D(... )
#    >>> tree.project ( h1           , 'm', 'chi2<10' ) ## use histo
#
#    ## make invididual projections of 'm1' and 'm2' and make a sum of distributions
#    >>> h1   = ROOT.TH1D(... )
#    >>> tree.project ( h1           , ['m1','m2'] , 'chi2<10' ) ## use histo
#
#    ## make invididual projections of 'm1' and 'm2' and make a sum of distributions
#    >>> h1   = ROOT.TH1D(... )
#    >>> tree.project ( h1           , "m1,m2"     , 'chi2<10' )
#    >>> tree.project ( h1           , "m1;m2"     , 'chi2<10' )
#  @endcode
#
#  @param tree   the tree
#  @param histo  the histogram or histogram name
#  @param what variable/expression to be projected.
#              It could be a list/tuple of variables/expressions or just a comma-separated expression
#  @param cuts expression for cuts/weights
#  @see TTree::Project
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-07-06
def _tt_project_(tree,
                 histo,
                 what,
                 cuts='',
                 option='',
                 nentries=_large,
                 firstentry=0,
                 silent=False):
    """Helper project method

    >>> tree = ...

    >>> h1   = ROOT.TH1D(... )
    >>> tree.Project ( h1.GetName() , 'm', 'chi2<10' ) ## standart ROOT

    >>> h1   = ROOT.TH1D(... )
    >>> tree.project ( h1.GetName() , 'm', 'chi2<10' ) ## ditto

    >>> h1   = ROOT.TH1D(... )
    >>> tree.project ( h1           ,  'm', 'chi2<10' ) ## use histo

    ## make invididual projections of m1 and m2 and make a sum of distributions
    >>> h1   = ROOT.TH1D(... )
    >>> tree.project ( h1           , ('m1','m2') , 'chi2<10' ) ## two variables
    >>> tree.project ( h1           , 'm1,m2'     , 'chi2<10' ) ## ditto
    >>> tree.project ( h1           , 'm1;m2'     , 'chi2<10' ) ## ditto

    - tree  : the tree
    - histo : the histogram (or histogram name)
    - what  : variable/expression to project. It can be expression or list/tuple of expression or comma (or semicolumn) separated expression
    - cuts  : selection criteria/weights
    """
    args = option, nentries, firstentry

    hname = histo
    if hasattr(histo, 'GetName'): hname = histo.GetName()
    ## elif isinstance ( histo , str       ) :
    ##    h = ROOT.gROOT.FindObject ( hname )
    ##    if h : histo = h

    ## reset it!
    if histo and isinstance(histo, ROOT.TH1): histo.Reset()
    #
    if isinstance(cuts, ROOT.TCut): cuts = str(cuts)
    if not what: return 0, histo
    #
    ## trivial 1-item list
    if hasattr(what, '__len__') and 1 == len(what) and not isinstance(
            what, (str, ROOT.TCut)):
        what = what[0]

    ## check for comma-separated list of expressions:
    if isinstance(what, str):
        what = what.split(',')
        if 1 == len(what): what = what[0]

    ## check for semicolumn-separated list of expressions:
    if isinstance(what, str):
        what = what.split(';')
        if 1 == len(what): what = what[0]

    #
    if isinstance(what, str): what = what
    elif isinstance(what, ROOT.TCut): what = str(what)
    elif isinstance(histo, ROOT.TH1):
        rr = 0
        hh = histo.clone()
        for v in what:
            r, h = _tt_project_(tree, hh, v, cuts, *args)
            rr += r
            histo += h
        hh.Delete()
        del hh
        return rr, histo
    elif isinstance(histo, str):
        ## process the head of the list: the first call creates the histo...
        rr, hh = _tt_project_(tree, histo, what[0], cuts, *args)
        histo = hh
        if 1 == len(what): return rr, histo
        # normal processing of the tail of the list using created historgams
        hh = histo.clone()
        r1, h1 = _tt_project_(tree, hh, what[1:], cuts, *args)
        rr += r1
        histo += h1
        hh.Delete()
        del hh, h1
        return rr, histo

    ## the basic case
    from Ostap.TFileDeco import ROOTCWD
    with ROOTCWD():
        ROOT.gROOT.cd()
        ## make projection
        result = tree.Project(hname, what, cuts, *args)
        if isinstance(histo, ROOT.TH1): return result, histo
        elif isinstance(histo, str):
            h = ROOT.gROOT.FindObject(hname)
            if h: return result, h

    return result, histo


for t in (ROOT.TTree, ROOT.TChain):
    ## add useful method
    t.project = _tt_project_
    ## to be redefined in Kisa
    if not hasattr(t, 'pproject'):
        ## do not redefine if already defined
        t.pproject = _tt_project_


# =============================================================================
## get the statistic for certain expression in Tree/Dataset
#  @code
#  tree  = ...
#  stat1 = tree.statVar( 'S_sw/effic' )
#  stat2 = tree.statVar( 'S_sw/effic' ,'pt>1000')
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-09-15
def _stat_var_(tree, expression, *cuts):
    """Get a statistic for the  expression in Tree/Dataset

    >>> tree  = ...
    >>> stat1 = tree.statVar ( 'S_sw/effic' )
    >>> stat2 = tree.statVar ( 'S_sw/effic' ,'pt>1000')

    """
    return cpp.Analysis.StatVar.statVar(tree, expression, *cuts)


ROOT.TTree.statVar = _stat_var_
ROOT.TChain.statVar = _stat_var_


# =============================================================================
## get the statistic for pair of expressions in Tree/Dataset
#  @code
#  tree  = ...
#  stat1 , stat2 , cov2 , len = tree.statCov( 'x' , 'y' )
#  # apply some cuts
#  stat1 , stat2 , cov2 , len = tree.statCov( 'x' , 'y' , 'z>0' )
#  # use only subset of events
#  stat1 , stat2 , cov2 , len = tree.statCov( 'x' , 'y' , 'z>0' , 100 , 10000 )
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-09-15
def _stat_cov_(tree, expression1, expression2, cuts='', *args):
    """Get the statistic for pair of expressions in Tree/Dataset

    >>>  tree  = ...
    >>>  stat1 , stat2 , cov2 , len = tree.statCov( 'x' , 'y' )

    Apply some cuts:
    >>> stat1 , stat2 , cov2 , len = tree.statCov( 'x' , 'y' , 'z>0' )

    Use only subset of events
    >>> stat1 , stat2 , cov2 , len = tree.statCov( 'x' , 'y' , 'z>0' , 100 , 10000 )
    """
    stat1 = cpp.Gaudi.Math.WStatEntity()
    stat2 = cpp.Gaudi.Math.WStatEntity()
    cov2 = cpp.Gaudi.Math.SymMatrix2x2()

    if cuts:
        length = cpp.Analysis.StatVar.statCov(tree, expression1, expression2,
                                              cuts, stat1, stat2, cov2, *args)
    else:
        length = cpp.Analysis.StatVar.statCov(tree, expression1, expression2,
                                              stat1, stat2, cov2, *args)

    return stat1, stat2, cov2, length


ROOT.TTree.statCov = _stat_cov_
ROOT.TChain.statCov = _stat_cov_


# =============================================================================
## Get min/max for the certain variable in chain/tree
#  @code
#  chain = ...
#  mn,mx = chain.vminmax('pt')
#  mn,mx = chain.vminmax('pt','y>3')
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2015-09-19
def _tc_minmax_(tree, var, cuts='', delta=0.0):
    """Get min/max for the certain variable in chain/tree
    >>> chain = ...
    >>> mn,mx = chain.vminmax('pt')
    >>> mn,mx = chain.vminmax('pt','y>3')
    """
    if cuts: s = tree.statVar(var, cuts)
    else: s = tree.statVar(var)
    mn, mx = s.minmax()
    if mn < mn and 0.0 < delta:
        dx = delta * 1.0 * (mx - mn)
        mx += dx
        mn -= dx
    return mn, mx


ROOT.TTree.vminmax = _tc_minmax_
ROOT.TChain.vminmax = _tc_minmax_


# =============================================================================
## make a sum over expression in Tree/Dataset
#
#  @code
#
#  >>> dataset = ...
#  ## get corrected number of events
#  >>> n_corr  = dataset.sumVar ( "S_sw/effic" )
#
#  @endcode
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-09-15
def _sum_var_old_(tree, expression):
    """Make a sum over expression in Tree/Dataset

    >>> dataset = ...
    ## get corrected number of signale events
    >>> n_corr  = dataset.sumVar ( 'S_sw/effic' )

    """
    from Ostap.Core import hID
    _h = ROOT.TH1D(hID(), '', 3, -1, 2)
    _h_one_.Sumw2()
    tree.project(_h, '1', expression)
    result = _h.accumulate()
    del _h
    return result


ROOT.TTree.sumVar_ = _sum_var_old_
ROOT.TChain.sumVar_ = _sum_var_old_


# =============================================================================
## make a sum over expression in Tree/Dataset
#
#  @code
#
#  >>> dataset = ...
#
#  ## get corrected number of events
#  >>> n_corr     = dataset.sumVar ( "S_sw/effic" )
#
#  ## get corrected number of events
#  >>> n_corr_pt  = dataset.sumVar ( "S_sw/effic" , 'pt>1')
#
#  @endcode
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-09-15
def _sum_var_(tree, expression, *cuts):
    """Make a sum over expression in Tree/Dataset

    >>> dataset = ...
    ## get corrected number of signal events
    >>> n_corr     = dataset.sumVar ( 'S_sw/effic' )

    ## get corrected number of signal events
    >>> n_corr_pt  = dataset.sumVar ( 'S_sw/effic' , 'pt>1')

    """
    w = tree.statVar(expression, *cuts)
    return VE(w.sum(), w.sum2())


ROOT.TTree.sumVar = _sum_var_
ROOT.TChain.sumVar = _sum_var_


# =============================================================================
## get the leaves for the given tree/chain
#  @see TTree
#  @code
#
#  >>> tree = ...
#  >>> lst = tree.leaves()
#  >>> for l in lst : print l
#
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-02-04
def _rt_leaves_(t, pattern='', *args):
    """ Get the list of leaves names

    >>> tree = ...
    >>> lst = tree.leaves()
    >>> for l in lst : print l
    >>> lst = tree.leaves( '.*(muon).*', re.I )
    >>> for l in lst : print l
    """
    vlst = t.GetListOfLeaves()

    if not vlst: return tuple()
    if pattern:
        try:
            import re
            c = re.compile(pattern, *args)
            lst = [v.GetName() for v in vlst if c.match(v.GetName())]
            lst.sort()
            return tuple(lst)
        except:
            logger.error('leaves: exception is caught, skip it', exc_info=True)

    lst = [v.GetName() for v in vlst]
    lst.sort()
    return tuple(lst)

    _lst = [l.GetName() for l in _lst]
    _lst.sort()
    return tuple(_lst)


ROOT.TTree.leaves = _rt_leaves_


# ==============================================================================
## Get the leaf with the certain name
def _rt_leaf_(tree, leaf):
    """Get the leaf with certain name:
    >>> tree = ...
    >>> l = tree.leaf('pt')
    """
    lst = tree.GetListOfLeaves()
    for i in lst:
        if leaf == i.GetName(): return i
    return None


ROOT.TTree.leaf = _rt_leaf_


# =============================================================================
## get the branches for the given tree/chain
#  @see TTree
#  @code
#  >>> tree = ...
#  >>> lst = tree.branches()
#  >>> for b in lst : print b
#  >>> lst = tree.branches( '.*(Muon).*' , re.I )
#  >>> for b in lst : print b
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-02-04
def _rt_branches_(t, pattern='', *args):
    """Get the list of branch names

    >>> tree = ...
    >>> lst = tree.branches()
    >>> for b in lst : print b
    >>> lst = tree.branches( '.*(Muon).*' , re.I )
    >>> for b in lst : print b

    """
    vlst = t.GetListOfBranches()
    if not vlst: return tuple()

    if pattern:
        try:
            import re
            c = re.compile(pattern, *args)
            lst = [v.GetName() for v in vlst if c.match(v.GetName())]
            lst.sort()
            return tuple(lst)
        except:
            logger.error(
                'branches: exception is caught, skip it', exc_info=True)

    lst = [v.GetName() for v in vlst]
    lst.sort()
    return tuple(lst)


ROOT.TTree.branches = _rt_branches_

# =============================================================================

__std_ints = ('char', 'short', 'int', 'long', 'long long')
__std_uints = tuple(['unsigned ' + i for i in __std_ints])
__std_types = ('bool', 'float', 'double',
               'long double') + __std_ints + __std_uints
__scalars = __std_types + ('Bool_t', 'Char_t', 'UChar_t', 'Short_t',
                           'UShort_t', 'Int_t', 'UInt_t', 'Float_t',
                           'Double_t', 'Long64_t', 'ULong64_t')
__vectors = tuple(['vector<' + i + '>' for i in __scalars])
__types = list(__scalars) + list(__vectors)
tmp = set()
for t in __types:
    while 0 <= t.find(2 * ' '):
        t = t.replace(2 * ' ', ' ')
    tmp.add(t)
__types = tuple(tmp)
del tmp


def __in_types(t):
    while 0 <= t.find(2 * ' '):
        t = t.replace(2 * ' ', ' ')
    return t in __types


# ==============================================================================
## print tree as table
def _rt_table_0_(tree, pattern=None, cuts='', *args):
    """
    """
    ## get list of branches
    brs = tree.leaves(pattern)
    if 'TObject' in brs:
        brs = list(brs)
        brs.remove('TObject')
        brs = tuple(brs)

    ## collect information
    _vars = []
    s0 = tree.statVar('1', cuts, *args)
    n0 = s0.nEntries()

    ## no entries passed the cuts
    brs = () if 0 == n0 else brs

    for b in brs:

        l = tree.leaf(b)

        if not l:
            logger.warning("table: can't get the leaf  \"%s\"" % b)
            continue

        tn = l.GetTypeName()
        typename = tn

        br = l.GetBranch()
        if br:
            n = br.GetTitle()
            typename = '%s %s '
            p = n.find('[')
            if 0 <= p:
                p2 = n.find('/', p + 1)
                if p < p2: typename = '%s %s' % (tn, n[p:p2])
                else: typename = '%s %s' % (tn, n[p:])
            else: typename = '%s' % tn

        rr = [b, typename]

        if __in_types(tn):

            try:
                s = tree.statVar(b, cuts, *args)
                n = s.nEntries()
                mnmx = s.minmax()
                mean = s.mean()
                rms = s.rms()
                rr += [
                    ('%+.5g' % mean.value()).strip(),  ## 2
                    ('%.5g' % rms).strip(),  ## 3
                    ('%+.5g' % mnmx[0]).strip(),  ## 4
                    ('%+.5g' % mnmx[1]).strip(),  ## 5
                    '' if n == n0 else '%.3g' % (float(n) / n0)
                ]  ## 6

            except:
                logger.warning("table: can't get info for the leaf \"%s\"" % b)
                rr += 5 * ['']

        else:
            rr += ['-', '-', '-', '-', '']

        _vars.append(tuple(rr))

    _vars.sort()
    name_l = len('Variable')
    mean_l = len('mean')
    rms_l = len('rms')
    min_l = len('min')
    max_l = len('max')
    num_l = len('#')
    type_l = len('type')
    for v in _vars:
        name_l = max(name_l, len(v[0]))
        type_l = max(type_l, len(v[1]))
        mean_l = max(mean_l, len(v[2]))
        rms_l = max(rms_l, len(v[3]))
        min_l = max(min_l, len(v[4]))
        max_l = max(max_l, len(v[5]))
        num_l = max(num_l, len(v[6]))

    __vars = []
    for v in _vars:
        if not ' ' in v[1]:
            __vars.append(v)
            continue
        tn = v[1]
        cl = len(tn)
        ml = type_l
        vv = list(v)
        vv[1] = tn.replace(' ', (ml + 1 - cl) * ' ', 1)
        vv = tuple(vv)
        __vars.append(vv)

    _vars = __vars

    sep = '# +%s+%s+%s+%s+%s+' % ((name_l + 2) * '-', (type_l + 2) * '-',
                                  (mean_l + rms_l + 5) * '-',
                                  (min_l + max_l + 5) * '-', (num_l + 2) * '-')
    fmt = '# | %%-%ds | %%-%ds | %%%ds / %%-%ds | %%%ds / %%-%ds | %%%ds |' % (
        name_l, type_l, mean_l, rms_l, min_l, max_l, num_l)

    from Ostap.Logger import allright, attention
    report = '# %s("%s","%s"' % (tree.__class__.__name__, tree.GetName(),
                                 tree.GetTitle())
    if tree.GetDirectory(): report += ',%s' % tree.GetDirectory().GetName()
    report += ')'
    if tree.topdir: report += '\n# top-dir:%s' % tree.topdir.GetName()
    report += '\n# ' + allright(
        '%d entries; %d/%d variables (selected/total)' %
        (len(tree), len(_vars), len(tree.branches())))

    header = fmt % ('Variable', 'type', 'mean', 'rms', 'min', 'max', '#')

    report += '\n' + sep
    report += '\n' + header
    report += '\n' + sep
    for v in _vars:
        line = fmt % (v[0], v[1], v[2], v[3], v[4], v[5], v[6])
        report += '\n' + line
    report += '\n' + sep

    return report, len(sep)


# ==============================================================================
## print rot-tree in a form of the table
#  @code
#  data = ...
#  print dat.table()
#  @endcode
def _rt_table_(dataset, variables=[], cuts='', *args):
    """print dataset in a form of the table
    >>> dataset = ...
    >>> print dataset.table()
    """
    return _rt_table_0_(dataset, variables, cuts, *args)[0]


# =============================================================================
## simplified printout for TTree/TChain
#  @see TTree
#  @code
#
#  >>> tree = ...
#  >>> print tree.pprint()
#
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-02-04
def _rt_print_(t):
    """Simplified print out for tree/chain

    >>> tree = ...
    >>> print tree.pprint()
    """
    ##
    res = "Name: %s Enries/#%d" % (t.GetName(), t.GetEntries())
    if hasattr(t, 'GetNtrees'): res += " Chain/#%d " % t.GetNtrees()
    ##
    _l = t.leaves()
    res += "\nLeaves:\n%s" % multicolumn(list(_l), indent=2, pad=1)

    ## collect non-trivial branches
    _b = t.branches()

    _bs = set(_b)
    _ls = set(_l)
    _b = list(_bs - _ls)
    _b.sort()
    if _b:
        res += "\nNon-trivial branches:\n%s" % multicolumn(_b, indent=2, pad=1)

    return res.replace('\n', '\n# ')


# =============================================================================
##  print DataSet
def _rt_print2_(data):
    """Print TTree/TChain"""

    br = len(data.branches())
    l = len(data)
    if 10000000 < br * l: return _rt_print_(data)

    from Ostap.Logger import terminal_size, isatty
    if not isatty(): return _rt_table_(data)
    th, tw = terminal_size()
    rep, wid = _rt_table_0_(data)
    if wid < tw: return rep
    ##
    return _rt_print_(data)


ROOT.TTree.__repr__ = _rt_print2_
ROOT.TTree.__str__ = _rt_print2_
ROOT.TTree.table = _rt_table_
ROOT.TTree.pprint = _rt_print_


# =============================================================================
## get lst of files used for the given chain
#  @code
#
#  >>> chain = ... ## get the files
#  >>> files = chain.files()
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-02-04
def _rc_files_(chain):
    """Get the list of files used for the chain

    >>> chain = ... ## get the files
    >>> files = chain.files()
    """
    lst = chain.GetListOfFiles()
    return [i.GetTitle() for i in lst]


ROOT.TChain.files = _rc_files_


# =============================================================================
## get the chain of reduced size (in terms of number of input files)
#  @code
#  chain = ...
#  new_chain = chain[1:3] ## keep only files 1-3
#  print len(chain), len(new_chain)
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2016-03-17
def _rc_getslice_(self, start, stop, *step):
    """ Get the chain of reduced size (in terms of number of input files)
    >>> chain = ...
    >>> new_chain = chain[1:3] ## keep pnly files 1-3
    >>> print len(chain), len(new_chain)
    """
    _files = self.files()
    ## get slice
    _files = _files[slice(start, stop, *step)]
    _chain = ROOT.TChain(self.GetName(), self.GetTitle())
    for _f in _files:
        _chain.Add(_f)
    return _chain


ROOT.TChain.__getslice__ = _rc_getslice_


# =============================================================================
## get "slice" from TTree in a form of numpy.array
#  @code
#  tree = ...
#  varr = tree.slice('Pt','eta>3')
#  print varr
#  @endcode
#  @see numpy.array
#  @author Albert BURSCHE
#  @date 2015-07-08
def _rt_slice_(tree, varname, cut=''):
    """ Get ``slice'' from TTree in a form of numpy.array
    ##
    >>> tree = ...
    >>> varr = tree.slice('Pt','eta>3')
    >>> print varr
    """
    #
    ## decode the name (if needed)
    if isinstance(varname, str):
        varname = varname.strip()
        varname = varname.replace(':', ',')
        varname = varname.replace(';', ',')
        varname = varname.replace(' ', ',')
        varname = varname.split(',')
        if 1 == len(varname): varname = varname[0].strip()
        else:
            for i in range(0, len(varname)):
                varname[i] = varname[i].strip()
    #
    if isinstance(varname, (list, tuple)):
        ## forward to appropriate method
        return tree.slices(varname, cut)
    elif not isinstance(varname, str):
        raise AttibuteError('Invalid type %s' % varname)

    ##
    p1 = varname.find('[')
    if 0 < p1:
        p2 = varname.find(']', p1 + 1)
        if p1 < p2:
            raise AttributeError(
                "TTree:slice: can't slice array-like variable '%s'" % varname)

    ge = long(tree.GetEstimate())
    tree.SetEstimate(max(len(tree), ge))
    ##
    n = tree.Draw(varname, cut, "goff")
    ##
    import numpy
    sl = numpy.array(numpy.frombuffer(tree.GetV1(), count=n), copy=True)
    ##
    tree.SetEstimate(ge)
    return sl


# =============================================================================
## get "slices" from TTree in a form of numpy.array
#  @code
#  tree = ...
#  varrs1 = tree.slices ( ['Pt','eta'] , 'eta>3' )
#  print varrs1
#  varrs2 = tree.slices (  'Pt , eta'  , 'eta>3' )
#  print varrs2
#  varrs3 = tree.slices (  'Pt : eta'  , 'eta>3' )
#  print varrs3
#  @endcode
#  @see numpy.array
#  @author Albert BURSCHE
#  @date 2015-07-08
def _rt_slices_(tree, varnames, cut=''):
    """ Get ``slices'' from TTree in a form of numpy.array

    >>> tree = ...

    >>> varrs1 = tree.slices( ['Pt' , 'eta'] ,'eta>3')
    >>> print varrs1

    >>> varrs2 = tree.slices( 'Pt,eta'  ,'eta>3')
    >>> print varrs2

    >>> varrs3 = tree.slices( 'Pt : eta' ,'eta>3')
    >>> print varrs3
    """
    #
    varname = varnames
    ## decode the name (if needed)
    for sep in (',', ':', ';'):
        if isinstance(varname, str):
            varname = varname.strip()
            varname = varname.split(sep)
            if 1 == len(varname): varname = varname[0].strip()
            else:
                for i in range(0, len(varname)):
                    varname[i] = varname[i].strip()
    #
    if isinstance(varname, str):
        ## forward to appropriate method
        return tree.slice(varname, cut)
    elif not isinstance(varname, (list, tuple)):
        raise AttibuteError('Invalid type %s' % varname)
    ##
    import numpy
    a = numpy.array([tree.slice(name, cut) for name in varname])
    a.sort()
    return a


ROOT.TTree.slice = _rt_slice_
ROOT.TTree.slices = _rt_slices_


def _not_implemented_(self, method, *args, **kwargs):
    raise NotImplementedError(
        '%s: the method "%s" is not implemented' % (self.__class__, method))


ROOT.TChain.slice = lambda s, *x: _not_implemented_(s, 'slice', *x)
ROOT.TChain.slices = lambda s, *x: _not_implemented_(s, 'slices', *x)


# =============================================================================
## extending the existing chain
def _tc_iadd_(self, other):
    """ Add elements (files,  chains) to existing chain
    >>>  chain  = ...
    >>>  chain += 'myfile.root'
    >>>  chain += ( 'myfile1.root' , 'myfile2.root' )
    """
    if self == other: return self
    elif isinstance(o, (list, tuple, set)):
        for f in other:
            _tc_iadd_(self, f)
        return self

    elif isinstance(other, ROOT.TChain):
        return _tc_iadd_(self, other.files())

    elif isinstance(other, str):
        if not other in self.files(): self.Add(other)
        return self

    return NotImplemented


# =============================================================================
## summing two existing chains
def _tc_add_(self, other):
    """ Add two  chains together
    >>>  chain1 = ...
    >>>  chain2 = ...
    >>>  chain3 =  chain1         + chain2
    >>>  chain4 =  chain1         + 'my_file.root'
    >>>  chain5 =  chain1         + ( 'my_file1.root' , 'my_file2.root' )
    >>>  chain6 =  'my_file.root' + chain2
    """
    left = ROOT.TChain(self.GetName())
    left += self
    left += other
    return left


ROOT.TChain.__iadd__ = _tc_iadd_
ROOT.TChain.__add__ = _tc_add_
ROOT.TChain.__radd__ = _tc_add_

# =============================================================================
from Ostap.Utils import CleanUp


# =============================================================================
## @class Chain
#  simple class to keep pickable definitinon of tree/chain
#  it is needed for multiprcessing
class Chain(CleanUp):
    """Simple class to keep definition of tree/chain
    """

    def __getstate__(self):
        return {
            'name': self.__name,
            'files': self.__files,
            'first': self.__first,
            'nevents': self.__nevents
        }

    def __setstate__(self, state):
        self.__name = state['name']
        self.__files = state['files']
        self.__first = state['first']
        self.__nevents = state['nevents']
        ## reconstruct the chain
        self.__chain = ROOT.TChain(self.__name)
        for f in self.__files:
            self.__chain.Add(f)

    def __init__(self, tree=None, name=None, files=[], first=0, nevents=-1):

        assert (name and files) or valid_pointer(
            tree), 'Invalid arguments %s/%s/%s' % (tree, name, files)
        assert isinstance(first,
                          int) and 0 <= first, "Invalid ``first'' %s" % first

        self.__first = int(first)
        self.__nevents = nevents if 0 <= nevents < ROOT.TChain.kMaxEntries else -1

        if files and isinstance(files, str): files = files,

        if name and files:

            self.__name = name
            self.__files = files

            chain = self.__create_chain()
            assert valid_pointer(chain), 'Invalid TChain!'
            assert len(files) == len(chain.files()), 'Invalid length of files'

            self.__chain = chain

        elif valid_pointer(tree):

            self.__name = tree.GetName()

            if isinstance(tree, ROOT.TChain):

                self.__files = tree.files()
                self.__files = tuple(self.__files)
                self.__chain = tree

            elif isinstance(tree, ROOT.TTree):

                topdir = tree.topdir
                if isinstance(topdir, ROOT.TFile):
                    self.__files = topdir.GetName(),

                else:

                    fname = CleanUp.tempfile(suffix='.root', prefix='tree_')
                    from Ostap.Core import ROOTCWD
                    with ROOTCWD():
                        import Ostap.TFileDeco
                        with ROOT.TFile(fname, 'NEW') as rfile:
                            rfile.cd()
                            tname = tree.GetName()
                            rfile[tname] = tree
                            rfile.ls()
                    self.__files = fname,
                    self.tmpfiles += [fname]

                chain = ROOT.TChain(tree.GetName())
                chain.Add(self.__files[0])
                tmp = chain
                assert len(chain) == len(
                    tree), 'Something wrong happens here :-( '
                self.__chain = chain

        ##  last adjustment
        ll = len(self)
        if ll < self.__first:
            logger.warning(
                'Tree/Chain will be empty %s/%s' % (self.__first, ll))
            self.__first = ll
            self.__nevents = 0

        ## if number of events is specified:
        if 0 < self.__nevents:
            self.__nevents = min(self.__nevents, ll - self.__first)

    ## split the chain for several chains  with max=chunk entries
    def split(self, chunk_size=200000):
        """Split the tree for several trees with chunk_size entries
        >>> tree = ....
        >>> trees = tree.split ( chunk_size = 1000000 )
        """

        trees = []

        ievt = 0
        nevt = 0

        for f in self.__files:

            if 0 <= self.__nevents and self.__nevents <= nevt: break  ## BREAK

            ## get the length of the current tree
            tt = Tree(name=self.name, file=f)
            ll = len(tt)
            if ievt + ll < self.__first: continue  ## CONTINUE

            ##
            first = self.__first - ievt if ievt <= self.__first else 0
            nevents = -1
            if 0 <= self.__nevents and self.__nevents < nevt + ll:
                nevents = self.__nevents - nevt
            t = Tree(
                tt.chain, name=self.name, file=f, first=first, nevents=nevents)
            trees += list(t.split(chunk_size))

            ievt += ll
            nevt += nevents if 0 <= nevents else ll

        return tuple(trees)

    ##  number of entries in the Tree/Chain
    def __len__(self):
        return len(self.__chain)

    def __create_chain(self):
        """``chain'' : get the undnerlyinng tree/chain"""
        c = ROOT.TChain(self.__name)
        for f in self.__files:
            c.Add(f)
        return c

    @property
    def chain(self):
        """``chain'' : get the undnerlying tree/chain"""
        if self.__chain: return self.__chain
        self.__chain = self.__create_chain()
        return self.__chain

    @property
    def name(self):
        """``name''   : TTree/TChain name"""
        return self.__name

    @property
    def files(self):
        """``files''   : the files"""
        return tuple(self.__files)

    @property
    def first(self):
        """``first'' : the first event to process"""
        return self.__first

    @property
    def last(self):
        """``last'' : the last event (not-inclusive)"""
        ll = len(self)
        return self.__first + min(ll, self.nevents)

    @property
    def nevents(self):
        """``nevents'' : number of events to process"""
        return self.__nevents if 0 <= self.__nevents else ROOT.TChain.kMaxEntries

    def __str__(self):
        r = "Chain('%s',%s" % (self.name, self.files)
        if 0 != self.__first or 0 <= self.nevents:
            r += ",%s,%s" % (self.first, self.last - self.first)
        return r + ")"

    __repr__ = __str__


# =============================================================================
## @class Tree
#  simple class to keep 'persistent' definition of the tree
#  it is needed for multiprocessing
class Tree(Chain):
    """Simple class to keep definition of tree/chain
    """

    def __getstate__(self):
        return Chain.__getstate__(self)

    def __setstate__(self, state):
        Chain.__setstate__(self, state)

    def __init__(self, tree=None, name=None, file='', first=0, nevents=-1):

        if name and file:

            assert isinstance(file, str), '"File should be single file name!"'

        elif valid_pointer(tree):

            if isinstance(tree, ROOT.TChain):
                assert 1 == len(tree.files()), 'Tree is for ROOT.TTree only!'

        Chain.__init__(
            self, tree, name, files=[file], first=first, nevents=nevents)

        assert 1 == len(self.files), 'Invalid number of files!'

    ## split the tree for several trees with max=chunk entries
    def split(self, chunk_size=200000):
        """Split the tree for several trees with max=chunk entries
        >>> tree = ....
        >>> trees = tree.split ( chunk_size = 1000000 )
        """

        if 0 == self.nevents: return ()

        assert isinstance(
            chunk_size, int), "Illegal type of ``chunk_size'' %s" % chunk_size

        ## no splitting ?
        if 0 >= chunk_size: return self,

        cs = chunk_size

        total = min(len(self), self.nevents)
        nchunks, rest = divmod(total, cs)

        results = []
        for i in range(nchunks):
            results.append(
                Tree(
                    name=self.name,
                    file=self.file,
                    first=self.first + i * cs,
                    nevents=cs))
        if rest:
            results.append(
                Tree(
                    name=self.name,
                    file=self.file,
                    first=self.first + nchunks * cs,
                    nevents=rest))

        return tuple(results)

    @property
    def file(self):
        """``file''   : the file name """
        assert 1 == len(
            self.files), 'Invalid number of files %s' % len(self.files)
        return self.files[0]

    def __str__(self):
        r = "Tree('%s',%s" % (self.name, self.file)
        if 0 != self.first or 0 <= self.nevents:
            r += ",%s,%s" % (self.first, self.last - self.first)
        return r + ")"

    __repr__ = __str__


# =============================================================================


# =============================================================================
## @class FuncTree
#  Helper class to implement "TTree-function"
#  @see Analysis::Functions::PyFuncTree
class FuncTree(cpp.Analysis.Functions.PyFuncTree):
    """Helper class to implement ``TTree-function''
    """

    def __init__(self, tree=None):
        ## initialize the base class
        cpp.Analysis.Functions.PyFuncTree.__init__(self, data)

    ## the main method
    def evaluate(self):
        tree = self.tree
        assert valid_pointer(tree), 'Invalid TTree object'
        return -1


# =============================================================================
## @class FuncData
#  Helper class to implement "RooAbsData-function"
#  @see analysis::Functions::PyFuncData
class FuncData(cpp.Analysis.Functions.PyFuncData):
    """Helper class to implement ``TTree-function''
    """

    def __init__(self, data=None):
        ## initialize the base class
        cpp.Analysis.Functions.PyFuncData.__init__(self, data)

    ## the main method
    def evaluate(self):
        data = self.data
        assert valid_pointer(data), 'Invalid RooAbsData object'
        return -1


# ==============================================================================
## @class FormulaFunc
#  simple  class that uses Ostap::Formula/TTreeFormula to get ``TTree-function''
#  @see Analysis::Formula
#  @see TTreeFormula
#  @see Analysis::Functions::FuncFormula
class FormulaFunc(cpp.Analysis.Functions.FuncFormula):
    def __init__(self, expression, tree=None, name=''):
        cpp.Analysis.Functions.FuncFormula.__init__(self, expression, tree,
                                                    name)


# ==============================================================================
## @class RooFormulaFunc
#  simple  class that uses RooFormulaVar to get ``RooAbsData-function''
#  @see RooFormulaVar
#  @see Analysis::Functions::FuncRooFormula
class RooFormulaFunc(cpp.Analysis.Functions.FuncFormula):
    def __init__(self, expression, tree=None, name=''):
        cpp.Analysis.Functions.FuncRooFormula.__init__(self, expression, tree,
                                                       name)


# ==================================================================================
## @class H1DFunc
#  Simple  class to use 1D-histogram  as tree-function
#  @see Analysis::Functions::FuncTH1
class H1DFunc(cpp.Analysis.Functions.FuncTH1):
    """Simple  class to use 1D-histogram  as tree-function
    """

    def __init__(
            self,
            histo,
            xvar,  ## x-axis
            tx=cpp.Gaudi.Math.HistoInterpolation.Cubic,
            edges=True,
            extrapolate=False,
            density=False,
            tree=None):

        cpp.Analysis.Functions.FuncTH1.__init__(self, histo, xvar, tree, tx,
                                                edges, extrapolate, density)


# ==================================================================================
## @class H2DFunc
#  Simple  class to use 2D-histogram  as tree-function
#  @see Analysis::Functions::FuncTH2
class H2DFunc(cpp.Analysis.Functions.FuncTH2):
    """Simple  class to use 2D-histogram  as tree-function
    """

    def __init__(
            self,
            histo,
            xvar,  ## x-axis
            yvar,  ## y-axis
            tx=cpp.Gaudi.Math.HistoInterpolation.Cubic,
            ty=cpp.Gaudi.Math.HistoInterpolation.Cubic,
            edges=True,
            extrapolate=False,
            density=False,
            tree=None):

        cpp.Analysis.Functions.FuncTH2.__init__(
            self, histo, xvar, yvar, tree, tx, ty, edges, extrapolate, density)


# ==================================================================================
## @class H3DFunc
#  Simple  class to use 3D-histogram  as tree-function
#  @see Analysis::Functions::FuncTH3
class H3DFunc(cpp.Analysis.Functions.FuncTH3):
    """Simple  class to use 3D-histogram  as tree-function
    """

    def __init__(
            self,
            histo,
            xvar,  ## x-axis
            yvar,  ## y-axis
            zvar,  ## y-axis
            tx=cpp.Gaudi.Math.HistoInterpolation.Cubic,
            ty=cpp.Gaudi.Math.HistoInterpolation.Cubic,
            tz=cpp.Gaudi.Math.HistoInterpolation.Cubic,
            edges=True,
            extrapolate=False,
            density=False,
            tree=None):

        cpp.Analysis.Functions.FuncTH3.__init__(self, histo, xvar, yvar, zvar,
                                                tree, tx, ty, tz, edges,
                                                extrapolate, density)


from Ostap.TFileDeco import top_dir
ROOT.TTree.topdir = property(top_dir, None, None)


# ==============================================================================
## get the moment of order 'order' relative to 'center'
#  @code
#  data =  ...
#  print data.get_moment (  3 , 0.0 , 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::get_moment
def _data_get_moment_(data, order, center, expression, cuts='', *args):
    """Get the moment of order 'order' relative to 'center'
    >>> data =  ...
    >>> print data.get_moment (  3 , 0.0 , 'mass' , 'pt>1' )
    - see Analysis::StatVar::get_moment
    """
    assert isinstance(order, int) and 0 <= order, 'Invalid order  %s' % order
    return cpp.Analysis.StatVar.get_moment(data, order, expression, center,
                                           cuts, *args)


# =============================================================================
## get the moment (with uncertainty) of order 'order'
#  @code
#  data =  ...
#  print data.moment ( 3 , 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::moment
def _data_moment_(data, order, expression, cuts='', *args):
    """Get the moment of order 'order' relative to 'center'
    >>> data =  ...
    >>> print data.moment (  3 , 'mass' , 'pt>1' )
    - see Analysis::StatVar::moment
    """
    assert isinstance(order, int) and 0 <= order, 'Invalid order  %s' % order
    return cpp.Analysis.StatVar.moment(data, order, expression, cuts, *args)


# =============================================================================
## get the central moment (with uncertainty) of order 'order'
#  @code
#  data =  ...
#  print data.central_moment ( 3 , 'mass' , 'pt>1' )
#  @endcode
#  - The moments are calculated with  O(1/n) precision
#  - For 3rd and  4th moments,  explicit  correction is applied
#  - Uncertainty is calculated with O(1/n^2) precision
#  @see Analysis::StatVar::central_moment
def _data_central_moment_(data, order, expression, cuts='', *args):
    """Get the moment of order 'order' relative to 'center'
    >>> tree =  ...
    >>> print tree.central_moment (  3 , 'mass' , 'pt>1' )
    - The moments are calculated with  O(1/n) precision
    - For 3rd and  4th moments,  explicit  correction is applied
    - Uncertainty is calculated with O(1/n^2) precision
    - see Analysis::StatVar::central_moment
    """
    assert isinstance(order, int) and 0 <= order, 'Invalid order  %s' % order
    return cpp.Analysis.StatVar.central_moment(data, order, expression, cuts,
                                               *args)


# =============================================================================
## get the  skewness (with uncertainty)
#  @code
#  data =  ...
#  print data.skewness ( 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::skewness
def _data_skewness_(data, expression, cuts='', *args):
    """Get the  skewness
    >>> data =  ...
    >>> print data.skewness ( 'mass' , 'pt>1' )
    - see Analysis::StatVar::skewness
    """
    return cpp.Analysis.StatVar.skewness(data, expression, cuts, *args)


# =============================================================================
## get the (excess) kurtosis (with uncertainty)
#  @code
#  data =  ...
#  print data.kustosis ( 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::kurtosis
def _data_kurtosis_(data, expression, cuts='', *args):
    """Get the kurtosis
    >>> data =  ...
    >>> print data.kurtosis ( 'mass' , 'pt>1' )
    - see Analysis::StatVar::kurtosis
    """
    return cpp.Analysis.StatVar.kurtosis(data, expression, cuts, *args)


# =============================================================================
## get the  quantile
#  @code
#  data =  ...
#  print data.quantile ( 0.10 , 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::quantile
def _data_quantile_(data, q, expression, cuts='', *args):
    """Get the quantile
    >>> data =  ...
    >>> print data.quantile ( 0.1 , 'mass' , 'pt>1' )
    - see Analysis::StatVar::quantile
    """
    assert isinstance(q, float) and 0 < q < 1, 'Invalid quantile:%s' % q
    return cpp.Analysis.StatVar.quantile(data, q, expression, cuts, *args)


# =============================================================================
## get the interval
#  @code
#  data =  ...
#  print data.interval ( 0.05, 0.95 , 'mass' , 'pt>1' ) ##   get 90% interval
#  @endcode
#  @see Analysis::StatVar::interval
def _data_interval_(data, qmin, qmax, expression, cuts='', *args):
    """Get the interval
    >>> data =  ...
    >>> print data.interval ( 0.05 , 0.95 , 'mass' , 'pt>1' ) ## get 90% interval
    - see Analysis::StatVar::interval
    """
    assert isinstance(qmin,
                      float) and 0 < qmin < 1, 'Invalid quantile-1:%s' % qmin
    assert isinstance(qmax,
                      float) and 0 < qmax < 1, 'Invalid quantile-2:%s' % qmax
    r = cpp.Analysis.StatVar.interval(data, min(qmin, qmax), max(qmin, qmax),
                                      expression, cuts, *args)
    return r.first, r.second


# =============================================================================
## Get the median
#  @code
#  data =  ...
#  print data.median ( 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::quantile
def _data_median_(data, expression, cuts='', *args):
    """Get the median
    >>> data =  ...
    >>> print data.median ( 'mass' , 'pt>1' )
    - see Analysis::StatVar::quantile
    """
    return _data_quantile_(data, 0.5, expression, cuts, *args)


# =============================================================================
## get the  quantiles
#  @code
#  data =  ...
#  print data.quantiles ( [0.1,0.3,0.5] , 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::quantile
def _data_quantiles_(data, quantiles, expression, cuts='', *args):
    """Get the quantiles
    >>> data =  ...
    >>> print data.quantiles ( 0.1       , 'mass' , 'pt>1' ) ## quantile
    >>> print data.quantiles ( (0.1,0.5) , 'mass' , 'pt>1' )
    >>> print data.quantiles ( 10        , 'mass' , 'pt>1' ) ## deciles!
    - see Analysis::StatVar::quantile
    """
    if isinstance(quantiles, float) and 0 < quantiles < 1:
        quantiles = [quantiles]
    elif isinstance(quantiles, int) and 1 < quantiles:
        N = quantiles
        quantiles = (float(i) / N for i in xrange(1, N))

    qq = []
    for q in quantiles:
        assert isinstance(q, float) and 0 < q < 1, 'Invalid quantile:%s' % q
        qq.append(q)
    qq.sort()
    from LHCbMath.Types import doubles
    rr = cpp.Analysis.StatVar.quantiles(data, doubles(qq), expression, cuts,
                                        *args)
    rr = [r for r in rr]
    return tuple(rr)


# =============================================================================
## Get the terciles
#  @code
#  data =  ...
#  print data.terciles( 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::quantile
def _data_terciles_(data, expression, cuts='', *args):
    """Get the terciles
    >>> data =  ...
    >>> print data.terciles ( 'mass' , 'pt>1' )
    - see Analysis::StatVar::quantile
    """
    return _data_quantiles_(data, 3, expression, cuts, *args)


# =============================================================================
## Get the quartiles
#  @code
#  data =  ...
#  print data.quarticles( 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::quantile
def _data_quartiles_(data, expression, cuts='', *args):
    """Get the quartiles
    >>> data =  ...
    >>> print data.quartiles ( 'mass' , 'pt>1' )
    - see Analysis::StatVar::quantile
    """
    return _data_quantiles_(data, 4, expression, cuts, *args)


# =============================================================================
## Get the quintiles
#  @code
#  data =  ...
#  print data.quintiles( 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::quantile
def _data_quintiles_(data, expression, cuts='', *args):
    """Get the quartiles
    >>> data =  ...
    >>> print data.quartiles ( 'mass' , 'pt>1' )
    - see Analysis::StatVar::quantile
    """
    return _data_quantiles_(data, 5, expression, cuts, *args)


# =============================================================================
## Get the deciles
#  @code
#  data =  ...
#  print data.deciles( 'mass' , 'pt>1' )
#  @endcode
#  @see Analysis::StatVar::quantile
def _data_deciles_(data, expression, cuts='', *args):
    """Get the deciles
    >>> data =  ...
    >>> print data.deciles ( 'mass' , 'pt>1' )
    - see Analysis::StatVar::quantile
    """
    return _data_quantiles_(data, 10, expression, cuts, *args)


# =============================================================================
## Get the mean (with uncertainty):
#  @code
#  tree = ...
#  tree.mean('mass*mass', 'pt>0')
#  @endcode
#  @see Analysis::StatVar::moment
def _data_mean_(data, expression, cuts='', *args):
    """Get the   mean (with uncertainty):
    >>> data = ...
    >>> data.mean('mass*mass', 'pt>0')
    """
    return _data_moment_(data, 1, expression, cuts, *args)


# =============================================================================
## Get the rms(with uncertainty):
#  @code
#  data = ...
#  data.rms('mass*mass', 'pt>0')
#  @endcode
#  @see Analysis::StatVar::central_moment
def _data_rms_(data, expression, cuts='', *args):
    """Get the rms (with uncertainty):
    >>> data = ...
    >>> data.rms('mass*mass', 'pt>0')
    """
    return _data_central_moment_(data, 2, expression, cuts, *args)**0.5


_data_get_moment_.__doc__ += '\n' + cpp.Analysis.StatVar.get_moment.__doc__
_data_moment_.__doc__ += '\n' + cpp.Analysis.StatVar.moment.__doc__
_data_central_moment_.__doc__ += '\n' + cpp.Analysis.StatVar.central_moment.__doc__
_data_skewness_.__doc__ += '\n' + cpp.Analysis.StatVar.skewness.__doc__
_data_kurtosis_.__doc__ += '\n' + cpp.Analysis.StatVar.kurtosis.__doc__
_data_quantile_.__doc__ += '\n' + cpp.Analysis.StatVar.quantile.__doc__
_data_interval_.__doc__ += '\n' + cpp.Analysis.StatVar.interval.__doc__
_data_quantiles_.__doc__ += '\n' + cpp.Analysis.StatVar.quantiles.__doc__


def data_decorate(klass):

    if hasattr(klass, 'get_moment'): klass.orig_get_moment = klass.get_moment
    if hasattr(klass, 'moment'): klass.orig_moment = klass.moment
    if hasattr(klass, 'central_moment'):
        klass.orig_central_moment = klass.central_moment
    if hasattr(klass, 'mean'): klass.orig_mean = klass.mean
    if hasattr(klass, 'rms'): klass.orig_rms = klass.rms
    if hasattr(klass, 'skewness'): klass.orig_skewness = klass.skewness
    if hasattr(klass, 'kurtosis'): klass.orig_kurtosis = klass.kurtosis

    if hasattr(klass, 'quantile'): klass.orig_quantile = klass.quantile
    if hasattr(klass, 'quantiles'): klass.orig_quantiles = klass.quantiles
    if hasattr(klass, 'interval'): klass.orig_interval = klass.interval
    if hasattr(klass, 'median'): klass.orig_median = klass.median
    if hasattr(klass, 'terciles'): klass.orig_terciles = klass.terciles
    if hasattr(klass, 'quartiles'): klass.orig_quartiles = klass.quartiles
    if hasattr(klass, 'quintiles'): klass.orig_quintiles = klass.quintiles
    if hasattr(klass, 'deciles'): klass.orig_deciles = klass.deciles

    klass.get_moment = _data_get_moment_
    klass.moment = _data_moment_
    klass.central_moment = _data_central_moment_
    klass.mean = _data_mean_
    klass.rms = _data_rms_
    klass.skewness = _data_skewness_
    klass.kurtosis = _data_kurtosis_

    klass.quantile = _data_quantile_
    klass.quantiles = _data_quantiles_
    klass.interval = _data_interval_
    klass.median = _data_median_
    klass.terciles = _data_terciles_
    klass.quartiles = _data_quartiles_
    klass.quintiles = _data_quintiles_
    klass.deciles = _data_deciles_


for t in (ROOT.TTree, ):
    data_decorate(t)

# =============================================================================
if '__main__' == __name__:

    import Ostap.Line
    logger.info(__file__ + '\n' + Ostap.Line.line)
    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(80 * '*')
    logger.info(' Author  : %s' % __author__)
    logger.info(' Version : %s' % __version__)
    logger.info(' Date    : %s' % __date__)
    logger.info(' Symbols : %s' % list(__all__))
    logger.info(80 * '*')

# =============================================================================
# The END
# =============================================================================
