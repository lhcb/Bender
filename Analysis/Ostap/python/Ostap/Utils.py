#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  Module with some simple but useful utilities
#   - timing
#   - memory
#   - suppression of stdout/stderr
#   - dumpting of stdout/stderr into file
#   - ROOT errors -> C++/Python exceptions
#   - Error handlers for GSL
#   - and more
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-02-10
#
# =============================================================================
"""Module with some simple but useful utilities
- timing
- memory
- suppression of stdout/stderr
- dumpting of stdout/stderr into file
- ROOT errors -> C++/Python exceptions
- Error handlers for GSL
- and more
"""
# =============================================================================
__version__ = "$Revision$"
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2013-02-10"
# =============================================================================
__all__ = (
    #
    'virtualMemory',  ## context manager to count virtual memory increase
    'memory',  ## ditto
    'clocks',  ## context manager to count clocks
    'timing',  ## context manager to count time
    'timer',  ## ditto
    'profiler',  ## context manager to perform profiling
    #
    'tee_py',  ## tee for Python's printouts
    'tee_cpp',  ## tee for C++'s    printouts
    'output',  ## redirect stdout/stderr into the file
    'mute_py',  ## suppress stdout/strerr Python printout
    'silence_py',  ## ditto
    'mute',  ## context manager to suppress stdout/strerr printout
    'silence',  ## ditto
    'rooSilent',  ## control RooFit verbosity
    'roo_silent',  ## control RooFit verbosity
    'rootError',  ## control ROOT verbosity
    'rootWarning',  ## control ROOT verbosity
    #'NoContext'      , ## empty context manager
    #'RooSilent'      , ## control RooFit verbosity
    #'ROOTIgnore'     , ## control ROOT verbosity, suppress ROOT errors
    #'Profiler'       , ## context manager to perform profiling
    ##
    #'takeIt'         , ## take and later delete ...buggy :-(
    ## logging
    'logColor',  ## swith on locally the colored logging
    'logVerbose',  ## redefine (locally) the logging level
    'logDebug',  ## redefine (locally) the logging level
    'logInfo',  ## redefine (locally) the logging level
    'logWarning',  ## redefine (locally) the logging level
    'logError',  ## redefine (locally) the logging level
    ##
    'isatty',  ## is sys.stdout attached to terminal or not ?
    ## Root Error handler
    'rootException',  ## context manager to perform ROOT Error -> C++/Python exception
    ## Gsl  Error Handlers
    #'gslIgnore'      , ## context manager to ignore GSL errore
    #'gslError'       , ## context manager to print  GSL errors
    #'gslException'   , ## context manager to turn   GSL errors into C++/Python exceptions
    #'GslIgnore'      , ## context manager to ignore GSL errore
    #'GslError'       , ## context manager to print  GSL errors
    #'GslException'   , ## context manager to turn   GSL errors into C++/Python exceptions
    #'setGslHandler'  , ## use ``global'' GSL handler
    #'useGslHandler'  , ## ditto
    ##
    'CleanUp',  ##  Helper base class to manage temporary files and directories
)
# =============================================================================
import ROOT, cppyy, time, os, sys  ## attention here!!
cpp = cppyy.makeNamespace('')
ROOT_RooFit_ERROR = 4
# =============================================================================
# logging
# =============================================================================
from Ostap.Logger import getLogger, logColor, isatty
if '__main__' == __name__: logger = getLogger('Ostap.Utils')
else: logger = getLogger(__name__)
del getLogger
from Ostap.Logger import logVerbose, logDebug, logInfo, logWarning, logError


# =============================================================================
## @class Memory
#  Simple context manager to measure the virtual memory increase
#
#  @see System::virtualMemory
#  @code
#
#  with Memory() :
#     <whatever action is>
#     at the exit it prints the change in virtual memory
#  @endcode
#
# Or:
#
#  @code
#
#  with Memory() as Q :
#     <whatever action is>
#     at the exit it prints the change in virtual memory
#
#  print Q.delta
#
#  @endcode
#
#  @author Vanya Belyaev Ivan.Belyaev@itep.ru
#  @date 2013-02-10
class Memory(object):
    """Simple class to evaluate the change in virtual memory
    to be used as context manager:

    >>> with Memory('here...') :
    ...     <whatever action is>
    at the exit it prints the change in virtual memory

    >>> with Memory('here...') as M :
    >>> <whatever action is>
    at the exit it prints the change in virtual memory

    >>> delta = M.delta
    """
    _logger = logger

    def __init__(self,
                 name='',
                 logger=None,
                 format='Memory %-18s %+.1fMB/[%.2fGB]'):
        self.name = name
        self.logger = logger if logger else self._logger
        self.format = format

    def __enter__(self):
        self.memory = cpp.System.virtualMemory()
        return self

    def __exit__(self, *_):
        current = float(cpp.System.virtualMemory())
        self.delta = current - self.memory
        self.delta /= 1000
        try:
            message = self.format % (self.name, self.delta, current / 1000000)
        except TypeError:
            message = 'Memory %-18s %+.1fMB/[%.2fGB]' % (self.name, self.delta,
                                                         current / 1000000)

        self.logger.info(message)


# ============================================================================
## create the context manager to monitor the virtual memory increase
def virtualMemory(name=''):
    """Create the context manager to monitor the virtual memory increase:

    >>> with memory('here...') :
    ...   <whatever action is>
    at the exit it prints the change in virtual memory

    >>> with memory('here...') as m :
    ...   <whatever action is>
    at the exit it prints the change in virtual memory

    >>> delta = m.delta
    """
    return Memory(name)


## ditto
memory = virtualMemory  ## ditto


# =============================================================================
## @class Clock
#  Smple context manager to measure the clock counts
#
#  @code
#
#  with Clock() :
#     <whatever action is>
#     at the exit it prints the clock counts
#  @endcode
#
# Or:
#
#  @code
#
#  with Clock() as c :
#     <whatever action is>
#     at the exit it prints the clock counts
#
#  print c.delta
#
#  @endcode
#  @author Vanya Belyaev Ivan.Belyaev@itep.ru
#  @date 2013-02-10
class Clock(object):
    """Simple context manager to measure the clock counts
    >>> with Clock() :
    ...  <whatever action is>
    at the exit it prints the clock counts

    >>> with Clock() as c :
    ...  <whatever action is>
    at the exit it prints the clock counts

    >>> print c.delta
    """
    _logger = logger

    def __init__(self, name='', logger=None, format='Clocks %-18s %s'):
        self.name = name
        self.logger = logger if logger else self._logger
        self.format = format

    def __enter__(self):
        self.clock = time.clock()
        return self

    def __exit__(self, *_):
        self.delta = time.clock() - self.clock
        try:
            message = self.format % (self.name, self.delta)
        except TypeError:
            message = 'Clocks %-18s %s' % (self.name, self.delta)

        self.logger.info(message)


# =============================================================================
## @class Timer
#  Simple context manager to measure the time
#  @code
#
#  with Timer() :
#     <whatever action is>
#     at the exit it prints the time
#  @endcode
#
# Or:
#
#  @code
#
#  with Timer() as t :
#     <whatever action is>
#     at the exit it prints the clock counts
#
#  print ct.delta
#
#  @endcode
#
#  @author Vanya Belyaev Ivan.Belyaev@itep.ru
#  @date 2013-02-10
#
class Timer(object):
    """Simple context manager to measure the time

    >>> with Timer() :
    ...  <whatever action is>
    at the exit it prints the time

    Or:

    >>> with Timer() as t :
    ...  <whatever action is>
    at the exit it prints the clock counts

    >>> print ct.delta
    """
    _logger = logger

    def __init__(self, name='', logger=None, format='Timing %-18s %.3f'):
        self.name = name
        self.logger = logger if logger else self._logger
        self.format = format

    def __enter__(self):
        self.time = time.time()
        return self

    def __exit__(self, *_):
        self.delta = time.time() - self.time

        try:
            message = self.format % (self.name, self.delta)
        except TypeError:
            message = 'Timing %-18s %s' % (self.name, self.delta)

        self.logger.info(message)


# =============================================================================
## Simple context manager to measure the clock counts
#
#  @code
#
#  with clocks () :
#     <whatever action is>
#     at the exit it prints the clock counts
#
#  @endcode
#
# Or:
#
#  @code
#
#  with clocks () as c :
#     <whatever action is>
#     at the exist it prints the clock counts
#
#  print c.delta
#
#  @endcode
#  @author Vanya Belyaev Ivan.Belyaev@itep.ru
#  @date 2013-02-10
def clocks(name=''):
    """Simple context manager to measure the clock counts

    >>> with clocks () :
    ...   <whatever action is>
    at the exit it prints the clock counts

    >>> with clocks () as c :
    ...   <whatever action is>
    at the exit it prints the clock counts

    >>>print c.delta
    """
    return Clock(name)


# =============================================================================
## Simple context manager to measure the time
#
#  @code
#
#  with timer () :
#     <whatever action is>
#     at the exit it prints the time
#
#  @endcode
#
# Or:
#
#  @code
#
#  with timer () as t :
#     <whatever action is>
#     at the exit it prints the clock counts
#
#  print t.delta
#
#  @endcode
#  @author Vanya Belyaev Ivan.Belyaev@itep.ru
#  @date 2013-02-10
def timing(name='', logger=None):
    """Simple context manager to measure the clock counts

    >>> with timing () :
    ...   <whatever action is>
    at the exit it prints the clock counts

    >>> with timing () as c :
    ...   <whatever action is>
    at the exit it prints the clock counts

    >>> print c.delta
    """
    return Timer(name, logger)


# =============================================================================
## ditto
timer = timing  # ditto


# =============================================================================
## @class Profiler
#  Very simple profiler, based on cProfile module
#  @see https://docs.python.org/2/library/profile.html
#  @code
#  with profiler() :
#      ...  some code here ...
#  with profiler('output.file') :
#      ...  some code here ...
#  @endcode
#  @author Vanya Belyaev Ivan.Belyaev@itep.ru
#  @date 2016-07-25
class Profiler(object):
    """  Very simple profiler, based on cProfile module
    - see https://docs.python.org/2/library/profile.html

    with profiler() :
    #
    # ...  some code here ...
    #

    with profiler( 'output.file' ) :
    #
    # ...  some code here ...
    #
    """

    def __init__(self, fname=''):
        self.fname = fname

    ## enter the context
    def __enter__(self):
        import cProfile as profile
        self._profile = profile.Profile()
        self._profile.enable()
        return self

    ## exit the context
    def __exit__(self, *_):
        ## end of profiling
        self._profile.disable()

        import pstats
        if self.fname:
            try:
                with open(self.fname, 'w') as out:
                    stat = pstats.Stats(
                        self._profile, stream=out).sort_stats('cumulative')
                    stat.print_stats()
                del self._profile
                return
            except:
                pass

        ## show on screen
        stat = pstats.Stats(self._profile).sort_stats('cumulative')
        stat.print_stats()
        del self._profile


# =============================================================================
## Very simple profiler, based on cProfile module
#  @see https://docs.python.org/2/library/profile.html
#  @code
#  with profiler() :
#      ...  some code here ...
#  @endcode
#  @author Vanya Belyaev Ivan.Belyaev@itep.ru
#  @date 2016-07-25
def profiler(name=''):
    """ Very simple profiler, based on cProfile module
    - see https://docs.python.org/2/library/profile.html

    with profiler() :
    #
    # ...  some code here ...
    #
    """
    return Profiler(name)


# ============================================================================
## @class MutePy
#  Very simple context manager to suppress python printout
class MutePy(object):
    """A context manager for doing a ``deep suppression'' of stdout and stderr in
    Python, i.e. will suppress all print, even if the print originates in a
    compiled C/Fortran sub-function.
    This will not suppress raised exceptions, since exceptions are printed
    to stderr just before a script exits, and after the context manager has
    exited (at least, I think that is why it lets exceptions through).

    stallen from
    http://stackoverflow.com/questions/11130156/suppress-stdout-stderr-print-from-python-functions
    """

    def __init__(self, out=True, err=False):
        self._out = out
        self._err = err

    def __enter__(self):
        #
        ## helper class to define empty stream
        class Silent(object):
            def write(self, *args, **kwards):
                pass

        import sys
        self.stdout = sys.stdout
        self.stderr = sys.stderr

        if self._out:
            sys.stdout.flush()
            sys.stdout = Silent()
        if self._err:
            sys.stderr.flush()
            sys.stderr = Silent()

        return self

    def __exit__(self, *_):

        import sys
        sys.stdout = self.stdout
        sys.stderr = self.stderr


# ============================================================================
## @class MuteC
#  context manager to suppress pythion prinout
#  the actual code is stallen from
#  http://stackoverflow.com/questions/11130156/suppress-stdout-stderr-print-from-python-functions
#  A fix is added for "IOError: [Errno 24] Too many open files" :
#  original code leaks the file descriptors
class MuteC(object):
    """A context manager for doing a ``deep suppression'' of stdout and stderr in
    Python, i.e. will suppress all print, even if the print originates in a
    compiled C/Fortran sub-function.
    This will not suppress raised exceptions, since exceptions are printed
    to stderr just before a script exits, and after the context manager has
    exited (at least, I think that is why it lets exceptions through).

    stallen from
    http://stackoverflow.com/questions/11130156/suppress-stdout-stderr-print-from-python-functions
    """
    #
    ## class variables: dev-null device & instance counter
    _devnull = 0
    _cnt = 0

    def __init__(self, out=True, err=False):

        self._out = out
        self._err = err

        # increment instance counter
        self.__class__._cnt += 1

        # create dev-null if not done yet
        if not self.__class__._devnull:
            self.__class__._devnull = os.open(os.devnull, os.O_WRONLY)

    def __del__(self):

        # decrement instance counter
        self.__class__._cnt -= 1

        # close dev-null if not done yet
        if self.__class__._cnt <= 0 and self.__class__._devnull:
            os.close(self.__class__._devnull)
            self.__class__._devnull = 0

    ## context-manager
    def __enter__(self):

        import sys
        if self._out: sys.stdout.flush()
        if self._err: sys.stderr.flush()

        ## Save the actual stdout (1) and stderr (2) file descriptors.
        self.save_fds = os.dup(1), os.dup(2)  # leak was here !!!

        ## mute it!
        import sys
        if self._out: os.dup2(self.__class__._devnull, 1)  ## C/C++
        if self._err: os.dup2(self.__class__._devnull, 2)  ## C/C++

        return self

    ## context-manager
    def __exit__(self, *_):

        import sys
        if self._out: sys.stdout.flush()
        if self._err: sys.stderr.flush()

        # Re-assign the real stdout/stderr back to (1) and (2)  (C/C++)
        if self._err: os.dup2(self.save_fds[1], 2)
        if self._out: os.dup2(self.save_fds[0], 1)

        # fix the  file descriptor leak
        # (there were no such line in example, and it causes
        #      the sad:  "IOError: [Errno 24] Too many open files"

        os.close(self.save_fds[1])
        os.close(self.save_fds[0])


# =============================================================================
## dump all stdout/stderr information (including C/C++) into separate file
#  @code
#  with output ('output.txt') :
#           print 'ququ!'
#  @endcode
#  @see MuteC
class OutputC(object):
    """Dump all stdout/stderr information into separate file:
    >>>  with output ('output.txt') :
    ...             print 'ququ!'
    """

    ## constructor: file name
    def __init__(self, filename, out=True, err=False):
        """
        Constructor
        """
        self._out = out
        self._err = out
        self._file = file(filename, 'w')

    ## context-manager
    def __enter__(self):

        self._file.__enter__()
        #
        import sys
        if self._out: sys.stdout.flush()
        if self._err: sys.stderr.flush()
        #
        ## Save the actual stdout (1) and stderr (2) file descriptors.
        #
        self.save_fds = os.dup(1), os.dup(2)  # leak was here !!!

        #
        ## mute it!
        #
        import sys
        if self._out: os.dup2(self._file.fileno(), 1)  ## C/C++
        if self._err: os.dup2(self._file.fileno(), 2)  ## C/C++

        return self

    ## context-manager
    def __exit__(self, *_):

        import sys
        if self._out: sys.stdout.flush()
        if self._err: sys.stderr.flush()
        #
        # Re-assign the real stdout/stderr back to (1) and (2)  (C/C++)
        #
        import sys
        if self._err: os.dup2(self.save_fds[1], 2)
        if self._out: os.dup2(self.save_fds[0], 1)

        # fix the  file descriptor leak
        # (there were no such line in example, and it causes
        #      the sad:  "IOError: [Errno 24] Too many open files"

        os.close(self.save_fds[1])
        os.close(self.save_fds[0])

        self._file.__exit__(*_)


# =============================================================================
## very simple context manager to duplicate Python-printout into file ("tee")
#  into separate file
#  @code
#  with tee('tee.txt') :
#           print 'ququ!'
#  @endcode
#  @attention: only Python printouts are grabbed
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  date 2012-07-06
class TeePy(object):
    """Very simple context manager to duplicate Python-printout into file (``tee'')
    into separate file

    >>>  with tee('tee.txt') :
    ...        print 'ququ!'

    Unfortunately only Python printouts are grabbed
    """

    ## constructor
    def __init__(self, filename):

        self._file = file(filename, 'w')

    ## context manager
    def __enter__(self):

        self._file.__enter__()

        ## helper class to define empty stream
        class _Tee(object):
            def __init__(self, the_file, the_stream):

                self._stream = the_stream
                self._log = the_file

            def write(self, *args):

                self._stream.write(*args)
                self._log.write(*args)

        self.stdout = sys.stdout
        sys.stdout = _Tee(self._file, self.stdout)

        return self

    ## context manager
    def __exit__(self, *_):

        self._file.flush()
        self.stdout.flush()

        sys.stdout = self.stdout

        self._file.__exit__(*_)


# =============================================================================
## very simple context manager to duplicate C++-printout into file ("tee")
#  into separate file
#  @code
#  >>> with tee_cpp('tee.txt') :
#  ...         some_cpp_function()
#  @endcode
#  @see Gaudi::Utils::Tee
#  @attention: Python&C-printouts probably  are not affected
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  date 2012-07-07
class TeeCpp(object):
    """Very simple context manager to duplicate C++-printout into file
    into separate file

    >>> with tee_cpp('tee.txt') :
    ...         some_cpp_function()

    """

    def __init__(self, fname):
        self._tee = cpp.Gaudi.Utils.Tee(fname)

    ## context manager
    def __enter__(self):
        self._tee.enter()
        return self

    ## context manager
    def __exit__(self, *_):
        self._tee.exit()
        del self._tee


# =============================================================================
## very simple context manager to suppress RooFit printout
#
#  @code
#
#  >>> with rooSilent( 4 , False ) :
#  ...        some_RooFit_code_here()
#
#  @endcode
#  @see RooMgsService
#  @see RooMgsService::globalKillBelow
#  @see RooMgsService::silentMode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-07-09
class RooSilent(object):
    """Very simple context manager to suppress RooFit printout

    >>> with rooSilent( 4 , False ) :
    ...        some_RooFit_code_here ()

    """

    ## constructor
    #  @param level  (INPUT) print level
    #  @param silent (print level
    #
    def __init__(self, level=ROOT_RooFit_ERROR, silent=True):
        """ Constructor
        @param level  (INPUT) print level
        @param silent (print level

        >>> with rooSilent( ROOT.RooFit.ERROR , True  ) :
        ...        some_RooFit_code_here ()


        >>> with rooSilent( ROOT.RooFit.INFO , False  ) :
        ...        some_RooFit_code_here ()


        """
        #
        if level > ROOT.RooFit.FATAL: level = ROOT.RooFit.FATAL
        if level < ROOT.RooFit.DEBUG: level = ROOT.RooFit.DEBUG
        #
        self._level = level
        self._silent = True if silent else False
        self._svc = ROOT.RooMsgService.instance()

    ## context manager
    def __enter__(self):

        self._prev_level = self._svc.globalKillBelow()
        self._prev_silent = self._svc.silentMode()

        self._svc.setGlobalKillBelow(self._level)
        self._svc.setSilentMode(self._silent)

        return self

    ## context manager
    def __exit__(self, *_):

        self._svc.setSilentMode(self._prev_silent)
        self._svc.setGlobalKillBelow(self._prev_level)


# =============================================================================
## Very simple context manager to suppress ROOT printout
#  @code
#  >>> with ROOTIgnore( ROOT.kError + 1 ) : some_ROOT_code_here()
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2015-07-30
class ROOTIgnore(object):
    """Very simple context manager to suppress ROOT printout
    >>> with ROOTIgnore ( ROOT.kError + 1 ) : some_ROOT_code_here()
    """

    ## constructor
    #  @param level  (INPUT) print level
    #  @param silent (print level
    #
    def __init__(self, level):
        """ Constructor:
        >>> with rootError   () : some_ROOT_code_here()
        >>> with rootWarning () : some_ROOT_code_here()
        """
        #
        self._level = int(level)

    ## context manager: ENTER
    def __enter__(self):
        "The actual context manager: ENTER"
        self._old = int(ROOT.gErrorIgnoreLevel)
        if self._old != self._level:
            ROOT.gROOT.ProcessLine("gErrorIgnoreLevel= %d ; " % self._level)

        return self

    ## context manager: EXIT
    def __exit__(self, *_):
        "The actual context manager: EXIT"
        if self._old != int(ROOT.gErrorIgnoreLevel):
            ROOT.gROOT.ProcessLine("gErrorIgnoreLevel= %d ; " % self._old)


# =============================================================================
## @class NoContext
#  Fake empty context manager to be used as empty placeholder
#  @code
#  with NoContext() :
#  ...  do_something()
#  @endocode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  date 2013-01-12
class NoContext(object):
    """ Fake (empty) context manager to be used as empty placeholder
    >>> with NoContext() :
    ...         do_something()
    """

    def __init__(self, *args, **kwargs):
        pass

    ## context manager
    def __enter__(self):
        return self

    ## context manager
    def __exit__(self, *args):
        pass


# =============================================================================
## @class TakeIt
#  Take some object, keep it and delete at the exit
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  date 2014-08-03
class TakeIt(object):
    """Take some object, keep it and delete at the exit

    >>> ds = dataset.reduce('pt>1')
    >>> with takeIt ( ds ) :
    ...

    """

    def __init__(self, other):
        self.other = other

    def __enter__(self):
        ROOT.SetOwnership(self.other, True)
        return self.other

    def __exit__(self, *args):

        o = self.other

        ## delete it!
        del self.other

        if o and hasattr(o, 'reset'): o.reset()
        if o and hasattr(o, 'Reset'): o.Reset()
        if o and hasattr(o, 'Delete'): o.Delete()

        if o: del o


# =============================================================================
## very simple context manager to duplicate Python-printout into file ("tee")
#  into separate file
#  @code
#  >>> with tee_py ('tee.txt') :
#  ...         print 'ququ!'
#  @endcode
#  @attention: only Python prinouts are grabbed
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  date 2012-07-06
def tee_py(filename):
    """Very simple context manager to duplicate Python-printout into file ("tee")
    into separate file
    >>> with tee('tee.txt') :
    ...        print 'ququ!'
    Unfortunately only Python printouts are grabbed
    """
    return TeePy(filename)


# =============================================================================
## very simple context manager to duplicate C++-printout into file ('tee')
#  into separate file
#  @code
#  >>> with tee_cpp ('tee.txt') : some_cpp_code()
#  @endcode
#  @attention: only C/C++ printouts are grabbed
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  date 2012-07-06
def tee_cpp(filename):
    """Very simple context manager to duplicate C++-printout into file ('tee')
    into separate file
    >>> with tee_cpp('tee.txt') : some_cpp_code()
    Unfortunately only C/C++ printouts are grabbed
    """
    return TeeCpp(filename)


# =============================================================================
## simple context manager to redirect all (C/C++/Python) printout
#  into separate file
#  @code
#  >>> with output ('output.txt') :
#  ...         print 'ququ!'
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  date 2012-07-06
def output(fname, cout=True, cerr=False):
    """ Simple context manager to redirect all (C/C++/Python) printotu

    >>> with output ('output.txt') :
    ...               print 'ququ!'

    """
    return OutputC(fname, cout, cerr)


# =============================================================================
## simple context manager to suppress C/C++-printout
#
#  @code
#  >>> with mute () :
#  ...        <some code here>
#  @endcode
def mute(cout=True, cerr=False):
    """Simple context manager to suppress C/C++ printout

    >>> with mute () :
    ...     <some code here>
    """
    return MuteC(cout, cerr)


# =============================================================================
## simple context manager to suppress Python-printout
#
#  @code
#  >>> with mute_py () :
#  ...        <some code here>
#  @endcode
def mute_py(cout=True, cerr=False):
    """Simple context manager to suppress python printouts

    >>> with mute_py () :
    ...    <some code here>
    """
    return MutePy(cout, cerr)


# ==============================================================================
## ditto
silence_py = mute_py  # ditto
silence = mute  # ditto


# =============================================================================
## very simple context manager to suppress RooFit printout
#
#  @code
#
#  >>> with rooSilent( 4 , False ) :
#  ...        some_RooFit_code_here()
#
#  @endcode
#  @see RooMgsService
#  @see RooMgsService::globalKillBelow
#  @see RooMgsService::silentMode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-07-09
def rooSilent(level=ROOT_RooFit_ERROR, silent=True):
    """Very simple context manager to suppress RooFit printout
    >>> with rooSilent( 4 , False ) :
    ...        some_RooFit_code_here()
    """
    return RooSilent(level, silent)


# =============================================================================
## helper context manager
#  @code
#
#  >>> with roo_silent( True ) :
#  ...        some_RooFit_code_here()
#
#  @endcode
#  @see rooSilent
#  @see NoContext
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2013-07-09
def roo_silent(silence, *args):
    """ Helper context manager#
    >>> with roo_silent ( True ) :
    ...        some_RooFit_code_here()
    """
    return rooSilent(*args) if silence else NoContext()


# =============================================================================
## very simple context manager to suppress ROOT printout
#  @code
#  >>> with rootError () : some_ROOT_code_here()
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2015-07-30
def rootError(level=1):
    """Very simple context manager to suppress ROOT printout
    >>> with rootError () : some_ROOT_code_here()
    """
    return ROOTIgnore(ROOT.kError + level)


# =============================================================================
## very simple context manager to suppress ROOT printout
#  @code
#  >>> with rootError () : some_ROOT_code_here()
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2015-07-30
def rootWarning(level=1):
    """Very simple context manager to suppress ROOT printout
    >>> with rootWarning () : some_ROOT_code_here()
    """
    return ROOTIgnore(ROOT.kWarning + level)


# =============================================================================
## Take some object, keep it and delete at the exit
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  date 2014-08-03
def takeIt(other):
    """ Take some object, keep it and delete at the exit
    >>> ds = dataset.reduce('pt>1')
    >>> with takeIt ( ds ) :
    ...
    """
    return TakeIt(other)


# =============================================================================
## get all open file descriptors
#  The actual code is copied from http://stackoverflow.com/a/13624412
def get_open_fds():
    """Get all open file descriptors
    The actual code is copied from http://stackoverflow.com/a/13624412
    """
    #
    import resource
    import fcntl
    #
    fds = []
    soft, hard = resource.getrlimit(resource.RLIMIT_NOFILE)
    for fd in range(0, soft):
        try:
            flags = fcntl.fcntl(fd, fcntl.F_GETFD)
        except IOError:
            continue
        fds.append(fd)
    return fds


# =============================================================================
## get the actual file name form file descriptor
#  The actual code is copied from http://stackoverflow.com/a/13624412
#  @warning: it is likely to be "Linux-only" function
def get_file_names_from_file_number(fds):
    """ Get the actual file name from file descriptor
    The actual code is copied from http://stackoverflow.com/a/13624412
    """
    names = []
    for fd in fds:
        names.append(os.readlink('/proc/self/fd/%d' % fd))
    return names


# =============================================================================
## helper context manager to activate ROOT Error -> Python exception converter
#  @see Analysis::Utils::useRootErrorHandler
#  @see Analysis::Utils::RootError
#  @code
#  with RootError2Exception() :
#  .... do something here
#  @endcode
class RootError2Exception(object):
    """Helper context manager to activate ROOT Error -> Python exception converter
    #
    with RootError2Exception() :
    ... do something here
    """

    def __init__(self):
        self.e_handler = cpp.Analysis.Utils.useRootErrorHandler
        self.m_previous = False

    ## context manager entry point
    def __enter__(self):
        self.m_previous = self.e_handler(True)
        return self

    ## context manager exit point
    def __exit__(self, *_):
        if self.m_previous: self.e_handler(False)
        self.m_previous = False

    def __del__(self):
        if self.m_previous: self.e_handler(False)


# =============================================================================
## helper context manager to activate ROOT Error -> Python exception converter
#  @see Analysis::Utils::useRootErrorHandler
#  @see Analysis::Utils::ErrorSentry
#  @code
#  with rootException () :
#  .... do something here
#  @endcode
def rootException():
    """Helper context manager to activate ROOT Error -> Python exception converter
    #
    with rootException() :
    ... do something here
    """
    return RootError2Exception()


# =============================================================================
## helper base class/context manager
class ErrHandler(object):
    handler = None

    def __init__(self):
        self.err_handler = None

    def __enter__(self):
        self.err_handler = self.handler()
        return self

    def __exit__(self, *_):
        if self.err_handler: del self.err_handler
        self.err_handler = None


# =============================================================================
## @class GslIgnore
#  Simple context manager to ignore all GSL errors
#  @code
#  with GslIgnore() :
#      ... do something
#  @endcode
class GslIgnore(ErrHandler):
    """Simple context manager to ignore all GSL errors
    >>> with GslIgnore() :
    >>>    ... do something...
    """
    with mute():
        handler = cpp.Analysis.Utils.GslIgnore


# =============================================================================
## @class GslError
#  Simple context manager to print GSL errors to stderr
#  @code
#  with GslError() :
#      ... do something
#  @endcode
class GslError(ErrHandler):
    """Simple context manager to print GSL errors to stderr
    >>> with GslError() :
    >>>    ... do something...
    """
    with mute():
        handler = cpp.Analysis.Utils.GslError


# =============================================================================
## @class GslException
#  Simple context manager to turn GSL errors into C++/Python exceptions
#  @code
#  with GslException() :
#      ... do something
#  @endcode
class GslException(ErrHandler):
    """Simple context manager to turn GSL Errors into C++/Python exceptions
    >>> with GslError() :
    >>>    ... do something...
    """
    with mute():
        handler = cpp.Analysis.Utils.GslException


# =============================================================================
## Simple context manager to ignore all GSL errors
#  @code
#  with gslIgnore() :
#      ... do something
#  @endcode
def gslIgnore():
    """Simple context manager to ignore all GSL errors
    >>> with gslIgnore() :
    >>>    ... do something...
    """
    return GslIgnore()


# =============================================================================
## Simple context manager to print GSL errors to stderr
#  @code
#  with gslError() :
#      ... do something
#  @endcode
def gslError():
    """Simple context manager to print GSL errors to stderr
    >>> with GslError() :
    >>>    ... do something...
    """
    return GslError()


# =============================================================================
## Simple context manager to turn GSL errors into C++/Python exceptions
#  @code
#  with gslException() :
#      ... do something
#  @endcode
def gslException():
    """Simple context manager to turn GSL Errors into C++/Python exceptions
    >>> with GslError() :
    >>>    ... do something...
    """
    return GslException()


# =============================================================================
_global_gsl_handler = []


def _setHandler(handler):
    global _global_gsl_handler
    while _global_gsl_handler:
        _global_gsl_handler.pop()
    if handler: _global_gsl_handler.append(handler)
    return _global_gsl_handler


# =============================================================================
## Make use ``global'' GSL handler
#  @code
#  setGslHandler ( None        ) ## clean up global  handlers
#  setGslHandler ( 'Ignore'    ) ## ignore all GSL erorrs
#  setGslHandler ( 'Error'     ) ## print GSL errors to stderr and continue
#  setGslHandler ( 'Exception' ) ## convert GSL errors into C++/Python exceptions
#  setGslHandler ( 'Raise'     ) ## ditto
#  setGslHandler ( 'Throw'     ) ## ditto
#  @endcode
def setGslHandler(handler):
    """Use ``global'' GSL handler
    >>> setGslHandler ( None        ) ## clean up global  handlers
    >>> setGslHandler ( 'Ignore'    ) ## ignore all GSL erorrs
    >>> setGslHandler ( 'Error'     ) ## print GSL errors to stderr and continue
    >>> setGslHandler ( 'Exception' ) ## convert GSL errors into C++/Python exceptions
    >>> setGslHandler ( 'Raise'     ) ## ditto
    >>> setGslHandler ( 'Throw'     ) ## ditto
    """
    #
    Analysis = cpp.Analysis
    #
    global _global_gls_handler
    if not handler: _setHandler(handler)
    elif isinstance(handler, str):
        hl = handler.lower()
        if 'ignore' == hl:
            _setHandler(Analysis.Utils.GslIgnore())
            logger.debug('Global GSL error Handler: Ignore all GLS errors')
        elif hl in ('error', 'print'):
            _setHandler(Analysis.Utils.GslError())
            logger.debug(
                'Global GSL error Handler: print all GLS errors to stderr')
        elif hl in ('exception', 'raise', 'throw'):
            _setHandler(Analysis.Utils.GslException())
            logger.debug(
                'Global GSL error Handler: convert GLS errors to C++/Python exceptions'
            )
        else:
            raise TypeError('Unknown handler type %s' % handler)
    elif isinstance(handler, Analysis.Utils.GslError):
        _setHandler(handler)
        logger.debug('Global Eror Handler: %s' % handler)
    elif issubclass(handler, Analysis.Utils.GslError):
        h = _setHandler(handler())
        logger.debug('Global Eror Handler: %s' % h)
    else:
        raise TypeError('Unknown handler type %s' % handler)


## ditto
useGslHandler = setGslHandler


# =============================================================================
## @class CleanUp
#  Simple (base) class to get temporary files and directories and to remove them at axit
class CleanUp(object):
    """Simple (base) class to get temporary files and directories and to remove them at axit
    """
    _tmpfiles = set()
    _tmpdirs = set()

    @property
    def tmpdir(self):
        """``tmpdir'' : return the name of temporary managed directory
        - the managed directory will be cleaned-up and deleted at-exit
        >>> o    = CleanUp()
        >>> tdir = o.tmpdir
        """
        tdir = CleanUp.tempdir()
        return tdir

    @property
    def tmpdirs(self):
        """``tmpdirs'' - list of currently registered managed temporary directories"""
        return tuple(self._tmpfiles)

    @property
    def tmpfiles(self):
        """``tempfiles'' : list of registered managed temporary files"""
        return list(self._tmpfiles)

    @tmpfiles.setter
    def tmpfiles(self, other):
        if isinstance(other, str): other = [other]
        for o in other:
            self._tmpfiles.add(o)

    @staticmethod
    def tempdir(suffix='', prefix='tmp_'):
        """Get the name of the temporary directory.
        The directory will be cleaned-up and deleted at-exit.
        >>> dirname = CleanUp.tempdir()
        """
        import tempfile
        tmp = tempfile.mkdtemp(suffix=suffix, prefix=prefix)
        CleanUp._tmpdirs.add(tmp)
        return tmp

    @staticmethod
    def tempfile(suffix='', prefix='tmp_', dir=None):
        """Get the name of the temporary file. The file will be deleted at-exit
        >>> fname = CleanUp.tempfile()
        """
        import tempfile, os
        _file = tempfile.NamedTemporaryFile(
            suffix=suffix, prefix=prefix, delete=False)
        fname = _file.name
        _file.close()
        os.unlink(fname)
        assert not os.path.exists(fname)
        CleanUp._tmpfiles.add(fname)
        return fname


# =============================================================================
import atexit


@atexit.register
def _cleanup_():
    files = CleanUp._tmpfiles
    if files: logger.debug('CleanUp: remove temporary files: %s' % list(files))
    while files:
        f = files.pop()
        if os.path.exists(f) and os.path.isfile(f):
            logger.verbose('CleanUp: remove temporary file: %s' % f)
            try:
                os.remove(f)
            except:
                pass
    dirs = CleanUp._tmpdirs
    if dirs:
        logger.debug('CleanUp: remove temporary directories: %s' % list(dirs))
    while dirs:
        f = dirs.pop()
        if os.path.exists(f) and os.path.isdir(f):
            ## remove all files & subdirectories
            for root, dirs, files in os.walk(f, topdown=False):
                for ff in files:
                    ff = os.path.join(root, ff)
                    logger.verbose(
                        'CleanUp: remove file %s in temporary directory : %s' %
                        (ff, f))
                    try:
                        os.remove(ff)
                    except:
                        pass
                for dd in dirs:
                    dd = os.path.join(root, dd)
                    logger.verbose(
                        'CleanUp: remove subdirectory %s in temporary directory %s '
                        % (dd, f))
                    try:
                        os.rmdir(dd)
                    except:
                        pass
            ## remove the root
            logger.verbose('CleanUp: remove temporary directory: %s' % f)
            try:
                os.rmdir(f)
            except:
                pass


# =============================================================================
if '__main__' == __name__:

    import Ostap.Line
    logger.info(__file__ + '\n' + Ostap.Line.line)
    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(80 * '*')
    logger.info(' Author  : %s' % __author__)
    logger.info(' Version : %s' % __version__)
    logger.info(' Date    : %s' % __date__)
    logger.info(' Symbols : %s' % list(__all__))
    logger.info(80 * '*')

# =============================================================================
# The END
# =============================================================================
