#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  An example of simple script to run PIDCalib machinery for Run-II samples
#
#  @code
#  pid_calib2.py Pi -p MagUp -y 2015
#  @endocode
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2017-05-05
# =============================================================================
""" An example of simple script to run PIDCalib machinery for Run-II samples

> pid_calib2.py Pi -p MagUp -y 2015

"""
# =============================================================================
__version__ = "$Revision$"
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2017-05-05"
__all__ = ()
# =============================================================================
import ROOT, cppyy
ROOT.PyConfig.IgnoreCommandLineOptions = True
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' == __name__: logger = getLogger('pid_calib2')
else: logger = getLogger(__name__)
# =============================================================================
import ROOT
import Ostap.Kisa

from Ostap.PidCalib2 import PARTICLE_3D as PARTICLE


# =============================================================================
## the actual function to fill PIDcalib histograms
#  - it books two histogram  (3D in this case)
#  - it fill them with 'accepted' and 'rejected' events (3D in this case)
#  - update input historgams
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-05-10
class PROTON(PARTICLE):
    def __init__(self, cuts=None):

        PARTICLE.__init__ (
            self     ,
            ## accepted sample
            accepted = '(probe_Brunel_hasRich)&&(probe_Brunel_MC15TuneV1_ProbNNp>0.4)' , ## ACCEPTED sample
            ## rejected sample
            rejected = '(probe_Brunel_hasRich)&&(probe_Brunel_MC15TuneV1_ProbNNp<0.4)' , ## REJECTED sample
            ## binning in P
            xbins    = [ 9  , 12 , 15 , 20 , 25 , 30 , 35 , 40 , 45 , 50 , 60 , 70 , 80 , 90 , 100 , 110 , 120 ] ,
            ## binning in ETA
            ybins    = [ 2.0 , 2.25 , 2.5 , 2.75 , 3.0 , 3.25, 3.5 , 4.75 , 4.0 , 4.25 , 4.5 , 4.65 , 4.9 ] ,
            ## binning in #tracks
            zbins    = [ 0 , 150 , 250 , 400 , 1000 ] ,
            ## additional cuts (if any)
            cuts     = cuts ,
            ## "Accept"-function                              what  to project/draw                                 cuts&weight
            acc_fun  = lambda s,data : data.pproject ( s.ha , 'nTracks : probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % s.accepted , silent = True ) ,
            ## "Reject"-function                              what  to project/draw                                 cuts&weight
            rej_fun  = lambda s,data : data.pproject ( s.hr , 'nTracks : probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % s.rejected , silent = True ) )


# =============================================================================
## the actual function to fill PIDcalib histograms
#  - it books two histogram  (3D in this case)
#  - it fill them with 'accepted' and 'rejected' events (3D in this case)
#  - update input historgams
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-05-10
class KAON(PARTICLE):
    def __init__(self, cuts=None):

        PARTICLE.__init__ (
            self     ,
            ## accepted sample
            accepted = '(probe_Brunel_hasRich)&&(probe_Brunel_MC15TuneV1_ProbNNk>0.4)' , ## ACCEPTED sample
            ## rejected sample
            rejected = '(probe_Brunel_hasRich)&&(probe_Brunel_MC15TuneV1_ProbNNk<0.4)' , ## REJECTED sample
            ## binning in P
            xbins    = [ 3.2 , 6 , 9 , 12 , 15 , 20 , 25 , 30 , 35 , 40 , 45 , 50 , 60 , 70 , 80 , 90 , 100 , 110 , 120 , 150 ] ,
            ## binning in ETA
            ybins    = [ 2.0 , 2.25 , 2.5 , 2.75 , 3.0 , 3.25, 3.5 , 4.75 , 4.0 , 4.25 , 4.5 , 4.65 , 4.9 ] ,
            ## binning in #tracks
            zbins    = [ 0 , 150 , 250 , 400 , 1000 ] ,
            ## additional cuts (if any)
            cuts     = cuts ,
            ## "Accept"-function                              what  to project/draw                                 cuts&weight
            acc_fun  = lambda s,data : data.pproject ( s.ha , 'nTracks : probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % s.accepted , silent = True ) ,
            ## "Reject"-function                              what  to project/draw                                 cuts&weight
            rej_fun  = lambda s,data : data.pproject ( s.hr , 'nTracks : probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % s.rejected , silent = True ) )


# =============================================================================
## the actual function to fill PIDcalib histograms
#  - it books two histogram  (3D in this case)
#  - it fill them with 'accepted' and 'rejected' events (3D in this case)
#  - update input historgams
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-05-10
class PION(PARTICLE):
    def __init__(self, cuts=None):

        PARTICLE.__init__ (
            self     ,
            ## accepted sample
            accepted = '(probe_Brunel_hasRich)&&(probe_Brunel_MC15TuneV1_ProbNNpi>0.4)' , ## ACCEPTED sample
            ## rejected sample
            rejected = '(probe_Brunel_hasRich)&&(probe_Brunel_MC15TuneV1_ProbNNpi<0.4)' , ## REJECTED sample
            ## binning in P
            xbins    = [ 3.2 , 6 , 9 , 12 , 15 , 20 , 25 , 30 , 35 , 40 , 45 , 50 , 60 , 70 , 80 , 90 , 100 , 110 , 120 , 150 ] ,
            ## binning in ETA
            ybins    = [ 2.0 , 2.25 , 2.5 , 2.75 , 3.0 , 3.25, 3.5 , 4.75 , 4.0 , 4.25 , 4.5 , 4.65 , 4.9 ] ,
            ## binning in #tracks
            zbins    = [ 0 , 150 , 250 , 400 , 1000 ] ,
            ## additional cuts (if any)
            cuts     = cuts ,
            ## "Accept"-function                              what  to project/draw                                 cuts&weight
            acc_fun  = lambda s,data : data.pproject ( s.ha , 'nTracks : probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % s.accepted , silent = True ) ,
            ## "Reject"-function                              what  to project/draw                                 cuts&weight
            rej_fun  = lambda s,data : data.pproject ( s.hr , 'nTracks : probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % s.rejected , silent = True ) )


# =============================================================================
if '__main__' == __name__:

    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(80 * '*')
    logger.info(' Author  : %s' % __author__)
    logger.info(' Version : %s' % __version__)
    logger.info(' Date    : %s' % __date__)
    logger.info(' Symbols : %s' % list(__all__))
    logger.info(80 * '*')

    #
    ## import function from Ostap
    #
    from Ostap.PidCalib2 import run_pid_calib
    import Ostap.Kisa

    ## use it!

    ## run_pid_calib ( KAON   , args = ['-y', '2016', 'K' ] )
    ## run_pid_calib ( PION   , args = ['-y', '2016', 'pi' , '-f' , '5' , '-q', ] )
    ## run_pid_calib ( PION   , args = ['-y', '2015', 'DSt3Pi_PiM' , '-f' , '10' ] )
    ## run_pid_calib ( PROTON , args = ['-y', '2016', 'P' , '-f' , '5' , '-v' , 'v5r1' ] )
    ## run_pid_calib ( PION   , args = ['-y', '2015', 'pi' , '-f' , '5' , '-v' ,  'v5r1' ] )

    run_pid_calib(
        KAON, args=['-y', '2016', 'K', '-f', '10', '-v', 'v5r1', '-q'])

    logger.info(80 * '*')

# =============================================================================
# The END
# =============================================================================
