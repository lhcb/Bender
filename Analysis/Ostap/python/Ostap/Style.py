#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file
# LHCb style file for ROOT-plots
#
# There are several predefined styles:
# - lhcbStyle    : modification of the standard LHCb-like style.
#                  It suits well to produce high quality plots for publishing.
#                  In LaTeX nice plots are obtained using 1-in-row layout.
# - lhcbStyle2   : modification of the standard LHCb-like style.
#                  It suits well to produce high quality downscaled plots for publishing.
#                  In LaTeX nice plots are obtained using 2-in-row layout.
# - lhcbStyle3   : modification of the standard LHCb-like style.
#                  It suits well to produce high quality downscaled plots for publishing.
#                  In LaTeX nice plots are obtained using 3-in-row layout.
# - lhcbStyleZ   : similar  to <code>lhcbStyle</code>, but with enlarged right margin.
#                  It is  good to produce <code>COLZ</code> plots
# - lhcbStyle2Z  : similar  to <code>lhcbStyle2</code>, but with enlarged right margin.
#                  It is  good to produce <code>COLZ</code> plots
# - lhcbStyle3Z  : similar  to <code>lhcbStyle3</code>, but with enlarged right margin.
#                  It is  good to produce <code>COLZ</code> plots
# =============================================================================
"""LHCb Style for ROOT-plots

- lhcbStyle    : modification of the standard LHCb-like style.
It suits well to produce high quality plots for publishing.
In LaTeX nice plots are obtained using 1-in-row layout.

- lhcbStyle2   : modification of the standard LHCb-like style.
It suits well to produce high quality downscaled plots for publishing.
In LaTeX nice plots are obtained using 2-in-row layout.

- lhcbStyle3   : modification of the standard LHCb-like style.
It suits well to produce high quality downscaled plots for publishing.
In LaTeX nice plots are obtained using 3-in-row layout.

- lhcbStyleZ   : similar to lhcbStyle, but with enlarged right margin.
It is  good to produce COLZ plots

- lhcbStyle2Z  : similar to lhcbStyle2, but with enlarged right margin.
It is  good to produce COLZ plots

- lhcbStyle3Z  : similar to lhcbStyle3, but with enlarged right margin.
It is  good to produce COLZ plots

"""
# =============================================================================
import ROOT
__all__ = (
    'lhcbStyle',
    'lhcbStyle2',
    'lhcbStyle3',
    'lhcbStyleZ',
    'lhcbStyle2Z',
    'lhcbStyle3Z',
    'lhcbLabel',
    'LHCbStyle',
    'lhcbLatex',
    'UseStyle',  ## context manager to use certain style
    'useStyle',  ## context manager (as function) to use  certain style
)
# =============================================================================
# logging
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' == __name__: logger = getLogger('Ostap.LHCbStyle')
else: logger = getLogger(__name__)
# =============================================================================
from Ostap.LHCbStyle import *


# =============================================================================
## Use some (temporary) style   as context manager
#  @code
#  with UseStyle ( lhcbStyle2 ) :
#  ...     h1 = ...
#  ...     h1.Draw()
#  @endcode
class UseStyle(object):
    """ Use some (temporary) style   as context manager
    >>> with UseStyle ( lhcbStyle2 ) :
    >>> ...     h1 = ...
    >>> ...     h1.Draw()
    """

    def __init__(self, style=None):

        if isinstance(style, int):
            if 1 == style: style = lhcbStyle
            elif 2 == style: style = lhcbStyle2
            elif 3 == style: style = lhcbStyle3

        if isinstance(style, str):
            if style.upper() in ('', '0', '1'): style = lhcbStyle
            elif '2' == style: style = lhcbStyle2
            elif '3' == style: style = lhcbStyle3
            elif style.upper() in ('Z', '1Z', 'Z1'): style = lhcbStyleZ
            elif style.upper() in ('Z', '2Z', 'Z2'): style = lhcbStyle2Z
            elif style.upper() in ('Z', '3Z', 'Z3'): style = lhcbStyle3Z

        ## use the style by name
        if isinstance(style, str):
            styles = ROOT.gROOT.GetListOfStyles()
            for s in styles:
                if s.GetName() == style:
                    style = s
                    break

        if not isinstance(style, ROOT.TStyle):
            logger.error('No valid style "%s" is found, use LHCbStyle' % style)
            from Ostap.LHCbStyle import lhcbStyle
            style = lhcbStyle

        self.__new_style = style
        self.__old_style = ROOT.gStyle

    ## context  manager: enter
    def __enter__(self):
        self.__force_style = ROOT.gROOT.GetForceStyle()
        self.__new_style.cd()
        ROOT.gROOT.ForceStyle(True)

    ## context  manager: exit
    def __exit__(self, *_):
        self.__old_style.cd()
        ROOT.gROOT.ForceStyle(self.__force_style)


# =============================================================================
## Use some (temporary) style   as context manager
#  @code
#  with useStyle ( lhcbStyle2 ) :
#  ...     h1 = ...
#  ...     h1.Draw()
#  @endcode
def useStyle(style=lhcbStyle):
    """ Use some (temporary) style   as context manager
    >>> with useStyle ( lhcbStyle2 ) :
    >>> ...     h1 = ...
    >>> ...     h1.Draw()
    """
    return UseStyle(style)


# =============================================================================
if '__main__' == __name__:

    import Ostap.Line
    logger.info(__file__ + '\n' + Ostap.Line.line)
    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(80 * '*')
    logger.info(' Symbols : %s' % list(__all__))
    logger.info(80 * '*')
    logger.info('Known styles:')
    for s in ROOT.gROOT.GetListOfStyles():
        logger.info('%25s : %-s' % ('"' + s.GetName() + '"', s.GetTitle()))
    logger.info(80 * '*')

# =============================================================================
# The END
# =============================================================================
