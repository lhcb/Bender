#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  very simple logger for Ostap
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2011-06-07
#
# =============================================================================
"""Very simple logger for Ostap
>>> logger = getLogger('MyLogger')
"""
# =============================================================================
__author__ = 'Vanya BELYAEV Ivan.Belyaev@itep.ru'
__date__ = "2012-03-16"
__version__ = '$Revision$'
__all__ = (
    'getLogger',  ## get (configured) logger
    'setLogging',  ## set disable level according to MSG.Level
    'LogLevel',  ## context manager to control output level
    'logLevel',  ## helper function to control output level
    'logVerbose',  ## helper function to control output level
    'logDebug',  ## helper function to control output level
    'logInfo',  ## helper function to control output level
    'logWarning',  ## helper function to control output level
    'logError',  ## helper function to control output level
    'logColor',  ## context manager to switch on  color logging locally
    'logNoColor',  ## context manager to switch off color logging locally
    'noColor',  ## context manager to switch off color logging locally
    'isatty',  ## is sys.stdout attached to terminal or not ?
    'terminal_size',  ## get terminal console size
    'multicolumn',  ## format list of string as multicolumn block of text
    'attention',  ##  "attention!" string
    'allright',  ##  "allright"   string
    'with_colors',  ## with colors?
)
# =============================================================================
import sys, os
# =============================================================================
## import actual logger
from AnalysisPython.Logger import (
    getLogger, setLogging, logLevel, logVerbose, logDebug, logInfo, logWarning,
    logError, logColor, logNoColor, noColor, make_colors, reset_colors,
    with_colors, attention, allright, colored_string, isatty)
# =============================================================================
if '__main__' == __name__: logger = getLogger('Ostap.Logger')
else: logger = getLogger(__name__)


# ============================================================================
def terminal_size():
    """Get the terminal console size"""
    try:
        import fcntl, termios, struct
        th, tw, hp, wp = struct.unpack(
            'HHHH',
            fcntl.ioctl(0, termios.TIOCGWINSZ, struct.pack('HHHH', 0, 0, 0,
                                                           0)))
        return th, tw
    except:
        return 20, 80


# =============================================================================
## format list of strings into multicolumn string
def multicolumn(lines, term_width=None, indent=0, pad=2):
    """Format list of strings into multicolumn string
    >>> strings =  ....
    >>> table   = multicolumn (  strings , indent = 2 )
    >>> print table
    """
    n_lines = len(lines)
    if n_lines == 0:
        return

    if not term_width:
        h, term_width = terminal_size()

    col_width = max(len(line) for line in lines)
    n_cols = int((term_width + pad - indent) / (col_width + pad))
    n_cols = min(n_lines, max(1, n_cols))

    col_len = int(n_lines / n_cols) + (0 if n_lines % n_cols == 0 else 1)
    if (n_cols - 1) * col_len >= n_lines:
        n_cols -= 1

    cols = [lines[i * col_len:i * col_len + col_len] for i in range(n_cols)]

    rows = list(zip(*cols))
    rows_missed = zip(*[col[len(rows):] for col in cols[:-1]])
    rows.extend(rows_missed)

    result = []
    for row in rows:
        line = " " * indent + (" " * pad).join(
            line.ljust(col_width) for line in row)
        result.append(line)
    return '\n'.join(result)


# =============================================================================
if __name__ == '__main__':

    import logging
    logging.disable(logging.VERBOSE - 1)

    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(' Author  : %s ' % __author__)
    logger.info(' Version : %s ' % __version__)
    logger.info(' Date    : %s ' % __date__)
    logger.info(' Symbols : %s ' % list(__all__))
    logger.info(80 * '*')

    with logColor():
        logger.verbose(80 * '*')
        logger.debug(80 * '*')
        logger.info(80 * '*')
        logger.error(80 * '*')
        logger.warning(80 * '*')
        logger.critical(80 * '*')
        logger.fatal(80 * '*')

    logger.verbose(80 * '*')
    logger.debug(80 * '*')
    logger.info(80 * '*')
    logger.error(80 * '*')
    logger.warning(80 * '*')
    logger.critical(80 * '*')
    logger.fatal(80 * '*')

# =============================================================================
# The END
# =============================================================================
