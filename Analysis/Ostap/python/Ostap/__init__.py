#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#
#  Helper module to load various Ostap-modules
#
#  This file is a part of
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV.
#  And it is based on the
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement
#  with the smear campaign of Dr.O.Callot et al.:
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2014-03-10
# =============================================================================
"""
Helper module to load various Ostap-modules

This file is a part of BENDER project:
``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV.

And it is based on the
LoKi project: ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement
with the smear campaign of Dr.O.Callot et al.:
``No Vanya's lines are allowed in LHCb/Gaudi software.''

"""
# =============================================================================
__author__ = "Vanya BELYAEV  Ivan.Belyaev@itep.ru"
__date__ = "2014-03-10"
__version__ = "$Revision$"
# =============================================================================
# logging
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' == __name__: logger = getLogger('Ostap')
else: logger = getLogger(__name__)
# =============================================================================
import ROOT, os

# =============================================================================
import Ostap.Fixes
from Ostap.Core import *
# =============================================================================

# =============================================================================
workdir = os.environ.get('OSTAP_DIR') or os.environ.get(
    'OSTAPDIR') or '$HOME/.ostap'
workdir = os.path.expandvars(workdir)

if not os.path.exists(workdir):
    try:
        os.mkdir(workdir)
        wdir = os.path.join(workdir, "cache")
        os.mkdir(wdir)
        logger.debug('Create working cache directory: %s' % wdir)
    except:
        pass

## play with macro path
macrodirs = ('$OSTAPMACRO', '$OSTAPMACROS', '$OSTAPDIR/macros',
             '$OSTAP_DIR/macros', '$OSTAPROOT/macros')


# =============================================================================
def _addMacroPath(path):
    """ Add path/item to the macro path
    >>> ROOT.gROOT.addMacroPath ( 'xxx/yyy/xxx' )
    >>> ROOT.gROOT.addMacroPath ( [ 'xxx/yyy/xxx' , 'xyz' , '$ROOTSYS/test' )
    """

    if isinstance(path, str): path = [path]

    import ROOT
    rmp = ROOT.gROOT.GetMacroPath()
    lmp = rmp.split(os.pathsep)
    while '' in lmp:
        lmp.remove('')

    for md in path:

        m = md
        m = os.path.expandvars(m)
        m = os.path.expanduser(m)
        if m and os.path.exists(m) and os.path.isdir(m):

            if m in lmp or md in lmp: continue
            lmp.insert(0, m)
            logger.debug('Add %s in ROOT macro path' % md)

    ROOT.gROOT.SetMacroPath(os.pathsep.join(lmp))


if not hasattr(ROOT.gROOT, 'addMacroPath'):
    ROOT.gROOT.addMacroPath = _addMacroPath

ROOT.gROOT.addMacroPath(macrodirs)

# =============================================================================
if __name__ == '__main__':

    import Ostap.Line
    logger.info(__file__ + '\n' + Ostap.Line.line)
    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(80 * '*')
    logger.info(' Author  : %s' % __author__)
    logger.info(' Version : %s' % __version__)
    logger.info(' Date    : %s' % __date__)
    logger.info(' Symbols : %s' % list(__all__))
    logger.info(80 * '*')

# =============================================================================
# The END
# =============================================================================
