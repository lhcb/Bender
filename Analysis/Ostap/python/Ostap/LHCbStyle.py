#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file
# LHCb style file for ROOT-plots
#
# There are several predefined styles:
# - lhcbStyle    : modification of the standard LHCb-like style.
#                  It suits well to produce high quality plots for publishing.
#                  In LaTeX nice plots are obtained using 1-in-row layout.
# - lhcbStyle2   : modification of the standard LHCb-like style.
#                  It suits well to produce high quality downscaled plots for publishing.
#                  In LaTeX nice plots are obtained using 2-in-row layout.
# - lhcbStyle3   : modification of the standard LHCb-like style.
#                  It suits well to produce high quality downscaled plots for publishing.
#                  In LaTeX nice plots are obtained using 3-in-row layout.
# - lhcbStyleZ   : similar  to <code>lhcbStyle</code>, but with enlarged right margin.
#                  It is  good to produce <code>COLZ</code> plots
# - lhcbStyle2Z  : similar  to <code>lhcbStyle2</code>, but with enlarged right margin.
#                  It is  good to produce <code>COLZ</code> plots
# - lhcbStyle3Z  : similar  to <code>lhcbStyle3</code>, but with enlarged right margin.
#                  It is  good to produce <code>COLZ</code> plots
# =============================================================================
"""LHCb Style for ROOT-plots

- lhcbStyle    : modification of the standard LHCb-like style.
It suits well to produce high quality plots for publishing.
In LaTeX nice plots are obtained using 1-in-row layout.

- lhcbStyle2   : modification of the standard LHCb-like style.
It suits well to produce high quality downscaled plots for publishing.
In LaTeX nice plots are obtained using 2-in-row layout.

- lhcbStyle3   : modification of the standard LHCb-like style.
It suits well to produce high quality downscaled plots for publishing.
In LaTeX nice plots are obtained using 3-in-row layout.

- lhcbStyleZ   : similar to lhcbStyle, but with enlarged right margin.
It is  good to produce COLZ plots

- lhcbStyle2Z  : similar to lhcbStyle2, but with enlarged right margin.
It is  good to produce COLZ plots

- lhcbStyle3Z  : similar to lhcbStyle3, but with enlarged right margin.
It is  good to produce COLZ plots

"""
# =============================================================================
import ROOT
__all__ = (
    'lhcbStyle',
    'lhcbStyle2',
    'lhcbStyle3',
    'lhcbStyleZ',
    'lhcbStyle2Z',
    'lhcbStyle3Z',
    'lhcbLabel',
    'LHCbStyle',
    'lhcbLatex',
)
# =============================================================================
# logging
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' == __name__: logger = getLogger('Ostap.LHCbStyle')
else: logger = getLogger(__name__)
# =============================================================================

## font
lhcbFont = 132  ## Times-Roman
## line thickness
lhcbWidth = 1

##define style for text
lhcbLabel = ROOT.TText()
lhcbLabel.SetTextFont(lhcbFont)
lhcbLabel.SetTextColor(1)
lhcbLabel.SetTextSize(0.04)
lhcbLabel.SetTextAlign(12)

## define style of latex text
lhcbLatex = ROOT.TLatex()
lhcbLatex.SetTextFont(lhcbFont)
lhcbLatex.SetTextColor(1)
lhcbLatex.SetTextSize(0.04)
lhcbLatex.SetTextAlign(12)


# =============================================================================
## define LHCb style for plots
def LHCbStyle(name="LHCbStyle",
              desc='Standard "LHCb-like" plots style',
              lineWidth=lhcbWidth,
              font=lhcbFont,
              makeNew=False,
              force=True,
              scale=1.0,
              colz=False):
    """Define LHCb-like style for the plots
    """
    obj = ROOT.gROOT.FindObject(name)
    if obj and issubclass(type(obj), ROOT.TStyle) and not makeNew:
        logger.info('The style %s is reused' % obj.GetName())
        if force:
            obd.cd()
            logger.info('The style %s is forced' % obj.GetName())
            ROOT.gROOT.SetStyle(obj.GetName())
            ROOT.gROOT.ForceStyle()
        return obj

    nam = name
    i = 1
    while obj:
        nam = name + '_%d' % i
        obj = ROOT.gROOT.FindObject(nam)
        i += 1

    style = ROOT.TStyle(nam, desc)
    logger.debug('New style %s is created' % style.GetName())

    ## use plain black on white colors
    style.SetFrameBorderMode(0)
    style.SetCanvasBorderMode(0)
    style.SetPadBorderMode(0)
    style.SetPadColor(0)
    style.SetCanvasColor(0)
    style.SetStatColor(0)

    ## set the paper & margin sizes
    style.SetPaperSize(20, 26)
    style.SetPadTopMargin(0.05)
    style.SetPadRightMargin(0.14 if colz else 0.05)
    style.SetPadBottomMargin(0.14)
    style.SetPadLeftMargin(0.14)

    ##  use large fonts
    style.SetTextFont(font)
    style.SetTextSize(0.08 * scale)
    style.SetLabelFont(font, "x")
    style.SetLabelFont(font, "y")
    style.SetLabelFont(font, "z")
    style.SetLabelSize(0.05 * scale, "x")
    style.SetLabelSize(0.05 * scale, "y")
    style.SetLabelSize(0.05 * scale, "z")
    style.SetTitleFont(font)
    style.SetTitleSize(0.06 * scale, "x")
    style.SetTitleSize(0.06 * scale, "y")
    style.SetTitleSize(0.06 * scale, "z")

    ## use bold lines and markers
    style.SetLineWidth(lineWidth)
    style.SetFrameLineWidth(lineWidth)
    style.SetHistLineWidth(lineWidth)
    style.SetFuncWidth(lineWidth)
    style.SetGridWidth(lineWidth)
    style.SetLineStyleString(2, "[12 12]")  ##  postscript dashes
    style.SetMarkerStyle(20)
    style.SetMarkerSize(1.2)

    ## label offsets
    style.SetLabelOffset(0.015)

    ## by default, do not display histogram decorations:
    style.SetOptStat(0)
    ## lhcbStyle->SetOptStat("emr");  ##  show only nent -e , mean - m , rms -r
    ## full opts at http://root.cern.ch/root/html/TStyle.html#TStyle:SetOptStat
    style.SetStatFormat("6.3g")  ##  specified as c printf options
    style.SetOptTitle(0)
    style.SetOptFit(0)
    ## lhcbStyle . SetOptFit(1011); // order is probability, Chi2, errors, parameters

    ## size of small lines at the end of error bars
    style.SetEndErrorSize(3)

    ## look of the statistics box:
    style.SetStatBorderSize(0)
    style.SetStatFont(font)
    style.SetStatFontSize(0.05 * scale)
    style.SetStatX(0.9)
    style.SetStatY(0.9)
    style.SetStatW(0.25)
    style.SetStatH(0.15)
    ## put tick marks on top and RHS of plots
    style.SetPadTickX(1)
    style.SetPadTickY(1)

    ## histogram divisions: only 5 in x to avoid label overlaps
    style.SetNdivisions(505, "x")
    style.SetNdivisions(510, "y")

    ## add several useful line types:
    style.SetLineStyleString(11, "76  24")
    style.SetLineStyleString(12, "60  16 8 16")
    style.SetLineStyleString(13, "168 32")
    style.SetLineStyleString(14, "32  32")

    ## Palettes:
    #  @see  https://root.cern.ch/doc/master/classTColor.html
    ## kDeepSea=51,          kGreyScale=52,    kDarkBodyRadiator=53,
    ## kBlueYellow= 54,      kRainBow=55,      kInvertedDarkBodyRadiator=56,
    ## kBird=57,             kCubehelix=58,    kGreenRedViolet=59,
    ## kBlueRedYellow=60,    kOcean=61,        kColorPrintableOnGrey=62,
    ## kAlpine=63,           kAquamarine=64,   kArmy=65,
    ## kAtlantic=66,         kAurora=67,       kAvocado=68,
    ## kBeach=69,            kBlackBody=70,    kBlueGreenYellow=71,
    ## kBrownCyan=72,        kCMYK=73,         kCandy=74,
    ## kCherry=75,           kCoffee=76,       kDarkRainBow=77,
    ## kDarkTerrain=78,      kFall=79,         kFruitPunch=80,
    ## kFuchsia=81,          kGreyYellow=82,   kGreenBrownTerrain=83,
    ## kGreenPink=84,        kIsland=85,       kLake=86,
    ## kLightTemperature=87, kLightTerrain=88, kMint=89,
    ## kNeon=90,             kPastel=91,       kPearl=92,
    ## kPigeon=93,           kPlum=94,         kRedBlue=95,
    ## kRose=96,             kRust=97,         kSandyTerrain=98,
    ## kSienna=99,           kSolar=100,       kSouthWest=101,
    ## kStarryNight=102,     kSunset=103,      kTemperatureMap=104,
    ## kThermometer=105,     kValentine=106,   kVisibleSpectrum=107,
    ## kWaterMelon=108,      kCool=109,        kCopper=110,
    ## kGistEarth=111,       kViridis=112,     kCividis=113

    style.SetPalette(ROOT.kDarkBodyRadiator)  ##  dark-body radiator pallete
    style.SetNumberContours(255)

    if force:
        style.cd()
        logger.debug('The style %s is forced' % style.GetName())
        ROOT.gROOT.SetStyle(style.GetName())
        ROOT.gROOT.ForceStyle()

    return style


## style for half-page-width COLZ plots
lhcbStyle2Z = LHCbStyle(
    'LHCbStyle2Z',
    desc=
    "Style, suitable to produce downscaled (2-in-row) plot with large right margin (COLZ)",
    scale=1.41,
    makeNew=True,
    colz=True)
## style for one-third-page-width COLZ plots
lhcbStyle3Z = LHCbStyle(
    'LHCbStyle3Z',
    desc=
    "Style, suitable to produce downscaled (3-in-row) plot with large right margin (COLZ)",
    scale=1.71,
    makeNew=True,
    colz=True)
## style for standard COLZ plots
lhcbStyleZ = LHCbStyle(
    'LHCbStyleZ',
    desc="Style, suitable to produce plot with large right margin (COLZ)",
    makeNew=True,
    colz=True)

## style for half-page-width plots
lhcbStyle2 = LHCbStyle(
    'LHCbStyle2',
    desc="Style, suitable to produce downscaled (2-in-row) plots",
    scale=1.41,
    makeNew=True)
## style for one-third-page-width plots
lhcbStyle3 = LHCbStyle(
    'LHCbStyle3',
    desc="Style, suitable to produce downscaled (3-in-row) plots",
    scale=1.71,
    makeNew=True)
## style for standard plots
lhcbStyle = LHCbStyle('LHCbStyle', force=True, makeNew=True)

## aliases
lhcbFull = lhcbStyle
lhcbHalf = lhcbStyle2
lhcbThird = lhcbStyle3
lhcbFullZ = lhcbStyleZ
lhcbHalfZ = lhcbStyle2Z
lhcbThirdZ = lhcbStyle3Z


## ============================================================================
# //////////////////////////////////////////////////////////////////////////
# // routine to print 'LHCb', 'LHCb Preliminary' on plots
# // options: optLR=L (top left) / R (top right) of plots
# //          optPrelim= Final (LHCb), Prelim (LHCb Preliminary), Other
# //          optText= text printed if 'Other' specified
# ////////////////////////////////////////////////////////////////////
def printLHCb(LR="L", prelim="Final", text=""):
    """
    ////////////////////////////////////////////////////////////////////
    // routine to print 'LHCb', 'LHCb Preliminary' on plots
    // options: optLR=L (top left) / R (top right) of plots
    //          optPrelim= Final (LHCb), Prelim (LHCb Preliminary), Other
    //          optText= text printed if 'Other' specified
    ////////////////////////////////////////////////////////////////////
    """

    if not LR in ('L', 'R'):
        raise TypeError("Unknown LR-option: %s" % LR)

    global lhcbName
    if 'R' == LR:
        lhcbName = ROOT.TPaveText(0.70 - lhcbStyle.GetPadRightMargin(),
                                  0.75 - lhcbStyle.GetPadTopMargin(),
                                  0.95 - lhcbStyle.GetPadRightMargin(),
                                  0.85 - lhcbStyle.GetPadTopMargin(), "BRNDC")

    else:  ## LR="L"
        lhcbName = ROOT.TPaveText(lhcbStyle.GetPadLeftMargin() + 0.05,
                                  0.87 - lhcbStyle.GetPadTopMargin(),
                                  lhcbStyle.GetPadLeftMargin() + 0.30,
                                  0.95 - lhcbStyle.GetPadTopMargin(), "BRNDC")

    if "Final" == prelim: lhcbName.AddText("LHCb")
    elif "Prelim" == prelim:
        lhcbName.AddText("#splitline{LHCb}{#scale[1.0]{Preliminary}}")
    else:
        lhcbName.AddText(text)

    lhcbName.SetFillColor(0)
    lhcbName.SetTextAlign(12)
    lhcbName.SetBorderSize(0)
    lhcbName.Draw()

    return lhcbName


# =============================================================================
if '__main__' == __name__:

    import Ostap.Line
    logger.info(__file__ + '\n' + Ostap.Line.line)
    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(80 * '*')
    logger.info(' Symbols : %s' % list(__all__))
    logger.info(80 * '*')
    logger.info('Known styles:')

# =============================================================================
# The END
# =============================================================================
