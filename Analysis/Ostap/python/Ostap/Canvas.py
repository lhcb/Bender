#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file Canvas.py
#
#     .oooooo.                .
#    d8P'  `Y8b             .o8
#   888      888  .oooo.o .o888oo  .oooo.   oo.ooooo.
#   888      888 d88(  "8   888   `P  )88b   888' `88b
#   888      888 `"Y88b.    888    .oP"888   888   888
#   `88b    d88' o.  )88b   888 . d8(  888   888   888
#    `Y8bood8P'  8""888P'   "888" `Y888""8o  888bod8P'
#                                            888
#                                           o888o
#
#  Simple helper module to get ROOT TCanvas
#
#  This file is a part of
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV.
#  And it is based on the
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  <b>``C++ ToolKit for Smart and Friendly Physics Analysis''</b>
#
#  By usage of this code one clearly states the disagreement
#  with the smear campaign of Dr.O.Callot et al.:
#  ``No Vanya's lines are allowed in LHCb/Gaudi software''
#
#  @date   2012-02-15
#  @author Vanya BELYAEV Ivan.Belyaevitep.ru
#
# =============================================================================
""" Simple helper module to get ROOT TCanvas

    This file is a part of BENDER project:

  ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV.

And it is based on the LoKi project:

   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement with the smear campaign
of Dr.O.Callot et al.:

   ``No Vanya's lines are allowed in LHCb/Gaudi software''
"""
# =============================================================================
__author__ = 'Vanya BELYAEV Ivan.Belyaev@itep.ru'
__date__ = "2014-10-19"
__version__ = '$Revision$'
__all__ = (
    'getCanvas',
    'getCanvases',
    'canvas_partition',  ## useful  NxM partition of canvas
    'canvas_pull',  ## 1x2 partition, for pull/residual plots
    'AutoPlots',  ## context manager to activate the auto-plotting machinery
    'auto_plots',  ## ditto, but as function
)
# =============================================================================
import ROOT, os
from collections import defaultdict
import Ostap.LHCbStyle
from Ostap.Utils import rootWarning, rooSilent
# =============================================================================
# logging
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' == __name__: logger = getLogger('Ostap.Canvas')
else: logger = getLogger(__name__)
# =============================================================================
_canvases = {}


# =============================================================================
## get the canvas
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2014-10-19
def getCanvas(
        name='glCanvas',  ## canvas name
        title='Ostap',  ## canvas title
        width=1000,  ## canvas width  in pixels
        height=800):  ## canvas height in pixels
    """Get create canvas/create new canvas

    >>> cnv = getCanvas ( 'glnewCanvas' , width = 1200 , height = 1000 )
    """
    name = '%s/%s' % (name, ROOT.gStyle.GetName())
    cnv = _canvases.get(name, None)
    if not cnv:
        ## create new canvas
        ## cnv  = ROOT.TCanvas ( 'glCanvas', 'Ostap' , width , height )
        cnv = ROOT.TCanvas(name, 'Ostap', width, height)
        ## adjust newly created canvas
        ## @see http://root.cern.ch/root/html/TCanvas.html#TCanvas:TCanvas@4
        if not ROOT.gROOT.IsBatch():
            dw = width - cnv.GetWw()
            dh = height - cnv.GetWh()
            cnv.SetWindowSize(width + dw, height + dh)

        ##
        _canvases[name] = cnv

    return cnv


# =============================================================================
## get all known canvases
def getCanvases():
    """ Get all known canvases """
    return _canvases.keys()


import atexit


@atexit.register
def _remove_canvases_():
    keys = _canvases.keys()
    for k in keys:
        del _canvases[k]


# =============================================================================
## perform partition of Canvas into
#  @code
#  canvas = ...
#  pads   = canvas.partition ( 3 , 2 )
#  for i in range(3) :
#    for j in range(2) :
#       histo_ij = ...
#       canvas.cd(0)
#       pad = pads[i,j]
#       pad.Draw()
#       pad.cd()
#       histo_ij.Draw()
#  @endcode
#  @see https://root.cern/doc/master/canvas2_8C.html
def canvas_partition(canvas,
                     nx,
                     ny,
                     left_margin=0.14,
                     right_margin=0.05,
                     bottom_margin=0.16,
                     top_margin=0.05,
                     hSpacing=0.0,
                     vSpacing=0.0):
    """Perform partition of Canvas into pads with no inter-margins

    canvas = ...
    nx = 3 , ny = 2
    pads   = canvas.partition ( nx  , ny )
    for i in range(nx) :
    ... for j in range(ny) :
    ... ... histo_ij = ...
    ... ... canvas.cd(0)
    ... ... pad = pads[i,j]
    ... ... pad.Draw()
    ... ... pad.cd()
    ... ... histo_ij.Draw()

    @see https://root.cern/doc/master/canvas2_8C.html

    """

    if not isinstance(nx, int) or nx <= 0:
        raise AttributeError('partition: invalid nx=%s' % nx)
    if not isinstance(ny, int) or ny <= 0:
        raise AttributeError('partition: invalid ny=%s' % ny)

    ## get the window size
    wsx = abs(canvas.GetWindowWidth())
    wsy = abs(canvas.GetWindowHeight())

    #
    ## if parametes given in the absolute units, convert them into relative coordinates
    #

    if left_margin < 0: left_margin = abs(left_margin) / wsx
    if right_margin < 0: right_margin = abs(right_margin) / wsx
    if bottom_margin < 0: bottom_margin = abs(bottom_margin) / wsy
    if top_margin < 0: top_margin = abs(bottom_margin) / wsy

    if hSpacing < 0: hSpacing = abs(hSpacing) / wsx
    if vSpacing < 0: vSpacing = abs(vSpacing) / wsy

    #
    ## check consistency
    #
    if 1 <= left_margin:
        raise AttributeError(
            'partition: invalid   left margin=%f' % left_margin)
    if 1 <= right_margin:
        raise AttributeError(
            'partition: invalid  right margin=%f' % right_margin)
    if 1 <= bottom_margin:
        raise AttributeError(
            'partition: invalid bottom margin=%f' % bottom_margin)
    if 1 <= top_margin:
        raise AttributeError(
            'partition: invalid    top margin=%f' % top_margin)

    if hasattr(canvas, 'pads'):
        while canvas.pads:
            i, p = canvas.pads.popitem()
            logger.verbose('delete pas %s' % p.GetName())
            del p
    ## make new empty dictionary
    canvas.pads = {}

    vStep = (1.0 - bottom_margin - top_margin - (ny - 1) * vSpacing) / ny
    if 0 > vStep: raise AttributeError('partition: v-step=%f' % vStep)

    hStep = (1.0 - left_margin - right_margin - (nx - 1) * vSpacing) / nx
    if 0 > hStep: raise AttributeError('partition: h-step=%f' % hStep)

    hposr, hposl, hmarr, hmarl, hfactor = 0., 0., 0., 0., 0.
    vposr, vposd, vmard, vmaru, vfactor = 0., 0., 0., 0., 0.

    for ix in range(nx):

        if 0 == ix:
            hposl = 0
            hposr = left_margin + hStep
            hfactor = hposr - hposl
            hmarl = left_margin / hfactor
            hmarr = 0.0
        elif nx == ix + 1:
            hposl = hposr + hSpacing
            hposr = hposl + hStep + right_margin
            hfactor = hposr - hposl
            hmarl = 0.0
            hmarr = right_margin / hfactor
        else:
            hposl = hposr + hSpacing
            hposr = hposl + hStep
            hfactor = hposr - hposl
            hmarl = 0.0
            hmarr = 0.0

        for iy in range(ny):
            if 0 == iy:
                vposd = 0.0
                vposu = bottom_margin + vStep
                vfactor = vposu - vposd
                vmard = bottom_margin / vfactor
                vmaru = 0.0
            elif ny == iy + 1:
                vposd = vposu + vSpacing
                vposu = vposd + vStep + top_margin
                vfactor = vposu - vposd
                vmard = 0.0
                vmaru = top_margin / vfactor
            else:
                vposd = vposu + vSpacing
                vposu = vposd + vStep
                vfactor = vposu - vposd
                vmard = 0.0
                vmaru = 0.0

            canvas.cd(0)
            pname = 'pad_%s_%d_%d' % (canvas.GetName(), ix, iy)
            pad = ROOT.gROOT.FindObject(pname)
            if pad: del pad
            pad = ROOT.TPad(pname, '', hposl, vposd, hposr, vposu)

            logger.verbose(' Create pad[%d,%d]=(%f,%f,%f,%f),[%f,%f,%f,%f] %s '
                           % (ix, iy, hposl, vposd, hposr, vposu, hmarl, hmarr,
                              vmard, vmaru, pad.GetName()))

            pad.SetLeftMargin(hmarl)
            pad.SetRightMargin(hmarr)
            pad.SetBottomMargin(vmard)
            pad.SetTopMargin(vmaru)

            pad.SetFrameBorderMode(0)
            pad.SetBorderMode(0)
            pad.SetBorderSize(0)

            ROOT.SetOwnership(pad, True)

            if not hasattr(canvas, 'pads'): canvas.pads = {}
            canvas.pads[(ix, iy)] = pad

    return canvas.pads


ROOT.TCanvas.partition = canvas_partition


# ==============================================================================
##  Perform partition of Canvas into 1x2 non-equal pads with no inter-margins
#  @code
#  canvas    = ...
#  pad_u, pud_b= canvas.pull_partition ( 0.20 )
#  canvas.cd(0)
#  pad_u.Draw()
#  pad_u.cd()
#  object1.Draw()
#  canvas.cd(0)
#  pad_b.Draw()
#  pad_b.cd()
#  object2.Draw()
#  @endcode
def canvas_pull(canvas,
                ratio=0.80,
                left_margin=0.14,
                right_margin=0.05,
                bottom_margin=0.14,
                top_margin=0.05,
                hSpacing=0.0,
                vSpacing=0.0):
    """Perform partition of Canvas into 1x2 non-equal pads with no inter-margins

    >>> canvas    = ...
    >>> pad_u, pud_b= canvas.pull_partition ( 0.20 )

    >>> canvas.cd(0)
    >>> pad_u.Draw()
    >>> pad_u.cd()
    >>> object1.Draw()

    >>> canvas.cd(0)
    >>> pad_b.Draw()
    >>> pad_b.cd()
    >>> object2.Draw()
    """

    ## get the window size
    wsx = abs(canvas.GetWindowWidth())
    wsy = abs(canvas.GetWindowHeight())

    #
    ## if parametes given in the absolute units, convert them into relative coordinates
    #

    if left_margin < 0: left_margin = abs(left_margin) / wsx
    if right_margin < 0: right_margin = abs(right_margin) / wsx
    if bottom_margin < 0: bottom_margin = abs(bottom_margin) / wsy
    if top_margin < 0: top_margin = abs(bottom_margin) / wsy

    if hSpacing < 0: hSpacing = abs(hSpacing) / wsx
    if vSpacing < 0: vSpacing = abs(vSpacing) / wsy

    hposr, hposl, hmarr, hmarl, hfactor = 0., 0., 0., 0., 0.
    vposr, vposd, vmard, vmaru, vfactor = 0., 0., 0., 0., 0.

    nx = 1
    ny = 2

    vStep = (1.0 - bottom_margin - top_margin - (ny - 1) * vSpacing) / ny
    if 0 > vStep: raise AttributeError('partition: v-step=%f' % vStep)

    hStep = (1.0 - left_margin - right_margin - (nx - 1) * vSpacing) / nx
    if 0 > hStep: raise AttributeError('partition: h-step=%f' % hStep)

    hposl = 0
    hposr = left_margin + hStep
    hfactor = hposr - hposl
    hmarl = left_margin / hfactor
    hmarr = 0.0

    if hasattr(canvas, 'pads'):
        del canvas.pads

    vStep0 = 2 * vStep * 1 / (1 + ratio)
    vStep1 = 2 * vStep * ratio / (1 + ratio)

    ix = 0
    for iy in range(2):

        if 0 == iy:
            vposd = 0.0
            vposu = bottom_margin + vStep0
            vfactor = vposu - vposd
            vmard = bottom_margin / vfactor
            vmaru = 0.0
        else:
            vposd = vposu + vSpacing
            vposu = vposd + vStep1 + top_margin
            vfactor = vposu - vposd
            vmard = 0.0
            vmaru = top_margin / vfactor

        canvas.cd(0)
        pname = 'pad_%s_%d_%d' % (canvas.GetName(), ix, iy)
        pad = ROOT.gROOT.FindObject(pname)
        if pad: del pad
        pad = ROOT.TPad(pname, '', hposl, vposd, hposr, vposu)

        logger.verbose(' Create pad[%d,%d]=(%f,%f,%f,%f),[%f,%f,%f,%f] %s ' %
                       (ix, iy, hposl, vposd, hposr, vposu, hmarl, hmarr,
                        vmard, vmaru, pad.GetName()))

        pad.SetLeftMargin(hmarl)
        pad.SetRightMargin(hmarr)
        pad.SetBottomMargin(vmard)
        pad.SetTopMargin(vmaru)

        pad.SetFrameBorderMode(0)
        pad.SetBorderMode(0)
        pad.SetBorderSize(0)

        ROOT.SetOwnership(pad, True)

        if not hasattr(canvas, 'pads'): canvas.pads = []
        canvas.pads.append(pad)

    canvas.pads = tuple(reversed(canvas.pads))
    return canvas.pads


ROOT.TCanvas.pull_partition = canvas_pull

ROOT.gROOT.SetDefCanvasName('glCanvas')

# =============================================================================
all_extensions = ('pdf', 'png', 'gif', 'eps', 'ps', 'cxx', 'c', 'jpg', 'jpeg',
                  'svg', 'root', 'xml', 'xpm', 'tiff', 'tex')


# =============================================================================
## define simplified print for TCanvas
def _cnv_print_(cnv, fname, exts=('pdf', 'png', 'eps', 'C')):
    """A bit simplified version for TCanvas print
    >>> canvas.print ( 'fig' )
    """
    #
    n, d, e = fname.rpartition('.')
    if d and e.lower() in all_extensions:
        with rootWarning():
            cnv.Print(fname)
            logger.debug('Canvas --> %s' % fname)
            return cnv

    es = set()
    for ext in exts:
        with rootWarning():
            name = fname + '.' + ext
            cnv.Print(name)
            if os.path.exists(name): es.add(ext)

    if es:
        es = list(es)
        es.sort()
        es = tuple(es)
        logger.debug('Canvas --> %s.%s' % (fname, es))

    return cnv


# =============================================================================
## define streamer for canvas
#  @code
#  canvas >> 'a'
#  @endcode
def _cnv_rshift_(cnv, fname):
    """Very simple print for canvas:
    >>> canvas >> 'a'
    """
    return _cnv_print_(cnv, fname)


ROOT.TVirtualPad.print_ = _cnv_print_
ROOT.TVirtualPad.__rshift__ = _cnv_rshift_


# =============================================================================
# Auto-plotting
# =============================================================================
## @class AutoPlots
#  Helper structure/context manager to setup "auto-plotting"
#  all produced plots will be saved
class AutoPlots(object):
    """helper structure to setup ``auto-plotting''
    all produced plots will be saved
    """

    __tmpdir = []
    __auto_plots = []
    __counts = defaultdict(int)
    __plots = set()

    @classmethod
    def use_auto(cls, pattern):
        if not cls.__auto_plots or pattern not in cls.__auto_plots:
            logger.info(
                'Global auto-plot is activated for "%s" pattern ' % pattern)
            cls.__auto_plots.append(pattern)

    @classmethod
    def plots(cls):
        return cls.__plots

    @classmethod
    def plot(cls):
        """Get the plot name for auto-plotting:
        >>> plotname = AutoPlot.plot()
        >>> if plotname : canvas >> plotname
        """
        if not cls.__auto_plots: return False
        p = cls.__auto_plots[-1]
        c = cls.__counts[p]
        c %= 10000
        cls.__counts[p] = c + 1

        pname = p % c
        cls.__plots.add(pname)

        return pname

    def __init__(self, pattern='ostap_plot_%0.4d', directory='.'):

        path = os.path.join(directory, pattern)
        directory, pattern = os.path.split(path)

        if directory and not os.path.exists(directory):
            try:
                os.makedirs(directory)
            except OSError:
                logger.error("Can't create directory \"%s\"" % directory)
                directory = None

        if directory and os.path.exists(directory):
            if not os.path.isdir(directory):
                logger.error("Invalid directory \"%s\"" % directory)
                directory = None

        if directory and os.path.exists(directory) and os.path.isdir(
                directory):
            if not os.access(directory, os.W_OK):
                logger.error("Non-writeable directory \"%s\"" % directory)
                directory = None

        if not directory:
            if not self.__tmpdir:
                import tempfile
                self.__tmpdir.append(tempfile.mkdtemp(prefix='plots_'))
                logger.info('AutoPlots: use tmp directory "%s"' % directory)
            directory = self.__tmpdir[0]

        ## check the validity of pattern
        try:
            r = pattern % 999
        except TypeError:
            pattern = 'ostap_%0.4d'
            logger.info('Pattern is redefine to be "%s"' % pattern)

        self.__pattern = os.path.join(directory, pattern)

    @property
    def pattern(self):
        """``pattern'' -  the pattern for the file name"""
        return self.__pattern

    ## context manager
    def __enter__(self):
        self.__auto_plots.append(self.pattern)
        return self

    def __exit__(self, *_):
        self.__auto_plots.remove(self.pattern)


# =============================================================================
## Helper function /context manager to setup "auto-plotting"
#  all produced plots will be saved
#  @code
#  with auto_plot ( 'all_%d'  , directory  = 'plots' ) :
#  ...     a.draw()
#  ...     b.draw()
#  ...     c.draw()
#  ...     d.draw()
#  @endcode
def auto_plot(pattern='ostap_plot_%0.4d', directory='.'):
    """Helper function /context manager to setup "auto-plotting"
    all produced plots will be saved
    with auto_plots ( 'all_%d'  , directory  = 'plots' ) :
    ...     a.draw()
    ...     b.draw()
    ...     c.draw()
    ...     d.draw()
    """
    return AutoPlots(pattern=pattern, directory=directory)


# =============================================================================
##  new draw method: silent draw
def _to_draw_(obj, *args, **kwargs):
    """ (silent) Draw of ROOT object
    >>> obj
    >>> obj.Draw()  ##
    >>> obj.draw()  ## ditto
    """
    with rootWarning(), rooSilent(2):
        result = obj.Draw(*args, **kwargs)
        if ROOT.gPad:
            plot = AutoPlots.plot()
            if plot: ROOT.gPad >> plot
        return result


# =============================================================================
## decorate ROOT.TObject
if not hasattr(ROOT.TObject, 'draw_with_autoplot'):

    ## add new method
    ROOT.TObject.draw_with_autoplot = _to_draw_
    ## save old method
    if hasattr(ROOT.TObject, 'draw'):
        ROOT.TObject._draw_backup = ROOT.TObject.draw

    ROOT.TObject.draw = ROOT.TObject.draw_with_autoplot

import atexit


@atexit.register
def _remove_canvases_():
    keys = _canvases.keys()
    for k in keys:
        del _canvases[k]
    plts = AutoPlots.plots()
    dirs = set()
    plot = set()
    import glob
    for p in plts:
        pattern = p + '.*'
        for i in glob.iglob(pattern):
            d, f = os.path.split(i)
            dirs.add(d)
            plot.add(pattern)

    nplot = len(plot)
    if nplot:
        ldirs = len(dirs)
        if 1 == ldirs:
            logger.info(
                "Created %d plots in '%s' directory" % (nplot, dirs.pop()))
        else:
            logger.info(
                "Created %d plots in %d directories" % (nplot, len(dirs)))


# =============================================================================
if '__main__' == __name__:

    from Ostap.Line import line
    logger.info(__file__ + '\n' + line)
    logger.info(80 * '*')
    logger.info(__doc__)
    logger.info(80 * '*')
    logger.info(' Author  : %s' % __author__)
    logger.info(' Version : %s' % __version__)
    logger.info(' Date    : %s' % __date__)
    logger.info(' Symbols : %s' % list(__all__))
    logger.info(80 * '*')

# =============================================================================
# The END
# =============================================================================
