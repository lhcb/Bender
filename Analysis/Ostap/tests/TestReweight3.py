#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file TestReweigt3.py
#  Test for the abstract reweighting machinery
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2018-03-26
# =============================================================================
"""Test for the abstract machinery:
 - use function as ``data distribution''
 - use the stream of random numbers as ``simulated sample''
"""
# =============================================================================
__version__ = "$Revision:"
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2018-03-25"
__all__ = ()  ## nothing to be imported
# =============================================================================
import ROOT, random, math, os, time
from Ostap.PyRoUts import *
import Ostap.ZipShelve as DBASE
# =============================================================================
# logging
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__ or '__builtin__' == __name__:
    logger = getLogger('Ostap.TestReweight3')
else:
    logger = getLogger(__name__)

# =============================================================================
import tempfile
dbname = os.path.join(tempfile.gettempdir(), 'test_reweight3.db')
## set the seed for random data
seed = 1234567890L
random.seed(1234567890L)


# =============================================================================
## 1)  ``DATA distribution''  - plain function
## data    =  lambda s : math.sin ( x / math.pi )
def data(x):
    return 0.5 + math.sin(x * math.pi)**2


# =============================================================================

# =============================================================================
## 2) ``simulation template'' -  histogram template for simulated sample
mc_hist = ROOT.TH1F('hMC', '', 20, 0, 1)


# =============================================================================
def mc_sample():
    x = random.expovariate(1)
    while x > 1:
        x -= 1
    return x


# =============================================================================
## 3) create empty database with initial weights
# =============================================================================
import Ostap.ZipShelve as DBASE
if os.path.exists(dbname): os.remove(dbname)
with DBASE.open(dbname, 'c') as db:
    pass

# ============================================================================
## 4) prepare reweigthing iterations
# ===========================================================================
from Ostap.Reweighting import Weight, makeWeights, WeightingPlot
from Ostap.Selectors import SelectorWithVars, Variable

h_iters = []  ## histograms to vizualize the  result of iterations
maxIter = 100
for iter in range(maxIter):

    ##                                             accessor       address in DB
    weighter = Weight(dbname, (Weight.Var(lambda x: x, 'x-reweight'), ))

    ## create ``weighted'' simulated  dataset using  current weights
    selector = SelectorWithVars(
        selection='1<2',  ## fake one :-( to be removed soon
        silence=True,
        variables=[
            Variable('x', 'x-var', 0, 1, lambda x: x),
            Variable('weight', 'weight', accessor=weighter)
        ])

    for i in range(1000000):
        x = mc_sample()
        selector(x)

    mcds = selector.data
    ##
    mcds.project(mc_hist, 'x', 'weight')
    h_iters.append(mc_hist.clone())

    ##  update weights: the rule to create weighted simulated histogram
    plots = [WeightingPlot('x', 'weight', 'x-reweight', data, mc_hist)]

    ## calculate updated weights and add them into   database
    more = makeWeights(mcds, plots, dbname, delta=0.01)

    mcds.reset()
    del mcds
    del selector

    if not more and 2 <= iter:
        logger.info('No more iterations are needed #%d' % iter)
        break

# =============================================================================
##  5) Draw the  first and the last iterations
# =============================================================================
h_first = h_iters[0].density()
h_last = h_iters[-1].density()
h_first.SetMinimum(0)
h_first.SetMaximum(2)
h_first.blue()
h_last.red()
h_first.draw('e1')
h_last.draw('same e1')
time.sleep(10)

# =============================================================================
## 6) create the weighted generator
# =============================================================================
weighter = Weight(dbname, (Weight.Var(lambda x: x, 'x-reweight'), ))


# =============================================================================
## 7) ``weighted'' simulated sample
def mc_weighted_sample(N):
    n = 0
    while n < N:
        x = mc_sample()
        w = weighter(x)
        n += 1
        yield x, w


# =============================================================================
## 8) test the weighted  sample
# =============================================================================
mc_final = ROOT.TH1F('final', 'final weighted distribution', 50, 0, 1)
mc_final.Sumw2()
for x, w in mc_weighted_sample(1000000):
    mc_final.Fill(x, w)

from LHCbMath.deriv import integral
mc_final = mc_final.density()
mc_final *= integral(data, 0, 1)
mc_final.green()
mc_final.SetMinimum(0)
mc_final.SetMinimum(2)


# ==============================================================================
## 9) draw the ``data distribution''
# ==============================================================================
def datafun(x):
    return data(x[0])


f1 = ROOT.TF1('f1', datafun, 0, 1)
f1.SetNpx(1000)
f1.SetLineColor(2)
f1.SetLineWidth(2)

mc_final.draw('e1')
f1.draw('same')
mc_final.draw('same')
time.sleep(10)

# =============================================================================
# The END
# =============================================================================
