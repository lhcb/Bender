#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file Test3DFit.py
#  Tests for 3D-fit machinery
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2017-11-22
# =============================================================================
""" Tests for 3D-fit machinery
"""
# =============================================================================
__version__ = "$Revision:"
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2017-11-22"
__all__ = ()  ## nothing to be imported
# =============================================================================
import ROOT, random
from Ostap.PyRoUts import *
from Ostap.Utils import rooSilent, timing
# =============================================================================
# logging
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__ or '__builtin__' == __name__:
    logger = getLogger('Ostap.Test3DFit')
else:
    logger = getLogger(__name__)
# =============================================================================
logger.info('Test for 3D nonfactorizable fit models from Analysis/Ostap')
# =============================================================================
## make simple test mass
m_x = ROOT.RooRealVar('mass_x', 'Some test mass(X)', 3, 3.2)
m_y = ROOT.RooRealVar('mass_y', 'Some test mass(Y)', 3, 3.2)
m_z = ROOT.RooRealVar('mass_z', 'Some test mass(Z)', 3, 3.2)

## book very simple data set
varset = ROOT.RooArgSet(m_x, m_y, m_z)
dataset = ROOT.RooDataSet(dsID(), 'Test Data set-1', varset)

## SxSxS
for i in xrange(0, 10000):

    x = random.gauss(3.1, 0.015)
    y = random.gauss(3.1, 0.015)
    z = random.gauss(3.1, 0.015)

    m_x.setVal(x)
    m_y.setVal(y)
    m_z.setVal(z)

    dataset.add(varset)

## SxSxB
for i in xrange(0, 5000):

    x = random.gauss(3.1, 0.015)
    y = random.gauss(3.1, 0.015)
    z = random.uniform(*m_z.minmax())

    m_x.setVal(x)
    m_y.setVal(y)
    m_z.setVal(z)

    dataset.add(varset)

    x = random.gauss(3.1, 0.015)
    y = random.uniform(*m_y.minmax())
    z = random.gauss(3.1, 0.015)

    m_x.setVal(x)
    m_y.setVal(y)
    m_z.setVal(z)

    dataset.add(varset)

    x = random.uniform(*m_x.minmax())
    y = random.gauss(3.1, 0.015)
    z = random.gauss(3.1, 0.015)

    m_x.setVal(x)
    m_y.setVal(y)
    m_z.setVal(z)

    dataset.add(varset)

## SxBxB
for i in xrange(0, 8000):

    x = random.gauss(3.1, 0.015)
    y = random.uniform(*m_y.minmax())
    z = random.uniform(*m_z.minmax())

    m_x.setVal(x)
    m_y.setVal(y)
    m_z.setVal(z)

    dataset.add(varset)

    x = random.uniform(*m_x.minmax())
    y = random.gauss(3.1, 0.015)
    z = random.uniform(*m_z.minmax())

    m_x.setVal(x)
    m_y.setVal(y)
    m_z.setVal(z)

    dataset.add(varset)

    x = random.uniform(*m_x.minmax())
    y = random.uniform(*m_y.minmax())
    z = random.gauss(3.1, 0.015)

    m_x.setVal(x)
    m_y.setVal(y)
    m_z.setVal(z)

    dataset.add(varset)

## BxBxB
for i in xrange(0, 10000):

    x = random.uniform(*m_y.minmax())
    y = random.uniform(*m_y.minmax())
    z = random.uniform(*m_z.minmax())

    m_x.setVal(x)
    m_y.setVal(y)
    m_z.setVal(z)

    dataset.add(varset)

print dataset

import Ostap.FitModels as Models

s1 = Models.Gauss_pdf(
    'SX', xvar=m_x, mean=(3.100, 3.090, 3.110), sigma=(0.012, 0.007, 0.018))
##mean  = 3.100 ,
##sigma = 0.015 )
s2 = Models.Gauss_pdf('SY', xvar=m_y, mean=s1.mean, sigma=s1.sigma)
s3 = Models.Gauss_pdf('SZ', xvar=m_z, mean=s1.mean, sigma=s1.sigma)

models = []

model1 = Models.Fit3D(
    signal_x=s1,
    signal_y=s2,
    signal_z=s3,
    bkg_1x=0,
    bkg_1y=0,
    bkg_1z=0,
    bkg_2x=0,
    bkg_2y=0,
    bkg_2z=0,
    bkg_3x=0,
    bkg_3y=0,
    bkg_3z=0,
    ## bkg_1x   = -1   ,
    ## bkg_1y   = -1   ,
    ## bkg_1z   = -1   ,
    ## bkg_2x   = None ,
    ## bkg_2y   = None ,
    ## bkg_2z   = None ,
    ## bkg_3x   = None ,
    ## bkg_3y   = None ,
    ## bkg_3z   = None ,
)

model1.SSS = 10000
model1.SSB = 5000
model1.SBS = 5000
model1.BSS = 5000
model1.SBB = 8000
model1.BSB = 8000
model1.BBS = 8000
model1.BBB = 10000

model2 = Models.Fit3DSym(
    signal_x=s1,
    bkg_1x=0,
    bkg_2x=0,
    bkg_3x=0,
    yvar=m_y,
    zvar=m_z,
    suffix='sym')
model2.SSS = 10000
model2.SSB = 15000
model2.SBB = 32000
model2.BBB = 10000

model3 = Models.Fit3DMix(
    signal_x=s1,
    signal_y=s2,
    bkg_1x=0,
    bkg_1y=0,
    bkg_2x=0,
    bkg_2y=0,
    bkg_3x=0,
    bkg_3y=0,
    yvar=m_y,
    zvar=m_z,
    suffix='mix')
model3.SSS = 10000
model3.SSB = 10000
model3.BSS = 5000
model3.SBB = 8000
model3.BSB = 24000
model3.BBB = 10000

models.append(model1)
models.append(model2)
models.append(model3)

with rooSilent():
    r1 = model1.fitTo(dataset)
    r1 = model1.fitTo(dataset)
    r1 = model1.fitTo(dataset)
logger.info('Non-symmetrized fit results is %s' % r1)

with rooSilent():
    r2 = model2.fitTo(dataset)
    r2 = model2.fitTo(dataset)
    r2 = model2.fitTo(dataset)
logger.info('    Symmetrized fit results is %s' % r2)

with rooSilent():
    r3 = model3.fitTo(dataset)
    r3 = model3.fitTo(dataset)
    r3 = model3.fitTo(dataset)
logger.info('Mix-Symmetrized fit results is %s' % r3)

#
## check that everything is serializeable
#
import Ostap.ZipShelve as DBASE
with DBASE.tmpdb() as db:
    ##with DBASE.open('tmp.db','c') as db :
    db['x,y,vars'] = m_x, m_y, m_z, varset
    db['dataset'] = dataset
    db['models'] = models
    db['results'] = r1, r2, r3
    db.ls()

# =============================================================================
# The END
# =============================================================================
