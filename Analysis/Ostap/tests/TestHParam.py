#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# $Id$
# =============================================================================
## @file TestHParam.py
#
#  tests some histo parametrezations
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2014-05-11
#
#                    $Revision$
#  Last modification $Date$
#                 by $Author$
# =============================================================================
"""
Tests some histo parametrezations
"""
# =============================================================================
__version__ = "$Revision:"
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2014-05-10"
__all__ = ()  ## nothing to be imported
# =============================================================================
import ROOT, random
from Ostap.PyRoUts import *
from Ostap.Utils import timing
# =============================================================================
# logging
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__ or '__builtin__' == __name__:
    logger = getLogger('Ostap.TestHParam')
else:
    logger = getLogger(__name__)
# =============================================================================
logger.info('Test for various parameterizations of histograms')
# =============================================================================

from Ostap.HParamDeco import legendre_sum, chebyshev_sum

#
## histos for exponential distribution
#

h1 = ROOT.TH1F(hID(), 'histogram', 100, 0, 1)
h1.Sumw2()
h2 = ROOT.TH1F(hID(), 'histogram', 100, 0, 1)
h2.Sumw2()
h3 = ROOT.TH1F(hID(), 'histogram', 100, 0, 1)
h3.Sumw2()
h4 = ROOT.TH1F(hID(), 'histogram', 100, 0, 1)
h4.Sumw2()
h5 = ROOT.TH1F(hID(), 'histogram', 100, 0, 1)
h5.Sumw2()
h6 = ROOT.TH1F(hID(), 'histogram', 100, 0, 1)
h6.Sumw2()

#f1 = ROOT.TF1('f3','exp(-x/3.)'       ,0,10)
#f2 = ROOT.TF1('f4','exp(-(10-x)/3.)'  ,0,10)
#f3 = ROOT.TF1('f3','1-exp(-x/3.)'     ,0,10)
#f4 = ROOT.TF1('f4','1-exp(-(10-x)/3.)',0,10)

f1 = ROOT.TF1('f3', '(x-1)**2', 0, 1)
f2 = ROOT.TF1('f4', 'x**2', 0, 1)
f3 = ROOT.TF1('f3', '1-(x-1)**2', 0, 1)
f4 = ROOT.TF1('f4', '1-x**2', 0, 1)
f5 = ROOT.TF1('f5', '4*(x-0.5)**2', 0, 1)
f6 = ROOT.TF1('f5', '1-4*(x-0.5)**2', 0, 1)

entries = 100000
##
## random.seed(10)
for i in xrange(0, entries):
    h1.Fill(f1.GetRandom())
    h2.Fill(f2.GetRandom())
    h3.Fill(f3.GetRandom())
    h4.Fill(f4.GetRandom())
    h5.Fill(f5.GetRandom())
    h6.Fill(f6.GetRandom())

# h1 - decreasing convex
# h2 - increasing convex
# h3 - increasing concave
# h4 - decreasing concave
# h5 - non-monothonic convex     (symmetric)
# h6 - non-monothonic concave    (symmetric)


## make a quadratic difference between two functions
def _diff2_(fun1, fun2, xmin, xmax):

    _fun1_ = lambda x: float(fun1(x))**2
    _fun2_ = lambda x: float(fun2(x))**2
    _fund_ = lambda x: (float(fun1(x)) - float(fun2(x)))**2

    from scipy import integrate
    import warnings

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        d1 = integrate.quad(_fun1_, xmin, xmax)[0]
        d2 = integrate.quad(_fun2_, xmin, xmax)[0]
        dd = integrate.quad(_fund_, xmin, xmax)[0]

    import math
    return math.sqrt(dd / (d1 * d2))


## make a quadratic difference between histogram and function
def diff2(func, histo):

    _f = func[2]
    _n = float(func[-1])

    _fun1 = lambda x: _n * _f(x)
    _fun2 = lambda x: float(histo(x))

    return _diff2_(_fun1, _fun2, histo.xmin(), histo.xmax())


## make a quadratic difference between histogram and function
def diff3(func, histo):

    _f = func[2]
    _n = float(func[3])

    _fun1 = lambda x: _n * _f(x)
    _fun2 = lambda x: float(histo(x))

    return _diff2_(_fun1, _fun2, histo.xmin(), histo.xmax())


## make a quadratic difference between histogram and function
def diff1(func, histo):

    _fun1 = lambda x: func(x)
    _fun2 = lambda x: float(histo(x))

    return _diff2_(_fun1, _fun2, histo.xmin(), histo.xmax())


logger.info(100 * '*')
logger.info('Parameterizations techniques using histogram values only  (fast)')
logger.info(100 * '*')

with timing('Bezier[4]'):  ## , logger ) :
    rB1 = h1.bernstein_sum(4)
    rB2 = h2.bernstein_sum(4)
    rB3 = h3.bernstein_sum(4)
    rB4 = h4.bernstein_sum(4)
    rB5 = h5.bernstein_sum(4)
    rB6 = h6.bernstein_sum(4)
logger.info('Bezier[4]: diff      %s ' % [
    diff1(*p)
    for p in [(rB1, h1), (rB2, h2), (rB3, h3), (rB4, h4), (rB5, h5), (rB6, h6)]
])
with timing('BezierEven[2]', logger):
    rB1 = h1.bernsteineven_sum(2)
    rB2 = h2.bernsteineven_sum(2)
    rB3 = h3.bernsteineven_sum(2)
    rB4 = h4.bernsteineven_sum(2)
    rB5 = h5.bernsteineven_sum(2)
    rB6 = h6.bernsteineven_sum(2)
logger.info('BezierEven[2]: diff  %s ' % [
    diff1(*p)
    for p in [(rB1, h1), (rB2, h2), (rB3, h3), (rB4, h4), (rB5, h5), (rB6, h6)]
])
with timing('Legendre[4]', logger):
    rB1 = h1.legendre_sum(4)
    rB2 = h2.legendre_sum(4)
    rB3 = h3.legendre_sum(4)
    rB4 = h4.legendre_sum(4)
    rB5 = h5.legendre_sum(4)
    rB6 = h6.legendre_sum(4)
logger.info('Legendre[4]: diff    %s ' % [
    diff1(*p)
    for p in [(rB1, h1), (rB2, h2), (rB3, h3), (rB4, h4), (rB5, h5), (rB6, h6)]
])

with timing('Chebyshev[4]', logger):
    rB1 = h1.chebyshev_sum(4)
    rB2 = h2.chebyshev_sum(4)
    rB3 = h3.chebyshev_sum(4)
    rB4 = h4.chebyshev_sum(4)
    rB5 = h5.chebyshev_sum(4)
    rB6 = h6.chebyshev_sum(4)
logger.info('Chebyshev[4]: diff   %s ' % [
    diff1(*p)
    for p in [(rB1, h1), (rB2, h2), (rB3, h3), (rB4, h4), (rB5, h5), (rB6, h6)]
])

with timing('Fourier[12]', logger):
    rB1 = h1.fourier_sum(12)
    rB2 = h2.fourier_sum(12)
    rB3 = h3.fourier_sum(12)
    rB4 = h4.fourier_sum(12)
    rB5 = h5.fourier_sum(12)
    rB6 = h6.fourier_sum(12)
logger.info('Fourier[12]: diff    %s ' % [
    diff1(*p)
    for p in [(rB1, h1), (rB2, h2), (rB3, h3), (rB4, h4), (rB5, h5), (rB6, h6)]
])

with timing('Cosine[12]', logger):
    rB1 = h1.cosine_sum(12)
    rB2 = h2.cosine_sum(12)
    rB3 = h3.cosine_sum(12)
    rB4 = h4.cosine_sum(12)
    rB5 = h5.cosine_sum(12)
    rB6 = h6.cosine_sum(12)
logger.info('Cosine[12]: diff     %s ' % [
    diff1(*p)
    for p in [(rB1, h1), (rB2, h2), (rB3, h3), (rB4, h4), (rB5, h5), (rB6, h6)]
])

logger.info(100 * '*')
logger.info('Parameterizations techniques using ROOT::TH1::Fit (slow)')
logger.info(100 * '*')

with timing('Bernstein[4]', logger):
    rB1 = h1.bernstein(4)
    rB2 = h2.bernstein(4)
    rB3 = h3.bernstein(4)
    rB4 = h4.bernstein(4)
    rB5 = h5.bernstein(4)
    rB6 = h6.bernstein(4)

logger.info('Bernstein[4]: diff   %s ' % [
    diff2(*p)
    for p in [(rB1, h1), (rB2, h2), (rB3, h3), (rB4, h4), (rB5, h5), (rB6, h6)]
])
with timing('BernsteinEven[2]', logger):
    rBE1 = h1.bernsteineven(2)
    rBE2 = h2.bernsteineven(2)
    rBE3 = h3.bernsteineven(2)
    rBE4 = h4.bernsteineven(2)
    rBE5 = h5.bernsteineven(2)
    rBE6 = h6.bernsteineven(2)
logger.info('BernsteinEven[2]:dif %s ' % [
    diff2(*p)
    for p in [(rBE1, h1), (rBE2, h2), (rBE3, h3), (rBE4, h4), (rBE5,
                                                               h5), (rBE6, h6)]
])

with timing('Chebyshev[4]', logger):
    rC1 = h1.chebyshev(4)
    rC2 = h2.chebyshev(4)
    rC3 = h3.chebyshev(4)
    rC4 = h4.chebyshev(4)
    rC5 = h5.chebyshev(4)
    rC6 = h6.chebyshev(4)
logger.info('Chebyshev[4]: diff   %s ' % [
    diff2(*p)
    for p in [(rC1, h1), (rC2, h2), (rC3, h3), (rC4, h4), (rC5, h5), (rC6, h6)]
])

with timing('Legendre[4]', logger):
    rL1 = h1.legendre(4)
    rL2 = h2.legendre(4)
    rL3 = h3.legendre(4)
    rL4 = h4.legendre(4)
    rL5 = h5.legendre(4)
    rL6 = h6.legendre(4)
logger.info('Legendre[4]: diff    %s ' % [
    diff2(*p)
    for p in [(rL1, h1), (rL2, h2), (rL3, h3), (rL4, h4), (rL5, h5), (rL6, h6)]
])

with timing('Monomial[4]', logger):
    rP1 = h1.polynomial(4)
    rP2 = h2.polynomial(4)
    rP3 = h3.polynomial(4)
    rP4 = h4.polynomial(4)
    rP5 = h5.polynomial(4)
    rP6 = h6.polynomial(4)
logger.info('Monomial[4]: diff    %s ' % [
    diff2(*p)
    for p in [(rP1, h1), (rP2, h2), (rP3, h3), (rP4, h4), (rP5, h5), (rP6, h6)]
])

with timing('Fourier[8]', logger):
    rF1 = h1.fourier(8)
    rF2 = h2.fourier(8)
    rF3 = h3.fourier(8)
    rF4 = h4.fourier(8)
    rF5 = h5.fourier(8)
    rF6 = h6.fourier(8)
logger.info('Fourier[8] : diff    %s ' % [
    diff2(*p)
    for p in [(rF1, h1), (rF2, h2), (rF3, h3), (rF4, h4), (rF5, h5), (rF6, h6)]
])

with timing('Cosine[8]', logger):
    rC1 = h1.cosine(8)
    rC2 = h2.cosine(8)
    rC3 = h3.cosine(8)
    rC4 = h4.cosine(8)
    rC5 = h5.cosine(8)
    rC6 = h6.cosine(8)
logger.info('Cosine[8]: diff      %s ' % [
    diff2(*p)
    for p in [(rC1, h1), (rC2, h2), (rC3, h3), (rC4, h4), (rC5, h5), (rC6, h6)]
])

with timing('Positive[4]', logger):
    rp1 = h1.positive(4)
    rp2 = h2.positive(4)
    rp3 = h3.positive(4)
    rp4 = h4.positive(4)
    rp5 = h5.positive(4)
    rp6 = h6.positive(4)
logger.info('Positive[4]: diff    %s ' % [
    diff2(*p)
    for p in [(rp1, h1), (rp2, h2), (rp3, h3), (rp4, h4), (rp5, h5), (rp6, h6)]
])

with timing('PositiveEven[4]', logger):
    rpe1 = h1.positiveeven(4)
    rpe2 = h2.positiveeven(4)
    rpe3 = h3.positiveeven(4)
    rpe4 = h4.positiveeven(4)
    rpe5 = h5.positiveeven(4)
    rpe6 = h6.positiveeven(4)
logger.info('PositiveEven[4]:diff %s ' % [
    diff2(*p)
    for p in [(rpe1, h1), (rpe2, h2), (rpe3, h3), (rpe4, h4), (rpe5,
                                                               h5), (rpe6, h6)]
])

with timing('Monothonic[4]', logger):
    rm1 = h1.monothonic(4, increasing=False)
    rm2 = h2.monothonic(4, increasing=True)
    rm3 = h3.monothonic(4, increasing=True)
    rm4 = h4.monothonic(4, increasing=False)
    rm5 = h5.monothonic(4, increasing=True)  ## should be bad
    rm6 = h6.monothonic(4, increasing=False)  ## should be bad
logger.info('PositiveEven[4]:diff %s ' % [
    diff2(*p)
    for p in [(rm1, h1), (rm2, h2), (rm3, h3), (rm4, h4), (rm5, h5), (rm6, h6)]
])

with timing('Convex[4]', logger):
    rZ1 = h1.convex(4, increasing=False, convex=True)
    rZ2 = h2.convex(4, increasing=True, convex=True)
    rZ3 = h3.convex(4, increasing=True, convex=False)
    rZ4 = h4.convex(4, increasing=False, convex=False)
    rZ5 = h5.convex(4, increasing=True, convex=True)
    rZ6 = h6.convex(4, increasing=False, convex=False)
logger.info('Convex[4] : diff     %s ' % [
    diff2(*p)
    for p in [(rZ1, h1), (rZ2, h2), (rZ3, h3), (rZ4, h4), (rZ5, h5), (rZ6, h6)]
])

with timing('Convex/ConcavePoly[4]', logger):
    rZ1 = h1.convexpoly(4)
    rZ2 = h2.convexpoly(4)
    rZ3 = h3.concavepoly(4)
    rZ4 = h4.concavepoly(4)
    rZ5 = h5.convexpoly(4)
    rZ6 = h6.concavepoly(4)
logger.info('Convex/Concave[4]:di %s ' % [
    diff2(*p)
    for p in [(rZ1, h1), (rZ2, h2), (rZ3, h3), (rZ4, h4), (rZ5, h5), (rZ6, h6)]
])
with timing('B-spline[2,2]', logger):
    rS1 = h1.bSpline(2, 2)
    rS2 = h2.bSpline(2, 2)
    rS3 = h3.bSpline(2, 2)
    rS4 = h4.bSpline(2, 2)
    rS5 = h5.bSpline(2, 2)
    rS6 = h6.bSpline(2, 2)
logger.info('B-spline[2,2] : diff %s ' % [
    diff2(*p)
    for p in [(rS1, h1), (rS2, h2), (rS3, h3), (rS4, h4), (rS5, h5), (rS6, h6)]
])
with timing('P-spline[2,2]', logger):
    rb1 = h1.pSpline(2, 2)
    rb2 = h2.pSpline(2, 2)
    rb3 = h3.pSpline(2, 2)
    rb4 = h4.pSpline(2, 2)
    rb5 = h5.pSpline(2, 2)
    rb6 = h6.pSpline(2, 2)
logger.info('P-spline[2,2] : diff %s ' % [
    diff2(*p)
    for p in [(rb1, h1), (rb2, h2), (rb3, h3), (rb4, h4), (rb5, h5), (rb6, h6)]
])

with timing('M-spline[2,2]', logger):
    rM1 = h1.mSpline(2, 2, increasing=False)
    rM2 = h2.mSpline(2, 2, increasing=True)
    rM3 = h3.mSpline(2, 2, increasing=True)
    rM4 = h4.mSpline(2, 2, increasing=False)
    rM5 = h5.mSpline(2, 2, increasing=True)  ## should be bad
    rM6 = h6.mSpline(2, 2, increasing=False)  ## should be bad
logger.info('M-spline[2,2] : diff %s ' % [
    diff2(*p)
    for p in [(rM1, h1), (rM2, h2), (rM3, h3), (rM4, h4), (rM5, h5), (rM6, h6)]
])

with timing('C-spline[2,2]', logger):
    rc1 = h1.cSpline(2, 2, increasing=False, convex=True)
    rc2 = h2.cSpline(2, 2, increasing=True, convex=True)
    rc3 = h3.cSpline(2, 2, increasing=True, convex=False)
    rc4 = h4.cSpline(2, 2, increasing=False, convex=False)
    rc5 = h5.cSpline(2, 2, increasing=True, convex=True)  ## should be bad
    rc6 = h6.cSpline(2, 2, increasing=False, convex=False)  ## should be bad
logger.info('C-spline[2,2] : diff %s ' % [
    diff2(*p)
    for p in [(rc1, h1), (rc2, h2), (rc3, h3), (rc4, h4), (rc5, h5), (rc6, h6)]
])

with timing('Convex-spline[2,2]', logger):
    rc1 = h1.convexSpline(2, 2)
    rc2 = h2.convexSpline(2, 2)
    rc3 = h3.concaveSpline(2, 2)
    rc4 = h2.concaveSpline(2, 2)
    rc5 = h2.convexSpline(2, 2)
    rc6 = h2.concaveSpline(2, 2)
logger.info('Convex-spline[2,2]:d %s ' % [
    diff2(*p)
    for p in [(rc1, h1), (rc2, h2), (rc3, h3), (rc4, h4), (rc5, h5), (rc6, h6)]
])
logger.info(100 * '*')
logger.info('Parameterizations techniques using RooFit  (relatively fast)')
logger.info(100 * '*')

with timing('Positive[4]', logger):
    rp1p = h1.pdf_positive(4, silent=True)
    rp2p = h2.pdf_positive(4, silent=True)
    rp3p = h3.pdf_positive(4, silent=True)
    rp4p = h4.pdf_positive(4, silent=True)
    rp5p = h5.pdf_positive(4, silent=True)
    rp6p = h6.pdf_positive(4, silent=True)
logger.info('Positive[4] : diff    %s ' % [
    diff3(*p)
    for p in [(rp1p, h1), (rp2p, h2), (rp3p, h3), (rp4p, h4), (rp5p,
                                                               h5), (rp6p, h6)]
])

with timing('Monothonic[4]', logger):
    rm1 = h1.pdf_decreasing(4, silent=True)
    rm2 = h2.pdf_increasing(4, silent=True)
    rm3 = h3.pdf_increasing(4, silent=True)
    rm4 = h4.pdf_decreasing(4, silent=True)
    rm5 = h5.pdf_increasing(4, silent=True)  ## must be bad
    rm6 = h6.pdf_decreasing(4, silent=True)  ## must be bad
logger.info('Monothonic[4] : diff  %s ' % [
    diff3(*p)
    for p in [(rm1, h1), (rm2, h2), (rm3, h3), (rm4, h4), (rm5, h5), (rm6, h6)]
])

with timing('Convex[4]', logger):
    rZ1 = h1.pdf_convex_decreasing(4, silent=True, density=False)
    rZ2 = h2.pdf_convex_increasing(4, silent=True, density=False)
    rZ3 = h3.pdf_concave_increasing(4, silent=True, density=False)
    rZ4 = h4.pdf_concave_decreasing(4, silent=True, density=False)
    rZ5 = h3.pdf_convex_increasing(
        4, silent=True, density=False)  ## must be bad
    rZ6 = h4.pdf_concave_decreasing(
        4, silent=True, density=False)  ## must be bad
logger.info('Convex[4] : diff      %s ' % [
    diff3(*p)
    for p in [(rZ1, h1), (rZ2, h2), (rZ3, h3), (rZ4, h4), (rZ5, h5), (rZ6, h6)]
])

with timing('Convex/Concave[4]', logger):
    rZ1 = h1.pdf_convexpoly(4, silent=True, density=False)
    rZ2 = h2.pdf_convexpoly(4, silent=True, density=False)
    rZ3 = h3.pdf_concavepoly(4, silent=True, density=False)
    rZ4 = h4.pdf_concavepoly(4, silent=True, density=False)
    rZ5 = h5.pdf_convexpoly(4, silent=True, density=False)
    rZ6 = h6.pdf_concavepoly(4, silent=True, density=False)
logger.info('Convex/Concave[4]:dif %s ' % [
    diff3(*p)
    for p in [(rZ1, h1), (rZ2, h2), (rZ3, h3), (rZ4, h4), (rZ5, h5), (rZ6, h6)]
])

with timing('P-spline[2,2]', logger):
    rb1p = h1.pdf_pSpline(spline=(2, 1), silent=True, density=False)
    rb2p = h2.pdf_pSpline(spline=(2, 1), silent=True, density=False)
    rb3p = h3.pdf_pSpline(spline=(2, 1), silent=True, density=False)
    rb4p = h4.pdf_pSpline(spline=(2, 1), silent=True, density=False)
    rb5p = h5.pdf_pSpline(spline=(2, 1), silent=True, density=False)
    rb6p = h6.pdf_pSpline(spline=(2, 1), silent=True, density=False)
logger.info('P-spline[2,2] : diff  %s ' % [
    diff3(*p)
    for p in [(rb1p, h1), (rb2p, h2), (rb3p, h3), (rb4p, h4), (rb5p,
                                                               h5), (rb6p, h6)]
])

with timing('M-spline[3,2]', logger):
    rM1p = h1.pdf_mSpline((3, 2, False), silent=True, density=False)
    rM2p = h2.pdf_mSpline((3, 2, True), silent=True, density=False)
    rM3p = h3.pdf_mSpline((3, 2, True), silent=True, density=False)
    rM4p = h4.pdf_mSpline((3, 2, False), silent=True, density=False)
    rM5p = h5.pdf_mSpline((3, 2, True), silent=True,
                          density=False)  ## must be bad
    rM6p = h6.pdf_mSpline((3, 2, False), silent=True,
                          density=False)  ## must be bad
logger.info('M-spline[3,2] : diff  %s ' % [
    diff3(*p)
    for p in [(rM1p, h1), (rM2p, h2), (rM3p, h3), (rM4p, h4), (rM5p,
                                                               h5), (rM6p, h6)]
])

with timing('C-spline[2,2]', logger):
    rc1p = h1.pdf_cSpline((2, 2, False, True), silent=True, density=False)
    rc2p = h2.pdf_cSpline((2, 2, True, True), silent=True, density=False)
    rc3p = h3.pdf_cSpline((2, 2, True, False), silent=True, density=False)
    rc4p = h4.pdf_cSpline((2, 2, False, False), silent=True, density=False)
    rc5p = h5.pdf_cSpline((2, 2, True, False), silent=True,
                          density=False)  ## bad
    rc6p = h6.pdf_cSpline((2, 2, False, False), silent=True,
                          density=False)  ## bad

logger.info('C-spline[2,2] : diff  %s ' % [
    diff3(*p)
    for p in [(rc1p, h1), (rc2p, h2), (rc3p, h3), (rc4p, h4), (rc5p,
                                                               h5), (rc6p, h6)]
])
with timing('C/C-spline[2,3]', logger):
    rc1p = h1.pdf_convexSpline((2, 3), silent=True, density=False)
    rc2p = h2.pdf_convexSpline((2, 3), silent=True, density=False)
    rc3p = h3.pdf_concaveSpline((2, 3), silent=True, density=False)
    rc4p = h4.pdf_concaveSpline((2, 3), silent=True, density=False)
    rc5p = h5.pdf_convexSpline((2, 3), silent=True, density=False)
    rc6p = h6.pdf_concaveSpline((2, 3), silent=True, density=False)
logger.info('C/C-spline[2,3]: diff %s ' % [
    diff3(*p)
    for p in [(rc1p, h1), (rc2p, h2), (rc3p, h3), (rc4p, h4), (rc5p,
                                                               h5), (rc6p, h6)]
])

# =============================================================================
# The END
# =============================================================================
