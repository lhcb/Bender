#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file TestData.py
#
#  tests for simple data-access
#
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2014-06-06
# =============================================================================
"""
tests for simple data-access
"""
# =============================================================================
__version__ = "$Revision:"
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2014-06-08"
__all__ = ()  ## nothing to be imported
# =============================================================================
import ROOT, os
import Ostap.PyRoUts
from Ostap.Data import Data, DataAndLumi
# =============================================================================
# logging
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__ or '__builtin__' == __name__:
    logger = getLogger('Ostap.TestData')
else:
    logger = getLogger(__name__)
# =============================================================================
logger.info('Test  Data')
# =============================================================================
# data patterns:
ganga = '/afs/cern.ch/work/i/ibelyaev/public/GANGA/workspace/ibelyaev/LocalXML'
if os.path.exists('/mnt/shared/VMDATA/LocalXML'):
    ganga = '/mnt/shared/VMDATA/LocalXML'
# =============================================================================
patterns = [
    ganga + '/690/*/output/ZC.root',  ## 2k+11,down
    ganga + '/691/*/output/ZC.root',  ## 2k+11,up
    ganga + '/692/*/output/ZC.root',  ## 2k+12,down
    ganga + '/693/*/output/ZC.root',  ## 2k+12,up
    ganga + '/708/*/output/ZC.root',  ## 2k+15,down
    ganga + '/709/*/output/ZC.root',  ## 2k+15,up
]
"""
data = Data ( 'aZ0/Z0'   , patterns )
logger.info  ( 'DATA %s' % data     )

# =============================================================================
logger.info('Test  Data&Lumi')
# =============================================================================

data7  = DataAndLumi ( 'aZ0/Z0' , patterns[ :2])
data8  = DataAndLumi ( 'aZ0/Z0' , patterns[2:4])
data13 = DataAndLumi ( 'aZ0/Z0' , patterns[4: ])
logger.info ( 'DATA@ 7TeV   %s' % data7  )
logger.info ( 'DATA@ 8TeV   %s' % data8  )
logger.info ( 'DATA@13TeV   %s' % data13 )
data_runI = data7 + data8
logger.info ( 'DATA Run-I   %s' % data_runI )
data_tot  = data13 | data_runI
logger.info ( 'DATA Total   %s' % data_tot  )
data_none = data7  & data8
logger.info ( 'DATA None    %s' % data_none )


chain = data.chain
logger.info ( 'TChain %s' % chain )

hZ0 = ROOT.TH1D('hZ0','mass(mu+mu-)',140,50,120)
chain.project( hZ0 , "mass", "50<= mass && mass <= 120 && 0<=c2dtf && c2dtf<5" )
logger.info ( hZ0.dump( 80 , 20 ) )


data_runI = data7 + data8

"""

data7q = DataAndLumi('aZ0/Z0', patterns[:2], missing=False)
data8q = DataAndLumi('aZ0/Z0', patterns[2:4], missing=False)
data13q = DataAndLumi('aZ0/Z0', patterns[4:], missing=False)

logger.info('DATA@ 7TeV(q)%s' % data7q)
logger.info('DATA@ 8TeV(q)%s' % data8q)
logger.info('DATA@13TeV(q)%s' % data13q)
data_runIq = data7q + data8q
logger.info('DATA Run-I(q)%s' % data_runIq)
data_totq = data13q | data_runIq
logger.info('DATA Total(q)%s' % data_totq)
data_noneq = data7q & data8q
logger.info('DATA None (q)%s' % data_noneq)

## # =============================================================================
## # More tests with more files
## eos = 'root://eoslhcb.cern.ch/'
## patterns = [ eos + '/eos/lhcb/wg/PID/PIDCalib_nopt_muons/MagDown/*_pidcalib_lowpt.root' ,
##              eos + '/eos/lhcb/wg/PID/PIDCalib_nopt_muons/MagUp/*_pidcalib_lowpt.root'   ]

## data_mup_d = Data ( 'Jpsinopt_MuPTuple/DecayTree' , patterns[0] , quick = True )
## data_mum_d = Data ( 'Jpsinopt_MuMTuple/DecayTree' , patterns[0] , quick = True )
## data_mup_u = Data ( 'Jpsinopt_MuPTuple/DecayTree' , patterns[1] , quick = True )
## data_mum_u = Data ( 'Jpsinopt_MuMTuple/DecayTree' , patterns[1] , quick = True )
## data_mup   = data_mup_u + data_mup_d
## data_mum   = data_mum_u + data_mum_d

## logger.info ( 'low-pt mu+:   %s'    % data_mup )
## logger.info ( 'low-pt mu-:   %s'    % data_mum )

# =============================================================================

# =============================================================================
# The END
# =============================================================================
