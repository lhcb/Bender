#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file TestKeysPdf.py
#  tests for RooKeysPdf with Ostap
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2018-07-19
# =============================================================================
""" Test for RooKeysPdf with Ostap
"""
# =============================================================================
__version__ = "$Revision:"
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2014-05-10"
__all__ = ()  ## nothing to be imported
# =============================================================================
import ROOT, random, time
from Ostap.PyRoUts import *
import Ostap.FitModels as Models
from Ostap.Canvas import auto_plot
# =============================================================================
# logging
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' == __name__ or '__builtin__' == __name__:
    logger = getLogger('Ostap.TestKeys')
else:
    logger = getLogger(__name__)
# =============================================================================
logger.info('Test RooKeysPdf with Ostap ')
# =============================================================================
## make simple test mass
# =============================================================================
mass = ROOT.RooRealVar('mass', 'Some test mass', 3.0, 3.2)

# =============================================================================
## book very simple data sets
# =============================================================================
varset = ROOT.RooArgSet(mass)
dataset0 = ROOT.RooDataSet(dsID(), 'Test Data set',
                           varset)  ## dataset for model
dataset1 = ROOT.RooDataSet(dsID(), 'Test Data set', varset)  ## dataste for fit

mmin, mmax = mass.minmax()

# =============================================================================
## fill dataset for model
# =============================================================================
m = VE(3.100, 0.015**2)
for i in xrange(0, 50000):
    x = m.gauss()
    if 3.0 < x < 3.2:
        mass.value = m.gauss()
        dataset0.add(varset)

# =============================================================================
## fill dataset for fit
# =============================================================================
for i in xrange(0, 1000):
    mass.value = m.gauss()
    dataset1.add(varset)
for i in xrange(0, 1000):
    mass.value = random.uniform(*mass.minmax())
    dataset1.add(varset)

# =============================================================================
## create the fit model
# =============================================================================
raw = ROOT.RooKeysPdf("raw", "title", dataset0.mass, dataset0)
signal = Models.Generic1D_pdf(raw, xvar=mass, name='Signal')
model = Models.Fit1D(signal=signal, background='const')

model.signal.draw()
time.sleep(5)  ## sleep for 5  seconds before the next step

# =============================================================================
## fit it! and visualize the
# =============================================================================
r, f = model.fitTo(dataset1, draw=True, silent=True)
logger.info('fit result is %s' % r)

time.sleep(5)  ## sleep for 5  seconds before exist

# =============================================================================
## The END
# =============================================================================
