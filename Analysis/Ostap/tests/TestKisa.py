#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file TestKisa.py
#  Test for parallel data processing
#  @author Vanya BELYAEV Ivan.Belyaeve@itep.ru
#  @date 2015-05-17
# =============================================================================
"""
Test for parallel data processing
"""
# =============================================================================
__version__ = "$Revision:"
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__ = "2014-06-08"
__all__ = ()  ## nothing to be imported
# =============================================================================
import ROOT, os, random
import Ostap.PyRoUts
from Ostap.Data import Data
from Ostap.Utils import timing
import Ostap.Kisa  ## attention!!
# =============================================================================
# logging
# =============================================================================
from AnalysisPython.Logger import getLogger
if '__main__' == __name__ or '__builtin__' == __name__:
    logger = getLogger('Ostap.TestKisa')
else:
    logger = getLogger(__name__)
# =============================================================================
logger.info('Test  Data')


# =============================================================================
def prepare_data(nfiles=100, nentries=100000):

    from array import array
    var1 = array('d', [0])
    var2 = array('d', [0])
    var3 = array('d', [0])

    from Ostap.Utils import CleanUp
    files = []
    for i in xrange(nfiles):
        fname = CleanUp.tempfile(suffix='.root', prefix='test_kisa_')

        with ROOT.TFile.Open(fname, 'new') as root_file:

            tree = ROOT.TTree('S', 'tree')
            tree.SetDirectory(root_file)
            tree.Branch('mass', var1, 'mass/D')
            tree.Branch('c2dtf', var2, 'c2dtf/D')
            tree.Branch('pt', var3, 'pt/D')

            for i in xrange(nentries):

                m = random.gauss(3.1, 0.015)
                c2 = random.gammavariate(2.5, 0.2)
                pt = random.uniform(0, 10)

                var1[0] = m
                var2[0] = c2
                var3[0] = pt

                tree.Fill()
            root_file.Write()
            files.append(fname)

    return Data('S', files)


with timing('Prepare data'):
    logger.info('Prepare data, it could take some time')
    data = prepare_data(100, 20000)
    logger.info('DATA %s' % data)


def test_kisa():

    h1 = ROOT.TH1D('h1', '', 200, 3, 3.2)
    h2 = h1.clone()

    chain = data.chain

    with timing('SEQUENTIAL(%s):' % len(chain), logger):
        chain.project(h1, 'mass',
                      '3<=mass && mass<=3.2 && 0<=c2dtf && c2dtf<5')

    logger.info(h1.dump(100, 30))

    with timing('PARALLEL(%s):' % len(chain), logger):
        chain.pproject(
            h2,
            'mass',
            '3<=mass && mass<=3.2 && 0<=c2dtf && c2dtf<5',
            silent=True)

    logger.info(h2.dump(100, 30))


class MASS(object):
    def __call__(self, s):
        return s.mass


def MASS1(s):
    return s.mass


def test_kisa2():

    from Ostap.Selectors import SelectorWithVars, Variable
    variables = [
        ## Variable ( 'mass1' , 'mass(mu+mu-)' , 2 , 4 , lambda s : s.mass ) ,
        Variable('mass2', 'mass(mu+mu-)', 1, 5, MASS1),
        Variable('mass3', 'mass(mu+mu-)', 1, 5, MASS())
    ]

    ppservers = ()  ## 'lxplus051' , )

    nf = len(data.files)
    nf //= 40
    nf += 1

    with timing('%s files in sequence %s' % (nf, len(data.chain))):
        selector = SelectorWithVars(
            variables=variables,
            selection='2<=mass && mass<4 && 0<=c2dtf && c2dtf<5',
            silence=True)
        chain = data.chain[:nf]
        chain.process(selector, silent=True)
        ds = selector.data
    logger.info('Dataset: %s' % ds)

    with timing(
            '%s files in parallel %s' % (len(data.files), len(data.chain))):
        selector = SelectorWithVars(
            variables=variables,
            selection='2<=mass && mass<4 && 0<=c2dtf && c2dtf<5',
            silence=True)
        st = data.chain.pprocess(selector, silent=True)
        ds = selector.data
    logger.info('Dataset: %s' % ds)


def test_stat():

    import Ostap.Selectors

    chain = data.chain[:3]
    ds = chain.make_dataset(['mass', 'pt', 'c2dtf'])
    from Ostap.RooFitDeco import useStorage
    with useStorage():
        ds = chain.make_dataset(['mass', 'pt', 'c2dtf'])
        ds = ds[0]
        tree = ds.store().tree()

        logger.info('Mean         pt %s/%s/%s' %
                    (chain.mean('pt'), ds.mean('pt'), tree.mean('pt')))
        logger.info('Rms          pt %s/%s/%s' %
                    (chain.rms('pt'), ds.rms('pt'), tree.rms('pt')))
        logger.info('Moment(5)    pt %s/%s/%s' % (chain.moment(
            5, 'pt'), ds.moment(5, 'pt'), tree.moment(5, 'pt')))
        logger.info('GetMoment(7) pt %s/%s/%s' %
                    (chain.get_moment(7, 10, 'pt'), ds.get_moment(7, 10, 'pt'),
                     tree.get_moment(7, 10, 'pt')))
        logger.info('CMoment(6)   pt %s/%s/%s' % (chain.central_moment(
            6, 'pt'), ds.central_moment(6, 'pt'), tree.central_moment(6,
                                                                      'pt')))
        logger.info(
            'Skewness     pt %s/%s/%s' %
            (chain.skewness('pt'), ds.skewness('pt'), tree.skewness('pt')))
        logger.info(
            'Kurtosis     pt %s/%s/%s' %
            (chain.kurtosis('pt'), ds.kurtosis('pt'), tree.kurtosis('pt')))
        logger.info('Median       pt %s/%s/%s' %
                    (chain.median('pt'), ds.median('pt'), tree.median('pt')))
        logger.info(
            'Terciles     pt %s/%s/%s' %
            (chain.terciles('pt'), ds.terciles('pt'), tree.terciles('pt')))


if '__main__' == __name__:

    test_kisa()
    test_kisa2()
    test_stat()

# =============================================================================
# The END
# =============================================================================
