#!/usr/bin/env python 
# -*- coding: utf-8 -*-
# =============================================================================
## @file Bender/Main.py
#  The major Python module for Bender application 
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @date   2004-07-11
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#
# =============================================================================
"""This is a major Python Module for Bender application

oooooooooo.                              .o8                     
`888'   `Y8b                            \"888                     
 888     888  .ooooo.  ooo. .oo.    .oooo888   .ooooo.  oooo d8b 
 888oooo888' d88' `88b `888P\"Y88b  d88' `888  d88' `88b `888\"\"8P 
 888    `88b 888ooo888  888   888  888   888  888ooo888  888     
 888    .88P 888    .o  888   888  888   888  888    .o  888     
o888bood8P'  `Y8bod8P' o888o o888o `Y8bod88P\" `Y8bod8P' d888b    

This file is a part of BENDER project:
    ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
    ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campain of Dr.O.Callot et al.: 
    ``No Vanya's lines are allowed in LHCb/Gaudi software.''

  Last modification $Date$
                 by $Author$ 
"""
# =============================================================================
__author__  = 'Vanya BELYAEV ibelyaev@physics.syr.edu'
__date__    = "2004-07-11"
__version__ = '$Revision$' 
# =============================================================================
## logging
# =============================================================================
from Bender.Logger import getLogger 
if '__main__' == __name__ : logger = getLogger ( 'Bender.Main' )
else                      : logger = getLogger ( __name__ )
# =============================================================================
## some default bender startup file
# =============================================================================
## Bender startup: history, readlines, etc... 
import Bender.Startup

# =============================================================================    
## massive imports of everything-II
logger.debug ( "Massive   import of ``everything''")
from LoKiCore.decorators           import *
from LoKiPhys.decorators           import *
from LoKiArrayFunctors.decorators  import *
from LoKiAlgo.decorators           import *
from LoKiCore.functions            import *

## import pseudo-configurable
from Bender.Configuration import BenderAlgo
## import "Selection"   
from Bender.Selections    import BenderSelection
 
# =============================================================================
## redefine intialization to allow use "fake" configurable as argument
def _algo_init_ ( self , name , **kwargs ) :
    props = {}
    if not isinstance ( name , str ) :
        ## selection? 
        if hasattr ( name , 'algorithm'     ) : name = name.algorithm()
        ## configurbale?
        if hasattr ( name , 'getProperties' ) and hasattr ( name , 'isPropertySet' ) :
            _props = name.getProperties()
            for k,v in _props.iteritems() :
                if name.isPropertySet(k)  : props [k] = v
        if   hasattr ( name , 'name'    ) : name = name.name    ()
        elif hasattr ( name , 'getName' ) : name = name.getName ()
        elif hasattr ( name , 'GetName' ) : name = name.GetName ()
        else :
            logger.warning('Algo: suspicios "name" attribute %s/%s' %  ( type(name) , name ) ) 
        
    props.update ( kwargs )
    #
    for k in ( 'HistoPrint'  , 'NTuplePrint' ,
               'ErrorsPrint' , 'StatPrint'   ) :
        if not props.has_key ( k ) : props[k] = True
        
    import GaudiPython.GaudiAlgs as _GPA 
    _GPA._init_ ( self , name , **props )


logger.debug('Redefine intialization to allow use fake configurable as argument')
Algo.__init__ = _algo_init_ 

# =============================================================================
## set the attribute or property
# - if the attribute name corresponds to the property name, the property is updated
# - keep track on all locally added attributes 
def _set_attr_ ( self , pname , pvalue ) :
    """Set the attribute (or property) :
    - if the attribute name corresponds to the property name, the property is updated
    - keep track on all locally added attributes 
    """
    import GaudiPython.Bindings as GPB 
    if not self.hasProperty( pname ) :
        if not self.__dict__.has_key('_my_attributes') :
            self.__dict__['_my_attributes'] = set() 
        self.__dict__[pname] = pvalue
        self.__dict__['_my_attributes'].add( pname )
    else : GPB.iAlgorithm.__setattr__ ( self , pname , pvalue )


# =============================================================================
## The default finalization : finalize the base C++ class
#  - cleanup all local attributes 
def _finalize_   ( self ) :
    """The default finalization : finalize the base C++ class
    - cleanup all local attributes 
    """
    if hasattr ( self , '_my_attributes' ) :
        import inspect 
        from GaudiPython.Bindings import iProperty as iP
        for a in self._my_attributes :
            if not hasattr ( self , a ) : continue
            o = getattr ( self , a ) 
            if inspect.isfunction ( o ) : continue
            if inspect.ismethod   ( o ) : continue
            if inspect.isroutine  ( o ) : continue
            if isinstance ( o , ( bool , int , long , float , str ) ) : continue
            if isinstance ( o , ( iP   , cpp.IInterface           ) ) : continue
            delattr ( self , a ) 
        del self._my_attributes
        
    if self.HistoPrint :
        import GaudiPython.HistoUtils 
        for k, h in self.Histos().iteritems() :
            if hasattr ( h , 'dump' ) :
                self.Print ( " Histogram '%s'\n%s" % ( k ,  h.dump ( 80 , 30 ) ) ,
                             SUCCESS , 7 ) 
                
    sc = self._Base.finalize_  ( self )
    return sc

logger.debug('Redefine setattr/finalize to allow proper cleanup')
Algo.__setattr__ = _set_attr_
Algo.finalize    = _finalize_

## massive imports of everything-II
logger.debug ( "Even more imports of ``everything''")
## from LoKiTrigger.decorators        import *
from LoKiTracks.decorators         import *
from LoKiProtoParticles.decorators import *
from LoKiHlt.decorators            import *
from LoKiNumbers.decorators        import *

# =============================================================================
from Gaudi.Configuration import importOptions
from Bender.Utils        import * 
# =============================================================================
## apply some last-minutes fixes
import Bender.Fixes

## @var LoKi   : define namespace LoKi 
LoKi   = cpp.LoKi
## @var Bender : define namespace Bender 
Bender = cpp.Bender
## @var LHCb   : define namespace LHCb
LHCb   = cpp.LHCb
## @var Gaudi  : define namespace Gaudi
Gaudi  = cpp.Gaudi

SUCCESS     = cpp.StatusCode(cpp.StatusCode.SUCCESS    ,True)
FAILURE     = cpp.StatusCode(cpp.StatusCode.FAILURE    ,True)
RECOVERABLE = cpp.StatusCode(cpp.StatusCode.RECOVERABLE,True)

from GaudiKernel.PhysicalConstants import c_light

## prepare machinery for powerfull n-tuple filling 
from BenderTools.Fill   import decorateFill 
decorateFill   ( Algo )
## prepare machinery for TisTos'sing
from BenderTools.TisTos import decorateTisTos 
decorateTisTos ( Algo )
#

del decorateFill, decorateTisTos 
# =============================================================================
## 
def with_ipython() :
    try :
        return __IPYTHON__
    except NameError:
        return False 
# =============================================================================
## start (interactive) python session
#  @code
#  ...
#  run(100)
#  bender_start( globals() )
#  @endcode
def bender_start  ( names = {}    ,
                    batch = False ,
                    embed = False ,
                    plain = False ) :
    """
    Start (interactive) Bender session
    >>> ...
    >>> run(100)
    >>> bender_start ( globals() )
    """
    if    batch :
        
        logger.info ('Batch... ') 
        
    elif  embed and not with_ipython () :

        
        logger.info ('Start embedded interactive shell') 
        import IPython
        IPython.embed ()
        
    elif plain and not with_ipython() :
        
        __vars = names.copy()
        
        import readline
        import code        
        logger.info ('Start plain interactive shell') 
        
        shell = code.InteractiveConsole(__vars)
        shell.interact()
        
    elif not with_ipython()  :
        
        __vars = names.copy()        
        logger.info ('Start interactive shell')     
        import IPython
        IPython.start_ipython ( argv = [] , user_ns = __vars )

    else :

        logger.warning('Session is runnig!')


def eval_cuts ( expression ) :
    return eval ( expression ) 

# =============================================================================
if __name__ == '__main__' :

    logger.info ( 80*'*'  ) 
    logger.info ( __doc__ ) 
    logger.info ( ' Author  : %s ' %  __author__  ) 
    logger.info ( ' Version : %s ' %  __version__ ) 
    logger.info ( ' Date    : %s ' %  __date__    ) 
    logger.info ( 80*'*'  ) 

# =============================================================================
# The END 
# =============================================================================
