################################################################################
# Package: BenderUser
################################################################################
gaudi_subdir(BenderUser v30r6)

gaudi_depends_on_subdirs(Phys/Bender
                         Phys/BenderTools)

gaudi_install_python_modules()

