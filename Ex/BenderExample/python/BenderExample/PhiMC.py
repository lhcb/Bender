#!/usr/bin/env python
# =============================================================================
## @file BenderExample/PhiMC.py
# The simple Bender-based example: plot dikaon mass peak with MC-truth
#
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly 
#   Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  ``C++ ToolKit for Smart and Friendly Physics Analysis''
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software.''
#
#  @date 2006-10-12
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
# =============================================================================
"""
The simple Bender-based example plot dikaon mass peak with MC-truth

This file is a part of BENDER project:
``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from
Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the 
LoKi project: ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement 
with the smear campaign of Dr.O.Callot et al.: 
``No Vanya's lines are allowed in LHCb/Gaudi software.''
"""
# =============================================================================
__author__  = " Vanya BELYAEV Ivan.Belyaev@nikhef.nl "
__date__    = " 2006-10-12 "
__version__ = " Version $Revision$ "
# =============================================================================
## import everything form bender
from Bender.MainMC import *

# =============================================================================
## Simple class for access MC-truth 
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#  @date 2006-10-13
class PhiMC(AlgoMC) :
    """
    Simple class to plot dikaon mass peak
    """
    ## standard constructor
    def __init__ ( self , name = 'PhiMC' , **args ) :
        """
        Standard constructor
        """ 
        AlgoMC.__init__ ( self , name , **args )

    ## standard method for analyses
    def analyse( self ) :
        """
        Standard method for analyses
        """
        
        mcphi   = self.mcselect ( 'mcPhi', 'phi(1020) => K+ K-')        
        if mcphi.empty() : return self.Warning('No MC-phi is found!', SUCCESS )
        
        matcher = self.mcTruth ()
        mc  = MCTRUTH( matcher , mcphi ) 
        
        ## select all kaons 
        kaons = self.select( 'kaons'  , ( 'K+'  == ABSID ) & mc )
        
        self.select( "K+" , kaons , 0 < Q )
        self.select( "K-" , kaons , 0 > Q )
        
        dikaon = self.loop( "K+ K-" , "phi(1020)" )
        for phi in dikaon :
            m12 = phi.mass(1,2) / 1000 
            if 1.1 < m12  : continue
            mass = M(phi)/1000
            if 0   > mass : continue 
            self.plot ( mass , 'K+ K- mass'           , 1. , 1.1 ) 
            chi2 = VCHI2( phi )
            if 0 > chi2 or 49 < chi2 : continue
            self.plot ( mass , 'K+ K- mass,0<chi2<49' , 1. , 1.1 )  
            if not mc ( phi ) : continue 
            self.plot ( mass , 'K+ K- mass,mctruth'   , 1. , 1.1 ) 
           
        self.setFilterPassed( True ) 
        return SUCCESS
    
# =============================================================================
def configure ( datafiles , catalogs  = [] , castor = False , params = {} ) :
    """
    Perform the real configuration of the job
    """
    
    ##
    ## Static Configuration (``Configurables'')
    ##    
    from Configurables import DaVinci
    daVinci = DaVinci (
        DataType       = '2012' ,
        Simulation    = True    ,
        HistogramFile = 'PhiMC_Histos.root'  , 
        )

    from PhysConf.Selections import AutomaticData
    Kaons = AutomaticData('Phy/StdTightKaons/Particles')
          
    InputParticles = [ Kaons.outputLocation() ]

    phi = BenderMCSelection (
        'PhiMC'           ,
        [ StdTightKaons ] ,
        PP2MCs     = [ 'Relations/Rec/ProtoP/Charged' ]  ## MC-truth relation tables
        )
    
    daVinci.UserAlgorithms.append ( phi ) 
    
    ## define the input data 
    setData ( datafiles , catalogs , castor , useDBtags = True ) 
    
    ## get the actual application manager (create if needed)
    gaudi = appMgr() 
    
    ## create local algorithm:
    alg = PhiMC( phi )
    
    return SUCCESS 
    
# =============================================================================
## job steering 
if __name__ == '__main__' :

    ## make printout of the own documentations 
    print '*'*120
    print                      __doc__
    print ' Author  : %s ' %   __author__    
    print ' Version : %s ' %   __version__
    print ' Date    : %s ' %   __date__
    print '*'*120  
  
    ## configure the job:
    inputdata = [
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000018_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000009_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000011_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000004_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000002_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000005_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000006_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000010_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000008_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000016_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000017_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000003_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000012_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000015_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000007_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000001_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000014_1.allstreams.dst',
        '/lhcb/MC/2012/ALLSTREAMS.DST/00025873/0000/00025873_00000013_1.allstreams.dst',
        ]

    configure( inputdata , castor = True ) 

    run(1000) 

# =============================================================================
# The END 
# =============================================================================
